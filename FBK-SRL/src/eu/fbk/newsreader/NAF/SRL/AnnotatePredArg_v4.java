package eu.fbk.newsreader.NAF.SRL;

/**
 * Author: minard@fbk.eu
 * Date: Aug 2015
 * Description: 
 * Lang: Italian
 * Usage: java -cp lib/kaflib-naf-1.1.9.jar:lib/jdom-2.05.jar eu.fbk.newsreader.NAF.SRL.AnnotatedPredArg_v4 input output resources/italian_pbframes/ resources/mapping-16-30 resources/it-signal-temporal.list resources/it-signal-spatial.list
 */

import ixa.kaflib.Annotation;
import ixa.kaflib.Chunk;
import ixa.kaflib.Dep;
import ixa.kaflib.Entity;
import ixa.kaflib.ExternalRef;
import ixa.kaflib.KAFDocument;
import ixa.kaflib.KAFDocument.AnnotationType;
import ixa.kaflib.KAFDocument.LinguisticProcessor;
import ixa.kaflib.Predicate;
import ixa.kaflib.Predicate.Role;
import ixa.kaflib.Span;
import ixa.kaflib.Term;
import ixa.kaflib.Timex3;
import ixa.kaflib.WF;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AnnotatePredArg_v4 {
	
	/**
	 * Get the span of an argument given its head in the dependency tree. 
	 * @param nafFile
	 * @param from
	 * @return
	 */
	private static List<Term> getSpanArg (KAFDocument nafFile, Term from){
		List<Term> listT = new ArrayList<Term> ();
		List<Dep> depL = nafFile.getDepsByTerm(from);
		if (depL.size() > 0){
			for (Dep d : depL){	
				if (d.getTo().equals(from)){
					listT.add(d.getFrom());
					listT.addAll(getSpanArgRec(listT, nafFile, d.getFrom()));
				}
			}
		}
		listT.add(from);
		listT = complete(nafFile,sort(listT));
		
		return listT;
	}
	
	/**
	 * Recursive method to get the span of an argument.
	 * @param termL
	 * @param nafFile
	 * @param from
	 * @return
	 */
	private static List<Term> getSpanArgRec (List<Term> termL, KAFDocument nafFile, Term from){
		List<Dep> depTempL = nafFile.getDepsByTerm(from);
		if (depTempL.size() > 0){
			boolean onlyFrom = true;
			for (Dep depTemp : depTempL){
				if (depTemp.getTo().equals(from) 
						&& depTemp.getRfunc().matches(
						"^((RMOD)|(DET)|(CONTIN-DENOM)|(CONTIN-LOCUT)|(PREP-ARG)|(NOUN-APPOSITION-DENOM)|(ADJC-ARG)|(SUBJ)|(SEPARATOR)||(PRED)|(RELCL)|(DOBJ))$")){
				
					if(!termL.contains(depTemp.getFrom()) || !termL.contains(depTemp.getTo())){
						termL.add(depTemp.getFrom());
						termL.add(depTemp.getTo());
					}
				
					onlyFrom = false;
				
					List<Term> termLTemp = getSpanArgRec(termL, nafFile, depTemp.getFrom());
					for (Term ttemp : termLTemp){
						if(!termL.contains(ttemp)){
							termL.add(ttemp);
						}
					}
				}
				else if (depTemp.getTo().equals(from) 
						&& depTemp.getRfunc().equals("OPEN-PARENTHETICAL")){

					Term closePar = null;
					for (Dep depTempP : depTempL){
						if (depTempP.getRfunc().equals("CLOSE-PARENTHETICAL")){
							closePar = depTempP.getFrom();
							break;
						}
					}
					if(closePar != null){
						termL.addAll(getParenthese(nafFile,depTemp.getFrom(),closePar));
					}
				}
				else{
					if(depTemp.getRfunc().matches("^(DET)$")){
						termL.add(depTemp.getFrom());
					}
					else if(depTempL.size() == 1){
						return termL;
					}
				}
			}
			if (onlyFrom){
				return termL;
			}
		}
		return termL;
	}
	
	/**
	 * Get the list of terms inside two brackets.
	 * @param nafFile
	 * @param opP
	 * @param clP
	 * @return
	 */
	private static List<Term> getParenthese (KAFDocument nafFile, Term opP, Term clP){
		List<Term> listTParenth = new ArrayList<Term> ();
		
		List<Annotation> listTerms = nafFile.getBySent(AnnotationType.TERM, opP.getSent());
		HashMap<String,Term> listTermById = new HashMap<String,Term> ();
		for (Annotation t: listTerms){
			listTermById.put(((Term)t).getId(), ((Term)t));
		}
		
		for (int i=Integer.parseInt(opP.getId().replace("t", ""))+1; i < Integer.parseInt(clP.getId().replace("t", "")); i++){
			listTParenth.add(listTermById.get("t"+Integer.toString(i)));
		}
		
		return listTParenth;
	}
	

	/**
	 * Sort a list of term (in the sentence order)
	 * @param listT
	 * @return
	 */
	private static List<Term> sort(List<Term> listT){
		HashMap<Integer,Term> termById = new HashMap<Integer,Term> ();
		
		for (Term t: listT){
			termById.put(Integer.parseInt(t.getId().replace("t", "")), t);
		}
		listT.clear();
		List<Integer> keys = new ArrayList(termById.keySet());
		Collections.sort(keys);
		for (Integer i : keys){
			listT.add(termById.get(i));
		}
		
		return listT;
	}
	
	/**
	 * Add terms missing in a span (eg: if the span is t2 t3 t5, t4 will be added)
	 * @param nafFile
	 * @param listT
	 * @return
	 */
	private static List<Term> complete (KAFDocument nafFile, List<Term> listT){
		List<Term> listTemp = new ArrayList<Term> ();
		
		HashMap<String, Chunk> listChunkByTerm = new HashMap<String,Chunk> ();
		List<Chunk> chunkl = nafFile.getChunks();
		for (Chunk chunk : chunkl){
			for(Term t : chunk.getSpan().getTargets()){
				listChunkByTerm.put(t.getId(), chunk);
			}
		}
		
		if(listT.size() > 0){
			List<Annotation> listTerms = nafFile.getBySent(AnnotationType.TERM, listT.get(0).getSent());
			HashMap<String,Term> termById = new HashMap<String,Term> ();
			for (Annotation t: listTerms){
				termById.put(((Term)t).getId(), ((Term)t));
			}
			
			int previd = Integer.parseInt(listT.get(0).getId().replace("t",""));
			for (Term t : listT){
				if(Integer.parseInt(t.getId().replace("t","")) > previd+1){
					for(int i=previd+1; i<Integer.parseInt(t.getId().replace("t","")); i++){
						listTemp.add(termById.get("t"+Integer.toString(i)));
					}
				}
				previd = Integer.parseInt(t.getId().replace("t",""));
				listTemp.add(t);
			}
			
			if(listChunkByTerm.containsKey(listTemp.get(listTemp.size()-1).getId())){
				List<Term> termChunk = listChunkByTerm.get(listTemp.get(listTemp.size()-1).getId()).getSpan().getTargets();
				if (termChunk.size() > 1){
					boolean findLast = false;
					for (Term t : termChunk){
						if (findLast){
							if (!listTemp.contains(t)){
								listTemp.add(t);
							}
						}
						else if(t.equals(listTemp.get(listTemp.size()-1))){
							findLast = true;
						}
					}
				}
			}
		}
		
		return listTemp;
	}
	
	/**
	 * For each predicate search for its arguments (roles) and add them to the NAF document.
	 * @param nafFile
	 * @param listFrames
	 * @param listTemporalSignal
	 * @param listSpatialSignal
	 * @return
	 */
	private static KAFDocument addArgs (KAFDocument nafFile, List<Frame> listFrames, List<String> listTemporalSignal, List<String> listSpatialSignal){
		//Term id by wf id
		HashMap<String,String> listTermIdWfId = new HashMap<String,String> ();
		HashMap<String,Term> listTermById = new HashMap<String,Term> ();
		List<Term> terms = nafFile.getTerms();
		for(Term t : terms){
			for(WF w : t.getSpan().getTargets()){
				listTermIdWfId.put(w.getId(), t.getId());
			}
			listTermById.put(t.getId(), t);
		}
		
		//Timex3 by term id
		HashMap<String,Timex3> listTimex3ByTerm = new HashMap<String,Timex3> ();
		List<Timex3> txl = nafFile.getTimeExs();
		
		for (Timex3 tx : txl){
			if(tx.hasSpan()){
				List<WF> wfl = tx.getSpan().getTargets();
				for(WF w : wfl){
					listTimex3ByTerm.put(listTermIdWfId.get(w.getId()), tx);
				}
			}
		}
		
		//Chunk by term id
		HashMap<String, Chunk> listChunkByTerm = new HashMap<String,Chunk> ();
		List<Chunk> chunkl = nafFile.getChunks();
		for (Chunk chunk : chunkl){
			for(Term t : chunk.getSpan().getTargets()){
				listChunkByTerm.put(t.getId(), chunk);
			}
		}
		
		//Frames by lemma string
		HashMap<String, List<Frame>> listFrameByLemma = new HashMap<String, List<Frame>> ();
		for (Frame f : listFrames){
			if(! listFrameByLemma.containsKey(f.getLemma().toLowerCase())){
				listFrameByLemma.put(f.getLemma().toLowerCase(),new ArrayList<Frame> ());
			}
			listFrameByLemma.get(f.getLemma().toLowerCase()).add(f);
		}
		
		List<Predicate> predl = nafFile.getPredicates();
		for(Predicate pr : predl){
			
			String lemma = "";
			List<Role> rolePr = new ArrayList<Role> ();
			List<Term> terml = pr.getSpan().getTargets();
			
			Boolean passive = false;
			
			Boolean hasSubj = false;
			
			int endScope = -1;
			
			for (int l = 0; l<terml.size(); l++){
				Term t = terml.get(l);
				lemma += t.getLemma().toLowerCase()+"_";
				List<Dep> depl = nafFile.getDepsByTerm(t);
				
				for (Dep dep : depl){
					Term to = dep.getTo();
					Term from = dep.getFrom();
					String depType = dep.getRfunc();
					//passive
					if(to.equals(t) && depType.equals("PASSIVE")){
						passive = true;
					}
					if(to.equals(t) && depType.equals("END")){
						endScope = Integer.parseInt(from.getId().replace("t", ""));
					}
					
					//from object complement in the dependency tree to predicate arguments
					if((to.equals(t) && depType.equals("DOBJ")) || (to.equals(t) && depType.equals("VERB-INDCOMPL-THEME"))
							 || (to.equals(t) && depType.equals("VERB-INDCOMPL-AGENT"))
							 || (to.equals(t) && depType.equals("VERB-INDCOMPL-LOC"))){
						String semRole = "A1-dobj";
						if(depType.equals("VERB-INDCOMPL-AGENT")){
							semRole = "A0-agent";
						}
						else if(depType.equals("VERB-INDCOMPL-LOC")){
							semRole = "AM-LOC";
						}
						
						if(depType.equals("VERB-INDCOMPL-LOC")){
							Span s = nafFile.newSpan(getSpanArg(nafFile, from));
							Role r = nafFile.newRole(pr, "AM-LOC", s);
							for (Term entT : r.getSpan().getTargets()){
								if (entT.getId().equals(from.getId())){
									r.getSpan().setHead(entT);
								}
							}
							rolePr.add(r);
						}
						//TMP arg
						else if (listTimex3ByTerm.containsKey(from.getId())){
							List<Term> targetL = new ArrayList<Term> ();
							for (WF w : listTimex3ByTerm.get(from.getId()).getSpan().getTargets()){
								targetL.add(listTermById.get(listTermIdWfId.get(w.getId())));
							}
							Span s = nafFile.newSpan(targetL);
							Role rb = nafFile.newRole(pr, "AM-TMP", s);
							rolePr.add(rb);
						}
						else{
							boolean entLoc = false;
							if(nafFile.getEntitiesByTerm(from) != null){
								for(Entity ent : nafFile.getEntitiesByTerm(from)){
									if (ent.getType().startsWith("LOC") || ent.getType().startsWith("GPE")){
										Span s = nafFile.newSpan(ent.getSpans().get(0).getTargets());
										Role r = nafFile.newRole(pr, "AM-LOC", s);
										for (Term entT : r.getSpan().getTargets()){
											if (entT.getId().equals(from.getId())){
												r.getSpan().setHead(entT);
											}
										}
										rolePr.add(r);
										entLoc = true;
									}
								}
							}
							if(!entLoc){
								String typeEnt = "";
							
								if(nafFile.getEntitiesByTerm(from) != null){
									for(Entity ent : nafFile.getEntitiesByTerm(from)){
										typeEnt = ent.getType().substring(0, 3);
										break;
									}
								}
								
								List<Term> listT = getSpanArg(nafFile, from);
								boolean outScope = false;
								if(listT.size() > 0 && endScope > -1 && Integer.parseInt(listT.get(0).getId().replace("t", "")) > endScope){
									outScope = true;
								}
								if (listT.size() > 0 && !outScope){
									Span ss = nafFile.newSpan(listT);
									Role r = nafFile.newRole(pr, semRole+"-"+typeEnt, ss);
									for (Term entT : r.getSpan().getTargets()){
										if (entT.getId().equals(from.getId())){
											r.getSpan().setHead(entT);
										}
									}
									if(containTemporalSignal(listT, listTemporalSignal)){
										r.setSemRole("AM-TMP");
									}
									rolePr.add(r);
								}
								else if(listChunkByTerm.containsKey(from.getId()) && !outScope){
									Span s = listChunkByTerm.get(from.getId()).getSpan();
									for (Term th : listChunkByTerm.get(from.getId()).getSpan().getTargets()){
										if (th.equals(from)){
											s.setHead(th);
										}	
									}
									Role r = nafFile.newRole(pr, semRole+"-"+typeEnt, s);
									if(containTemporalSignal(listT, listTemporalSignal)){
										r.setSemRole("AM-TMP");
									}
									rolePr.add(r);
								}
								
							}
							
						}
					}
					//From a subject complement in the dependency tree to a predicate argument.
					else if(to.equals(t) && depType.equals("SUBJ") || to.equals(t) && depType.equals("VERB-SUBJ")){
						hasSubj = true;
						boolean entPerOrg = false;
						if(depType.equals("VERB-SUBJ")){
							passive = false;
						}
						
						if(nafFile.getEntitiesByTerm(from) != null){
							for(Entity ent : nafFile.getEntitiesByTerm(from)){
								if (ent.getType().startsWith("PER")){
									Span s = nafFile.newSpan(ent.getSpans().get(0).getTargets());
									Role r = nafFile.newRole(pr, "A0-PER", s);
									
									for (Term entT : r.getSpan().getTargets()){
										if (entT.getId().equals(from.getId())){
											r.getSpan().setHead(entT);
										}
									}
									rolePr.add(r);
									entPerOrg = true;
								}
								else if (ent.getType().startsWith("ORG")){
									Span s = nafFile.newSpan(ent.getSpans().get(0).getTargets());
									Role r = nafFile.newRole(pr, "A0-ORG", s);
									
									for (Term entT : r.getSpan().getTargets()){
										if (entT.getId().equals(from.getId())){
											r.getSpan().setHead(entT);
										}
									}
									rolePr.add(r);
									entPerOrg = true;
								}
							}
						}
						if(!entPerOrg){
							
							if (listTimex3ByTerm.containsKey(from.getId())){
								List<Term> targetL = new ArrayList<Term> ();
								for (WF w : listTimex3ByTerm.get(from.getId()).getSpan().getTargets()){
									targetL.add(listTermById.get(listTermIdWfId.get(w.getId())));
								}
								Span s = nafFile.newSpan(targetL);
								Role rb = nafFile.newRole(pr, "AM-TMP", s);
								rolePr.add(rb);
							}
							else{
								List<Term> listT = getSpanArg(nafFile, from);
								if (listT.size() > 0){
									Span ss = nafFile.newSpan(listT);
									Role r = nafFile.newRole(pr, "A0-subj", ss);
									
									for (Term entT : r.getSpan().getTargets()){
										if (entT.getId().equals(from.getId())){
											r.getSpan().setHead(entT);
										}
									}
									
									if(containTemporalSignal(listT, listTemporalSignal)){
										r.setSemRole("AM-TMP");
									}
									rolePr.add(r);
								}
								else if(listChunkByTerm.containsKey(from.getId())){
									Span s = listChunkByTerm.get(from.getId()).getSpan();
									for (Term th : listChunkByTerm.get(from.getId()).getSpan().getTargets()){
										if (th.equals(from)){
											s.setHead(th);
										}
									}
									Role r = nafFile.newRole(pr, "A0-subj", s);
									
									if(containTemporalSignal(listT, listTemporalSignal)){
										r.setSemRole("AM-TMP");
									}
									rolePr.add(r);
								}
							}
						}
					}
					//convert predicative into predicate argument
					else if(to.equals(t) && depType.equals("PRED") && ! hasRole(rolePr,"ARG-PRX")){
						List<Term> listT = getSpanArg(nafFile, from);
						if (listT.size() > 0){
							Span s = nafFile.newSpan(listT);
							Role r = nafFile.newRole(pr, "ARG-PRX", s);
							
							for (Term entT : r.getSpan().getTargets()){
								if (entT.getId().equals(from.getId())){
									r.getSpan().setHead(entT);
								}
							}
							rolePr.add(r);
						}
					}
					//convert relative clause modifier into predicate argument
					else if(from.equals(t) && depType.equals("RELCL")){
						String semRole = "A1-relcl";
						if(listChunkByTerm.containsKey(to.getId())){
							Span s = listChunkByTerm.get(to.getId()).getSpan();
							for (Term th : listChunkByTerm.get(to.getId()).getSpan().getTargets()){
								if (th.equals(to)){
									s.setHead(th);
								}
							}
							if(passive){
								semRole = "A0-relcl";
							}
							Role r = nafFile.newRole(pr, semRole, s);
							rolePr.add(r);
						}
						else{
							List<Term> listT = new ArrayList<Term> ();
							listT.add(to);
							Span s = nafFile.newSpan(listT);
							s.setHead(to);
							
							if(passive){
								semRole = "A0-relcl";
							}
							Role r = nafFile.newRole(pr, semRole, s);
							rolePr.add(r);
						}
					}
					//Convert restrictive modifiers into predicate argument
					else if(to.equals(t) && depType.equals("RMOD") && !from.getPos().startsWith("XP") && !from.getPos().equals("C")){
						List<Term> listT = getSpanArg(nafFile, from);
						boolean outScope = false;
						if(listT.size() > 0 && endScope > -1 && Integer.parseInt(listT.get(0).getId().replace("t", "")) > endScope){
							outScope = true;
						}
						
						List<Term> targetL = new ArrayList<Term> ();
						if (listT.size() > 0 && !outScope){
							termInTx:for (Term ttx : listT){
								if (listTimex3ByTerm.containsKey(ttx.getId())){
									for (WF w : listTimex3ByTerm.get(ttx.getId()).getSpan().getTargets()){
										targetL.add(listTermById.get(listTermIdWfId.get(w.getId())));
									}
									break termInTx;
								}
							}
							if (targetL.size()>0){
								Span s = nafFile.newSpan(targetL);
								Role rb = nafFile.newRole(pr, "AM-TMP", s);
								rolePr.add(rb);
							}
							else if(t.getPos().equals("SS") || t.getPos().equals("SP")){
								if (listT.size() > 0 && (listT.size() > 1 
										//adjective alone cannot be an argument, e.g. "grandi" for the predicate "ordini" in "due grandi ordini per ..."
										|| (listT.size() == 1 && !listT.get(0).getPos().equals("AP") && !listT.get(0).getPos().equals("AS")))){
									Span s = nafFile.newSpan(listT);
									Role r = nafFile.newRole(pr, "A1-rmod-noun", s);
									
									for (Term entT : r.getSpan().getTargets()){
										if (entT.getId().equals(from.getId())){
											r.getSpan().setHead(entT);
										}
									}
									if(containTemporalSignal(listT, listTemporalSignal)){
										r.setSemRole("AM-TMP");
									}
									rolePr.add(r);
								}
							}
							else if(listT.size() == 1 && listT.get(0).getPos().equals("B")){
								Span s = nafFile.newSpan(listT);
								Role r = nafFile.newRole(pr, "AM-MNR", s);
								
								for (Term entT : r.getSpan().getTargets()){
									if (entT.getId().equals(from.getId())){
										r.getSpan().setHead(entT);
									}
								}
								rolePr.add(r);
							}
							else{
								if (listT.size() > 0){
									Span s = nafFile.newSpan(listT);
									Role r = nafFile.newRole(pr, "A1-rmod", s);
									
									for (Term entT : r.getSpan().getTargets()){
										if (entT.getId().equals(from.getId())){
											r.getSpan().setHead(entT);
										}
									}
									
									if(containTemporalSignal(listT, listTemporalSignal)){
										r.setSemRole("AM-TMP");
									}
									rolePr.add(r);
								}
							}
						}
					}
					else if(to.equals(t) && depType.equals("RMOD") 
							&& (from.getPos().startsWith("XP") || from.getPos().equals("C"))
							&& ! isBeginSent(from, nafFile.getSentenceTerms(from.getSent()))
							&& Integer.parseInt(from.getId().replace("t", "")) > Integer.parseInt(t.getId().replace("t", ""))+2
							){
						break;
					}
				}
			}
			
			
			//if predicate has no SUBJ dep but another term of his chunk has one, then add SUBJ as A0 of the predicate
			if(!hasRole(rolePr, "A0") && !hasSubj && listChunkByTerm.containsKey(pr.getSpan().getTargets().get(0).getId())
					&& listChunkByTerm.get(pr.getSpan().getTargets().get(0).getId()).getPhrase().equals("VX")){
				Role r = getRoleChunkPred(pr, listChunkByTerm.get(pr.getSpan().getTargets().get(0).getId()), nafFile, "SUBJ");
				if(r != null){
					rolePr.add(r);
				}
			}
			
			lemma = lemma.substring(0,lemma.length()-1).toLowerCase();
			
			List<Frame> lemmaFrames = new ArrayList<Frame> ();
			
			if(listFrameByLemma.containsKey(lemma)){
				lemmaFrames = listFrameByLemma.get(lemma);
			}
			
			List<Term> predT = pr.getTerms();
			boolean propbankSense = false;

			//Select the frame which fit the best with the arguments of the predicate. 
			Frame predF = null;
			if(lemmaFrames.size() > 0){
				if(lemmaFrames.size() == 1){
					predF = lemmaFrames.get(0);
				}
				else{
					predF = getCommonFrame(rolePr, lemmaFrames);
				}
				if(predF != null && predF.getFramenet() != null){
					for(String wnid : predF.getFramenet()){
						ExternalRef ref = nafFile.newExternalRef("framenet", wnid);
						pr.addExternalRef(ref);
					}
					
					ExternalRef refPb = nafFile.newExternalRef("PropBank", predF.getRoleSetId());
					pr.addExternalRef(refPb);
					
					propbankSense = true;
				}
			}
			
			if(!propbankSense){
				pr.addExternalRef(nafFile.newExternalRef("PropBank", lemma+".01"));
			}

			//Remove arguments, for example if it includes the predicate
			List<Integer> listRoleToRemove = new ArrayList<Integer> ();
			
			for (int i=0; i<rolePr.size(); i++){
				
				if(!listRoleToRemove.contains(i)){
				Role r = rolePr.get(i);
				List<Term> roleT = r.getTerms();
				boolean add = true;
				
				//argument include the predicate
				for (Term t : predT){
					if (roleT.contains(t)){
						add = false;
					}
				}
				
				if(add){
					for (int j=0; j<rolePr.size(); j++){
						//argument is included in another
						if(j != i && r.getSemRole().equals(rolePr.get(j).getSemRole()) && roleT.size() < rolePr.get(j).getTerms().size()){
							for (Term t : roleT){
								if (rolePr.get(j).getTerms().contains(t)){
									add = false;
									break;
								}
							}
						}
						
						if(j > i && r.getSemRole().startsWith("A0") && rolePr.get(j).getSemRole().startsWith("A0")){
							int predPosition = Integer.parseInt(pr.getSpan().getTargets().get(0).getId().replace("t",""));
							int firstArgPosition = Integer.parseInt(r.getSpan().getTargets().get(r.getSpan().getTargets().size()/2).getId().replace("t", ""));
							int secondArgPosition = Integer.parseInt(rolePr.get(j).getSpan().getTargets().get(rolePr.get(j).getSpan().getTargets().size()/2).getId().replace("t", ""));
							if(predPosition < firstArgPosition && predPosition > secondArgPosition){
								r.setSemRole("A1-subj");
							}
							else if(predPosition < firstArgPosition && predPosition > secondArgPosition){
								rolePr.get(j).setSemRole("A1-subj");
							}
							else if(predPosition - firstArgPosition > predPosition - secondArgPosition){
								r.setSemRole("A1-subj");
							}
							else{
								rolePr.get(j).setSemRole("A1-subj");
							}
						}
						else if(j > i && r.getSemRole().startsWith(rolePr.get(j).getSemRole().substring(0,2))
								&& !rolePr.get(j).getSemRole().startsWith("A1-dobj")
								&& !r.getSemRole().startsWith("A1-dobj")
								&& Integer.parseInt(roleT.get(roleT.size()-1).getId().replace("t", ""))+1 ==
								Integer.parseInt(rolePr.get(j).getTerms().get(0).getId().replace("t",""))){
							for (Term ttadd : rolePr.get(j).getTerms()){
								if(! terml.contains(ttadd) && ! isInRole(rolePr, j, ttadd)){
									roleT.add(ttadd);
								}
								else{
									break;
								}
							}
							listRoleToRemove.add(j);
						}
					
						else if(j > i && rolePr.get(j).getSemRole().startsWith("A1") && rolePr.get(j).getTerms().size() < 3	
							&& ! rolePr.get(j).getSemRole().startsWith("A1-dobj")
							&& ! r.getSemRole().startsWith("A1-dobj")
							&& (Integer.parseInt(roleT.get(roleT.size()-1).getId().replace("t", "")) ==
							Integer.parseInt(rolePr.get(j).getTerms().get(0).getId().replace("t",""))
							|| (roleT.size() > 1 && Integer.parseInt(roleT.get(roleT.size()-2).getId().replace("t", "")) ==
									Integer.parseInt(rolePr.get(j).getTerms().get(0).getId().replace("t",""))))){
							listRoleToRemove.add(j);
						}
						
						else if(j > i && rolePr.get(j).getSemRole().equals("AM-TMP") && r.getSemRole().equals("AM-TMP")){
							listRoleToRemove.add(j);
						}
					}
					
					for(int k=listRoleToRemove.size()-1; k>=0; k--){
						rolePr.remove(listRoleToRemove.get(k));
					}
				}
				
				//Convert semrole name into Propbank one (A1, A2, AM-LOC, etc.)
				if(add && predF != null){
					if(r.getSemRole().endsWith("LOC") || containSpatialSignal(r.getSpan().getTargets(), listSpatialSignal)){
						if(predF.getArg1() != null && (predF.getArg1().contains("location") || predF.getArg1().contains("source") || predF.getArg1().contains("destination"))){
							r.setSemRole("A1");
						}
						else if(predF.getArg2() != null && (predF.getArg2().contains("location") || predF.getArg2().contains("source") || predF.getArg2().contains("destination"))){
							r.setSemRole("A2");
						}
						else{
							boolean argFind = false;
							for(String role : predF.getArgM().keySet()){
								if(predF.getArgM().get(role).equals("location")){
									r.setSemRole("A"+role);
									argFind = true;
								}
							}
							if(!argFind){
								r.setSemRole("AM-LOC");
							}
						}
					}
					else if(r.getSemRole().startsWith("A0") && predF.getArg0() != null && !hasRole(pr.getRoles(),"A0")){
						r.setSemRole("A0");
					}
					else if(r.getSemRole().startsWith("A0") && predF.getArg0() != null && hasRole(pr.getRoles(),"A0")){
						add = false;
					}
					else if(r.getSemRole().startsWith("A0") && predF.getArg0() == null){
						r.setSemRole("A1");
					}
					else if(r.getSemRole().endsWith("rmod") && hasRole(rolePr,"A1")){
						r.setSemRole("A2");
					}
					else if(r.getSemRole().startsWith("A1") && predF.getArg1() != null && predF.getArg0() != null){
						r.setSemRole("A1");
					}
					else if(r.getSemRole().startsWith("A1") && predF.getArg2() != null && predF.getArg0() == null){
						r.setSemRole("A2");
					}
					else if(r.getSemRole().startsWith("A1") && predF.getArg1() != null){
						r.setSemRole("A1");
					}
				}
				else if(add){
					if(r.getSemRole().startsWith("A0")){ 
						r.setSemRole("A0");
					}
					else if(r.getSemRole().startsWith("A1")){ 
						r.setSemRole("A1");
					}
				}
				
				
				
				if (add){
					pr.addRole(r);
				}
				}
			}
		}
		return nafFile;	
	}
	
	/**
	 * Return true if the term is one of the first 4 of the sentence. 
	 * @param from
	 * @param listT
	 * @return
	 */
	private static Boolean isBeginSent(Term from, List<Term> listT){
		for (int i=0; i<listT.size(); i++){
			if(listT.get(i).getId().equals(from.getId())
					&& i < 4){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Return true if a given argument/role contains a term
	 * @param rolePr
	 * @param j
	 * @param ttadd
	 * @return
	 */
	private static Boolean isInRole(List<Role> rolePr, int j, Term ttadd){
		for(int i=0; i<j; i++){
			if(rolePr.get(i).getTerms().contains(ttadd)){
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Return true if a span contains a temporal signal
	 * @param listTerms
	 * @param listSignals
	 * @return
	 */
	private static boolean containTemporalSignal(List<Term> listTerms, List<String> listSignals){
		for (int i=0; i<listTerms.size() && i<3; i++){
			Term t = listTerms.get(i);
			if(listSignals.contains(t.getId())){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Return true if a span contains a spatial signal
	 * @param listTerms
	 * @param listSignals
	 * @return
	 */
	private static boolean containSpatialSignal(List<Term> listTerms, List<String> listSignals){
		if(listSignals.contains(listTerms.get(0).getId())){
			return true;
		}
		return false;
	}
	
	/**
	 * Add an arg0 to the predicate if a term inside his phrase (chunk) has a SUBJ in the dependency tree.
	 * @param pr
	 * @param c
	 * @param nafFile
	 * @param depFunc
	 * @return
	 */
	private static Role getRoleChunkPred(Predicate pr, Chunk c, KAFDocument nafFile, String depFunc){
		Role r = null;
		
		for(Term t : c.getSpan().getTargets()){
			if(! pr.getSpan().getTargets().contains(t)){
				List<Dep> deps = nafFile.getDepsByTerm(t);
				for(Dep d : deps){
					if(d.getRfunc().equals(depFunc)){
						List<Term> listT = getSpanArg(nafFile, d.getFrom());
						if (listT.size() > 0){
							Span ss = nafFile.newSpan(listT);
							r = nafFile.newRole(pr, "A0-subj", ss);
							for (Term entT : r.getSpan().getTargets()){
								if (entT.getId().equals(d.getFrom().getId())){
									r.getSpan().setHead(entT);
								}
							}
						}
					}
				}
			}
		}
		
		return r;
	}
	
	/**
	 * Convert WN code from WN1.6 to WN3.0
	 * @param nafFile
	 * @param wnMappings
	 * @return
	 */
	private static KAFDocument correctWNid (KAFDocument nafFile, HashMap<String,String> wnMappings){
		List<Predicate> predL = nafFile.getPredicates();
		for(Predicate pr : predL){
			List<ExternalRef> extRefL = pr.getExternalRefs();
			List<ExternalRef> newExtRef = new ArrayList<ExternalRef> ();
			boolean wn16 = false;
			List<String> wnili = new ArrayList<String> ();
			for (ExternalRef ref : extRefL){
				if (ref.getResource().equals("WordNet")){
					wnili.add(ref.getReference());
				}
				if (ref.getResource().equals("framenet")){
					if(wnMappings.containsKey(ref.getReference())){
						ExternalRef wn30Ref = nafFile.newExternalRef("WordNet", wnMappings.get(ref.getReference()));
						newExtRef.add(wn30Ref);
					}
					wn16 = true;
				}
			}
			if(wn16 && newExtRef.size() > 0){
				for(int i=extRefL.size()-1; i>=0; i--){
					if(extRefL.get(i).getResource().equals("framenet") || extRefL.get(i).getResource().equals("WordNet")){
						pr.getExternalRefs().remove(i);
					}
				}
				pr.addExternalRefs(newExtRef);
			}
			else if(wn16){
				for(int i=extRefL.size()-1; i>=0; i--){
					if(extRefL.get(i).getResource().equals("framenet")){
						pr.getExternalRefs().remove(i);
					}
				}
			}
		}
		
		return nafFile;
	}
	
	/**
	 * Get the frames that fit the best with the list of roles of a predicate.
	 * @param rolesL
	 * @param frames
	 * @return
	 */
	private static Frame getCommonFrame (List<Role> rolesL, List<Frame> frames){
		HashMap<Frame,Integer> listFrameScore = new HashMap<Frame,Integer> ();
		for(Frame f : frames){
			int score = 0;
			boolean hasA1 = false;
			for (Role r : rolesL){
				if (r.getSemRole().startsWith("A0") && f.getArg0()  != null){
					if((r.getSemRole().equals("A0-PER") || r.getSemRole().equals("A0-ORG")) && 
							(f.getArg0().contains("agent") || f.getArg0().contains("experiencer"))){
						score += 1;
					}
					score += 1;
				}
				else if(!hasA1 && r.getSemRole().startsWith("A0") && f.getArg1() != null){
					if((r.getSemRole().endsWith("PER") || r.getSemRole().endsWith("ORG")) && 
							(f.getArg1().contains("patient") || f.getArg1().contains("theme"))){
						score += 1;
					}
					score += 1;
					hasA1 = true;
				}
				else if(!hasA1 && r.getSemRole().startsWith("A1") && f.getArg1() != null){
					if((r.getSemRole().endsWith("PER") || r.getSemRole().endsWith("ORG")) && 
							(f.getArg1().contains("patient") || f.getArg1().contains("theme"))){
						score += 1;
					}
					
					score += 1;
					hasA1 = true;
				}
				else if(r.getSemRole().startsWith("A1") && f.getArg2() != null){
					if((r.getSemRole().endsWith("PER") || r.getSemRole().endsWith("ORG")) && 
							(f.getArg2().contains("patient") || f.getArg2().contains("theme"))){
						score += 0.5;
					}

					score += 0.5;
				}
				else if(r.getSemRole().startsWith("AM-LOC") && 
						((f.getArg1() != null && f.getArg1().contains("location")) || (f.getArg2() != null && f.getArg2().contains("location"))
						|| (f.getArg1() != null && f.getArg1().contains("source")) || (f.getArg2() != null && f.getArg2().contains("source"))
						|| (f.getArg1() != null && f.getArg1().contains("destination")) || (f.getArg2() != null && f.getArg2().contains("destination")))){
					score += 2;
				}
				else if(f.getArgM().size() > 0 && ((r.getSemRole().startsWith("AM-TMP")) 
						|| (r.getSemRole().startsWith("AM-LOC") && frameHasArgM(f.getArgM(), "AM-LOC")))){
					score += 2;
				}
				else if(r.getSemRole().startsWith("ARG-PRX") && ((f.getArg1() != null && f.getArg1().contains("predicate"))
						|| (f.getArg2() != null && f.getArg2().contains("predicate")))){
					score += 3;
				}
			}
			listFrameScore.put(f,score);
		}
		
		int max = 0;
		Frame selectF = null;
		for (Frame f : listFrameScore.keySet()){
			if (max < listFrameScore.get(f)){
				max = listFrameScore.get(f);
				selectF = f;
			}
		}
		return selectF;
	}
	
	/**
	 * Return true if the given role is "AM-LOC" and the given frame has an argument of type location, source or destination, or if the given role is "AM-TMP".
	 * @param argM
	 * @param semRole
	 * @return
	 */
	private static boolean frameHasArgM (HashMap<String,String> argM, String semRole){
		for(String role : argM.keySet()){
			if(semRole.equals("AM-LOC") && argM.get(role).matches("((location)|(source)|(destination))")){
				return true;
			}
			else if(semRole.equals("AM-TMP") && argM.get(role).equals("")){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Return true if a list of roles contains the given role.
	 * @param listRoles
	 * @param role
	 * @return
	 */
	private static boolean hasRole(List<Role> listRoles, String role){
		for (Role r : listRoles){
			if (r.getSemRole().startsWith(role)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Read the file containing the mapping between WN1.6 and WN3.0 senses.
	 * @param repName
	 * @return
	 */
	private static HashMap<String,String> readMappingWN1630 (String repName){
		HashMap<String,String> mapping = new HashMap<String,String> ();
		try{
			for (final File fileEntry : (new File (repName)).listFiles()) {
				BufferedReader bf = new BufferedReader(new FileReader(fileEntry));
				
				String pos = "";
				if (fileEntry.getName().endsWith("verb")){
					pos = "v";
				}
				else if (fileEntry.getName().endsWith("adj")){
					pos = "a";
				}
				else if (fileEntry.getName().endsWith("adv")){
					pos = "r";
				}
				else if (fileEntry.getName().endsWith("noun")){
					pos = "n";
				}
				
				String strline = "";
				while((strline = bf.readLine()) != null){
					if(strline.contains(" ")){
						String [] map = strline.split(" ");
						mapping.put(pos+"#"+map[0], "ili-30-"+map[1]+"-"+pos);
					}
				}	
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		return mapping;
	}
	
	/**
	 * Read the PropBank-like frames
	 * @param repName
	 * @return
	 */
	private static List<Frame> readFrameFolder(String repName){
		List<Frame> listFrames = new ArrayList<Frame> ();
		try{
			
			for (final File fileEntry : (new File (repName)).listFiles()) {
				if(fileEntry.getName().endsWith(".xml")){
				File fXmlFile = new File(repName+fileEntry.getName());
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
				
				
				Node predicate = doc.getElementsByTagName("predicate").item(0);
				NodeList rolesetL = doc.getElementsByTagName("roleset");
				for(int i=0; i<rolesetL.getLength(); i++){
					Node roleset = rolesetL.item(i);
					
					Frame f = new Frame();
					f.setLemma(predicate.getAttributes().getNamedItem("lemma").getTextContent());
					
					f.setRoleSetId(roleset.getAttributes().getNamedItem("id").getTextContent());
					
					NodeList rolesetInfo = roleset.getChildNodes();
					
					for(int j=0; j<rolesetInfo.getLength(); j++){
						Node child = rolesetInfo.item(j);
						if(child.getNodeName().equals("aliases")){
							Node alias = child.getChildNodes().item(1);
							if(alias.hasAttributes() && alias.getAttributes().getNamedItem("framenet") != null){
								String [] frameid = alias.getAttributes().getNamedItem("framenet").getTextContent().split(", ");
								List<String> frameidL = new ArrayList<String> ();
								for (String fid : frameid){
									frameidL.add(fid);
								}
								f.setFramenet(frameidL);
							}
						}
						else if(child.getNodeName().equals("roles")){
							NodeList rolesL = child.getChildNodes();
							for (int k=0; k<rolesL.getLength(); k++){
								Node role = rolesL.item(k);
								NodeList vnroleL = role.getChildNodes();
								
								List<String> vnthetaL = new ArrayList<String> ();
								for (int l=0; l<vnroleL.getLength(); l++){
									Node vnrole = vnroleL.item(l);
									if(vnrole.hasAttributes()){
										vnthetaL.add(vnrole.getAttributes().getNamedItem("vntheta").getTextContent());
									}
								}
								if(role.hasAttributes() && role.getAttributes().getNamedItem("n").getTextContent().equals("0")){
									f.setArg0(vnthetaL);
								}
								else if(role.hasAttributes() && role.getAttributes().getNamedItem("n").getTextContent().equals("1")){
									f.setArg1(vnthetaL);
								}
								else if(role.hasAttributes() && role.getAttributes().getNamedItem("n").getTextContent().equals("2")){
									f.setArg2(vnthetaL);
								}
								else{
									if(f.getArgM() == null){
										HashMap<String,String> temp = new HashMap<String,String>();
										f.setArgM(temp);
									}
									if (role.hasAttributes()){
										f.getArgM().put(role.getAttributes().getNamedItem("n").getTextContent(), vnthetaL.get(0));
									}
								}
							}
						}
					}
					listFrames.add(f);
				}
			}
			}
			
		} catch (Exception e){
			e.printStackTrace();
		}
		
		return listFrames;
		
	}
	
	/**
	 * Read the file that contains a list of signals
	 * @param fileName
	 * @return
	 */
	private static List<String> readSignalList (String fileName){
		List<String> listSignal = new ArrayList<String>();
		
		try{
			BufferedReader bf = new BufferedReader(new FileReader(fileName));
			String strline = "";
			while((strline = bf.readLine()) != null){
				listSignal.add(strline.replace("\n", ""));
			}
			
		} catch (Exception e){
			e.printStackTrace();
		}
		
		return listSignal;
	}
	
	/**
	 * Get the list index of the role which has the given term in his span.
	 * @param pr
	 * @param t
	 * @return
	 */
	private static Integer getRoleSpanArg (Predicate pr, Term t){
		List<Role> roles = pr.getRoles();
		for(int i=0; i<roles.size(); i++){
			if(roles.get(i).getSpan().getTargets().size() == 1 && roles.get(i).getSpan().getTargets().get(0).equals(t)){
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Get time in string
	 * @return
	 */
	public static String getTodayDate (){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		Date date = new Date();
		String dateString = dateFormat.format(date).toString();
		return dateString;
	}
	
	/**
	 * Add arguments of type modifiers to the predicates. 
	 * @param nafFile
	 * @return
	 */
	private static KAFDocument addModifiers (KAFDocument nafFile){
		
		List<Predicate> predL = nafFile.getPredicates();
		for (Predicate pr : predL){
			Term prTerm = pr.getSpan().getTargets().get(0);
			List<Dep> depL = nafFile.getDepsByTerm(prTerm);
			for(Dep d : depL){
				if (d.getFrom().equals(prTerm) && d.getRfunc().equals("VERB")
						&& d.getTo().getLemma().matches("((potere)|(volere)|(dovere)|(sapere))")){
					
					int j = getRoleSpanArg(pr,d.getTo());
					if(j > -1){
						pr.getRoles().get(j).setSemRole("AM-MOD");
					}
					else{
						Span spanMod = nafFile.newSpan();
						spanMod.addTarget(d.getTo());
						Role r = nafFile.newRole(pr, "AM-MOD", spanMod);
						pr.addRole(r);
					}
				}
				else if(d.getTo().equals(prTerm) && d.getRfunc().equals("RMOD")
						&& d.getFrom().getLemma().matches(
								"((non)|(mai)|(nessuno)|(niente)|(nulla)|(nessun)|(né)|(neanche)|(nemmeno)|(neppure))"
								)){
					int j = getRoleSpanArg(pr,d.getFrom());
					if(j > -1){
						pr.getRoles().get(j).setSemRole("AM-NEG");
					}
					else{
						Span spanMod = nafFile.newSpan();
						spanMod.addTarget(d.getFrom());
						Role r = nafFile.newRole(pr, "AM-NEG", spanMod);
						pr.addRole(r);
					}
				}
			}
		}
		
		return nafFile;
	}
	
	/**
	 * get list term id that match with signal in listSignal
	 * @param nafFile
	 * @param listSignal
	 * @return
	 */
	private static List<String> getTermIdSignal (KAFDocument nafFile, List<String> listSignal){
		List<String> termIdSignal = new ArrayList<String> ();
		
		List<Term> listTerms = nafFile.getTerms();
		
		HashMap<String,List<String>> listSignalByLemma = new HashMap<String,List<String>> ();
		for(String sig : listSignal){
			if(sig.contains(" ")){
				String [] tok = sig.split(" ");
				for(String t : tok){
					if(!listSignalByLemma.containsKey(t)){
						List<String> temp = new ArrayList<String> ();
						listSignalByLemma.put(t,temp);
					}
					listSignalByLemma.get(t).add(sig);
				}
			}
			else{
				if(!listSignalByLemma.containsKey(sig)){
					List<String> temp = new ArrayList<String> ();
					listSignalByLemma.put(sig,temp);
				}
				listSignalByLemma.get(sig).add(sig);
			}
		}
		
		for (int i=0; i<listTerms.size(); i++){
			Term t = listTerms.get(i);
				if(!termIdSignal.contains(t.getId())){
				
				String lemma = t.getLemma();
				if(lemma.contains("/")){
					lemma = lemma.substring(0, lemma.indexOf("/"));
				}
				lemma = lemma.toLowerCase();
				
				if(listSignalByLemma.containsKey(lemma)){
					for(String sig : listSignalByLemma.get(lemma)){
						if(sig.contains(" ")){
							int nbTok = sig.split(" ").length;
							String stringLemma = lemma;
							for(int j=i+1; j<i+nbTok && j<listTerms.size(); j++){
								stringLemma += " "+listTerms.get(j).getLemma().replaceAll("/.*$","");
							}
							if(stringLemma.equals(sig)){
								for(int j=i; j<i+nbTok; j++){
									termIdSignal.add(listTerms.get(j).getId());
								}
							}
						}
						else{
							termIdSignal.add(t.getId());
						}
					}
				}
			}
		}
		
		return termIdSignal;
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		KAFDocument nafFile = KAFDocument.createFromFile(new File(args[0]));
		
		LinguisticProcessor lp = nafFile.addLinguisticProcessor("srl","FBK-SRL","v0.1, Aug 2015");
		lp.setBeginTimestamp(getTodayDate());
		
		List<Frame> listFrames = readFrameFolder(args[2]);
		HashMap<String,String> wnMappings = readMappingWN1630(args[3]);
		List<String> temporalSignal = new ArrayList<String> ();
		List<String> spatialSignal = new ArrayList<String> ();
		temporalSignal = readSignalList(args[4]);
		spatialSignal = readSignalList(args[5]);
		
		List<String> termIdTemporalSignal = getTermIdSignal (nafFile, temporalSignal);
		List<String> termIdSpatialSignal = getTermIdSignal (nafFile, spatialSignal);
		
		nafFile = addArgs(nafFile, listFrames, termIdTemporalSignal, termIdSpatialSignal);
		nafFile = correctWNid(nafFile, wnMappings);
		
		nafFile = addModifiers(nafFile);
		
		lp.setEndTimestamp(getTodayDate());
		
		nafFile.save(args[1]);
	}

}
