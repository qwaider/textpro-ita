package eu.fbk.newsreader.NAF.SRL;

import java.util.HashMap;
import java.util.List;

public class Frame {
	String lemma;
	List<String> framenet;
	List<String> arg0;
	List<String> arg1;
	List<String> arg2;
	HashMap<String,String> argM;
	String roleSetId;
	
	public String getLemma (){
		return this.lemma;
	}
	
	public List<String> getFramenet (){
		return this.framenet;
	}
	
	public List<String> getArg0 (){
		return this.arg0;
	}

	public List<String> getArg1 (){
		return this.arg1;
	}

	public List<String> getArg2 (){
		return this.arg2;
	}

	public HashMap<String,String> getArgM (){
		return this.argM;
	}
	
	public String getRoleSetId(){
		return this.roleSetId;
	}
	
	public void setLemma (String lemma){
		this.lemma = lemma;
	}
	
	public void setFramenet (List<String> framenet){
		this.framenet = framenet;
	}
	
	public void setArg0 (List<String> arg0){
		this.arg0 = arg0;
	}

	public void setArg1 (List<String> arg1){
		this.arg1 = arg1;
	}

	public void setArg2 (List<String> arg2){
		this.arg2 = arg2;
	}

	public void setArgM (HashMap<String,String> argM){
		this.argM = argM;
	}
	
	public void setRoleSetId(String roleSetId){
		this.roleSetId = roleSetId;
	}
		
}
