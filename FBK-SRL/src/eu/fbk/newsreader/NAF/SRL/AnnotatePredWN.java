package eu.fbk.newsreader.NAF.SRL;

/**
 * Author: minard@fbk.eu
 * Date: Aug 2015
 * Description: Add to each predicate the list of possible wordnet senses (from MultiWordNet) as externalReferences.
 * Lang: Italian
 * Usage: java -cp lib/kaflib-naf-1.1.9.jar:lib/jdom-2.05.jar eu.fbk.newsreader.NAF.SRL.AnnotatedPredWN input output resources/wn-data-ita.tab
 */

import ixa.kaflib.ExternalRef;
import ixa.kaflib.KAFDocument;
import ixa.kaflib.Predicate;
import ixa.kaflib.Term;
import ixa.kaflib.KAFDocument.LinguisticProcessor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class AnnotatePredWN {

	/**
	 * Add to each predicate the list of possible wordnet senses as externalReferences.
	 * @param nafFile
	 * @param wnFile
	 */
	private static void addWN (KAFDocument nafFile, String wnFile){
		HashMap<String, List<String>> wn_ili = readWNFile(wnFile);
		HashMap<String, String> wn_ili_non_ambig = getNonAmbiguousLemma(wn_ili);
		
		for(Predicate pred : nafFile.getPredicates()){

			String pos = "";
			String lemma = "";
			
			for(Term t : pred.getSpan().getTargets()){
				if (t.getPos() != null){
					if(t.getPos().startsWith("V")){
						pos = "v";
					}
					else if(t.getPos().startsWith("S")){
						pos = "n";
					}
					else if(t.getPos().startsWith("A")){
						pos = "a";
					}
					else if(t.getPos().startsWith("B")){
						pos = "r";
					}
					if(pred.getSpan().isHead(t)){
						break;
					}
				}
				
				if(t.getLemma().contains("/rifl")){
					String tempLemma = t.getLemma();
					tempLemma = tempLemma.substring(0,tempLemma.indexOf("/")-1);
					tempLemma = tempLemma+"si";
					lemma += tempLemma+" ";
				}
				else if(t.getLemma().contains("/pro")){
					String tempLemma = t.getLemma().substring(0,t.getLemma().indexOf("/")-1);
					lemma += tempLemma+" ";
				}
				else{
					lemma += t.getLemma()+" ";
				}
				lemma = lemma.toLowerCase().substring(0,lemma.length()-1);
			}
				
			if (!lemma.equals("") && wn_ili_non_ambig.containsKey(lemma)){
				if(wn_ili_non_ambig.get(lemma).endsWith(pos)){
					String ili_ref = "ili-30-"+wn_ili_non_ambig.get(lemma);
					ExternalRef ref = nafFile.createExternalRef("WordNet", ili_ref);
					pred.addExternalRef(ref);
				}
			}
			else if(!lemma.equals("") && wn_ili.containsKey(lemma)){
				for (String ili : wn_ili.get(lemma)){
					if(ili.endsWith(pos)){
						String ili_ref = "ili-30-"+ili;
						ExternalRef ref = nafFile.createExternalRef("WordNet", ili_ref);
						pred.addExternalRef(ref);
					}
				}
			}
		}
	}
	
	/**
	 * Read the file of multiwordnet available at http://compling.hss.ntu.edu.sg/omw/
	 * Build hash: key --> lemma, value --> list of wordnet sense
	 * @param wnFile
	 * @return
	 */
	private static HashMap<String, List<String >> readWNFile(String wnFile){
		HashMap<String, List<String>> wn_ili = new HashMap<String,List<String>> ();
		
		try{
			InputStream ips=new FileInputStream(wnFile); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String line;
			while ((line=br.readLine())!=null){
				if (line.contains("\t") && line.contains("ita:lemma")){
					String [] elts = line.split("\t");
					if(!wn_ili.containsKey(elts[2])){
						wn_ili.put(elts[2], new ArrayList<String> ());
					}
					wn_ili.get(elts[2]).add(elts[0]);
				}
			}
		} catch (Exception e){
			System.out.println(e.toString());
		}
		
		return wn_ili;
		
	}
	
	/**
	 * from a hash containing list of lemmas and their ili codes
	 * @param wn_ili
	 * @return
	 */
	private static HashMap<String, String> getNonAmbiguousLemma (HashMap<String, List<String>> wn_ili){
		HashMap<String, String> wn_ili_non_ambig = new HashMap<String,String> ();
		
		for (String lemma : wn_ili.keySet()){
			if(wn_ili.get(lemma).size()==1){
				wn_ili_non_ambig.put(lemma,wn_ili.get(lemma).get(0));
			}
		}
		
		return wn_ili_non_ambig;
		
	}
	
	/**
	 * get time in string
	 * @return
	 */
	public static String getTodayDate (){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		Date date = new Date();
		String dateString = dateFormat.format(date).toString();
		return dateString;
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		KAFDocument nafFile = KAFDocument.createFromFile(new File(args[0]));
		
		LinguisticProcessor lp = nafFile.addLinguisticProcessor("srl","FBK-SRL-WN","v0.1, Aug 2015");
		lp.setBeginTimestamp(getTodayDate());
		
		addWN(nafFile, args[2]);
		
		lp.setEndTimestamp(getTodayDate());
		
		nafFile.save(args[1]);
		
	}

}
