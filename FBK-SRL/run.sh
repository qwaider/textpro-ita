#!/bin/bash
#rootDir=/home/newsreader/components-it/FBK-SRL

scratchDir=/tmp

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink 
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR

rootDir=$DIR

RANDOM=`bash -c 'echo $RANDOM'`

RANDOMFILE=$scratchDir/$RANDOM

cat > $RANDOMFILE

java -cp ${rootDir}/lib/SRL_it.jar:${rootDir}/lib/kaflib-naf-1.1.9.jar:${rootDir}/lib/jdom-2.0.5.jar eu.fbk.newsreader.NAF.SRL.AnnotatePredWN $RANDOMFILE $RANDOMFILE.wn.naf ${rootDir}/resources/wn-data-ita.tab 

java -cp ${rootDir}/lib/SRL_it.jar:${rootDir}/lib/kaflib-naf-1.1.9.jar:${rootDir}/lib/jdom-2.0.5.jar eu.fbk.newsreader.NAF.SRL.AnnotatePredArg_v4 $RANDOMFILE.wn.naf $RANDOMFILE.srl.naf ${rootDir}/resources/italian_pbframes/ ${rootDir}/resources/mapping-16-30/ ${rootDir}/resources/it-signal-temporal.list  ${rootDir}/resources/it-signal-spatial.list

cat $RANDOMFILE.srl.naf

rm $RANDOMFILE
rm $RANDOMFILE.srl.naf
rm $RANDOMFILE.wn.naf

