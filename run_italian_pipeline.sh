#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink 
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR

rootDir=$DIR
#rootDir=/home/newsreader/components-it

cat $1 | ${rootDir}/TextPro2.0/run.sh 2>/dev/null | \
${rootDir}/EHU-wikify/run.sh 2>/dev/null | \
${rootDir}/EHU-ned/run.sh 2>/dev/null | \
${rootDir}/FBK-SRL/run.sh 2>/dev/null | \
${rootDir}/FBK-eventCoref/run.sh 2>/dev/null | \
${rootDir}/FBK-timeAnchor/run.sh 2>/dev/null 

