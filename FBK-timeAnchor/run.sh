#!/bin/bash
#rootDir=/home/newsreader/components-it/FBK-timeAnchor

scratchDir=/tmp

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink 
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR

rootDir=$DIR

RANDOM=`bash -c 'echo $RANDOM'`

RANDOMFILE=$scratchDir/$RANDOM

cat > $RANDOMFILE

cat $RANDOMFILE | java -cp ${rootDir}/lib/PredicateTimeAnchorIt.jar:${rootDir}/lib/kaflib-naf-1.1.9.jar:${rootDir}/lib/jdom-2.0.5.jar eu.fbk.newsreader.italian.TimeAnchor.PredicateTimeAnchor_v2 


rm $RANDOMFILE
