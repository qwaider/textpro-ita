package eu.fbk.newsreader.italian.TimeAnchor;

/**
 * Author: minard@fbk.eu
 * Date: Nov 2015
 * Description: Annotation of predicate time anchoring represented by temporal relations between predicates and time anchors.
 *
 * Lang: Italian
 * Usage: java -cp lib/kaflib-naf-1.1.9.jar:lib/jdom-2.05.jar eu.fbk.newsreader.italian.TimeAnchor.PredicateTimeAnchor_v2 input output

 */


import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import ixa.kaflib.*;
import ixa.kaflib.KAFDocument.Layer;
import ixa.kaflib.KAFDocument.LinguisticProcessor;
import ixa.kaflib.Predicate.Role;

import org.jdom2.JDOMException;

public class PredicateTimeAnchor_v2 {

	static KAFDocument nafFile = null;
	static List<timexFeatStructure> listTxFeat = new ArrayList<timexFeatStructure> ();
	
	/**
	 * @param args
	 * @throws IOException 
	 * @throws ParseException 
	 * @throws JDOMException 
	 */
	public static void main(String[] args) throws IOException, ParseException, JDOMException {
		// TODO Auto-generated method stub

		String beginTime = getTodayDate();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		nafFile = KAFDocument.createFromStream(br);

		LinguisticProcessor lp = nafFile.addLinguisticProcessor("temporalRelations","FBK-timeAnchor","v0.1, Nov 2015");
		lp.setBeginTimestamp(beginTime);
		
		HashMap<String, List<String>> listIdEvTenseAspect = getListIdEvTenseAspect(nafFile);

		
		addTimeAnchorPredicates(listIdEvTenseAspect);
		
		lp.setEndTimestamp(getTodayDate());
		System.out.print(nafFile.toString());
	}
	
	/**
	 * get time in string
	 * @return
	 */
	public static String getTodayDate (){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		Date date = new Date();
		String dateString = dateFormat.format(date).toString();
		return dateString;
	}
	
	/**
	 * Extract tense and aspect of a predicate from the morpho analysis of the terms of its span.
	 * build hash: key --> predicate id, value --> list of the tense and aspect attributes of this predicate
	 * @param nafFile
	 * @return
	 */
	private static HashMap<String,List<String>> getListIdEvTenseAspect(KAFDocument nafFile){
		HashMap<String,List<String>> listIdEvTA = new HashMap<String,List<String>> ();
		
		HashMap<String,Term> listTermById = new HashMap<String,Term> ();
		for(Term t : nafFile.getTerms()){
			listTermById.put(t.getId(),t);
		}
		
		for (Predicate pr : nafFile.getPredicates()){
			List<String> listTA = new ArrayList<String> ();
		
			for (Term t : pr.getSpan().getTargets()){
				if(t.hasMorphofeat()){
				
					if(t.getMorphofeat().contains("indic+pres")){
						listTA.add("PRESENT");
						listTA.add("NONE");
						break;
					}
					else if(t.getMorphofeat().contains("indic+past") || t.getMorphofeat().contains("indic+imperf")){
						listTA.add("PAST");
						listTA.add("NONE");
						break;
					}
					else if(t.getMorphofeat().contains("fut")){
						listTA.add("FUTURE");
						listTA.add("PERFECTIVE");
						break;
					}
					else if(t.getMorphofeat().contains("gerundio")){
						listTA.add("PREPART");
						listTA.add("NONE");
						break;
					}
					else if(t.getMorphofeat().contains("part+past")){
						String prevTId = "t"+Integer.toString(Integer.parseInt(t.getId().replace("t", ""))+1);
						if(listTermById.containsKey(prevTId) && listTermById.get(prevTId).hasMorphofeat()
								&& listTermById.get(prevTId).getMorphofeat().contains("indic+pres")){
							listTA.add("PAST");
							listTA.add("NONE");
						}
						else if(listTermById.containsKey(prevTId)  && listTermById.get(prevTId).hasMorphofeat()
								&& listTermById.get(prevTId).getMorphofeat().contains("indic+fut")){
							listTA.add("FUTURE");
							listTA.add("NONE");
						}
						else if(listTermById.containsKey(prevTId) && listTermById.get(prevTId).hasMorphofeat()
								&& listTermById.get(prevTId).getMorphofeat().contains("part+past")){
							listTA.add("PAST");
							listTA.add("NONE");
						}
						else{
							listTA.add("PASTPART");
							listTA.add("NONE");
						}
						break;
					}
					else if(t.getMorphofeat().contains("infinito+pres")){
						String prevTId = "t"+Integer.toString(Integer.parseInt(t.getId().replace("t", ""))+1);
						if(listTermById.containsKey(prevTId) && listTermById.get(prevTId).hasMorphofeat()
								&& listTermById.get(prevTId).getMorphofeat().contains("indic+pres")){
							listTA.add("PRES");
							listTA.add("NONE");
						}
						else{
							listTA.add("NONE");
							listTA.add("NONE");
						}
						break;
					}
				}
			}
			
			listIdEvTA.put(pr.getId(), listTA);
		}
		
		return listIdEvTA;
	}
	
	/**
	 * Get the keys of a hash that have the value given in parameter.
	 * @param map
	 * @param value
	 * @return
	 */
	public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
	    for (Entry<T, E> entry : map.entrySet()) {
	        if (Objects.equals(value, entry.getValue())) {
	            return entry.getKey();
	        }
	    }
	    return null;
	}
	
	/**
	 * Get the list of timex3 that are inside the sentence given in parameter.
	 * @param numSent
	 * @return
	 */
	public static List<Timex3> getTmxBySent (int numSent){
		List<Timex3> list_tmx_sent = new ArrayList<Timex3> ();
		ListIterator<Timex3> list_tmx = nafFile.getTimeExs().listIterator();
		
		while(list_tmx.hasNext()){
			Timex3 tmx = list_tmx.next();
			if (tmx.hasSpan()){
				List<WF> tmx_terms = tmx.getSpan().getTargets();
				if(tmx_terms.size() > 0 && tmx_terms.get(0).getSent() == numSent){
					list_tmx_sent.add(tmx);
				}
			}
		}
		
		return list_tmx_sent;
	}
	
	/**
	 * get for each predicate its event type
	 * build hash: key --> predicate id, value --> event type
	 * @return
	 */
	public static HashMap<String,String> getListPredEventType (){
		HashMap<String,String> listPredEvType = new HashMap<String,String> ();
		
		List<Predicate> listPred = nafFile.getPredicates();
		for (Predicate pr : listPred){
			List<ExternalRef> prExRef = pr.getExternalRefs();
			for (ExternalRef ref : prExRef){
				if (ref.getResource().equals("EventType")){
					if((listPredEvType.containsKey(pr.getId()) && ref.getReference().equals("REPORTING"))
							|| !listPredEvType.containsKey(pr.getId())){
						listPredEvType.put(pr.getId(), ref.getReference());
					}
				}
			}
		}
		
		return listPredEvType;
	}
	
	
	/**
	 * Get the list of terms given a list of word forms
	 * @param list_wf
	 * @return
	 */
	private static List<Term> getTermsByWFs (List<WF> list_wf){
		List<Term> list_term = new ArrayList<Term> ();
		Iterator<Term> list_all_terms = nafFile.getTerms().iterator();
		int nb_wf = 0;
		while(list_all_terms.hasNext() && nb_wf < list_wf.size()){
			Term t = list_all_terms.next();
			for (WF w : t.getSpan().getTargets()){
				if (w.getId().equals(list_wf.get(nb_wf).getId())){
					list_term.add(t);
					nb_wf ++;
				}
			}
		}
		return list_term;
	}
	
	/**
	 * get pair PRED-TMX that are linked by a dep relation (NMOD, TMP or APPO)
	 * @return
	 */
	public static HashMap<String,List<depPredTmxStructure>> getListPredTmxDep(){
		HashMap<String,List<depPredTmxStructure>> predTmxNMOD = new HashMap<String,List<depPredTmxStructure>> ();
		
		HashMap<String,String> predEventType = getListPredEventType();
		
		for (int numSent = 0; numSent < nafFile.getNumSentences(); numSent++){
			List<Predicate> list_pred = nafFile.getPredicatesBySent(numSent);
			List<Timex3> list_tmx = getTmxBySent(numSent);
			
			//NMOD(PRED, TMX) or TMP(PRED, TMX) or OBJ(PRED, TMX)
			if(list_pred.size() > 0 && list_tmx.size() > 0){
				boolean findDepRel = false;
				for (Predicate pr : list_pred){
					List<Dep> list_dep_from_pred = nafFile.getDepsFromTerm(pr.getSpan().getTargets().get(0));
					if (list_dep_from_pred.size() > 0){
						for (Dep d : list_dep_from_pred){
							Term depTo = d.getTo();
							for (Timex3 tmx: list_tmx){
								if (tmx.hasSpan() && tmx.getSpan().getTargets().size() > 0 
										&& getTermsByWFs(tmx.getSpan().getTargets()).get(0) == depTo &&
										(d.getRfunc().equals("NMOD") || d.getRfunc().equals("TMP") || d.getRfunc().equals("OBJ"))){
									depPredTmxStructure st = new depPredTmxStructure();
									st.predId = pr.getId();
									st.tmxId = tmx.getId();
									st.depRel = d.getRfunc();
									if (predTmxNMOD.containsKey(pr.getId())){
										predTmxNMOD.get(pr.getId()).add(st);
									}
									else{
										List<depPredTmxStructure> tmp = new ArrayList<depPredTmxStructure>();
										tmp.add(st);
										predTmxNMOD.put(pr.getId(), tmp);
									}
									findDepRel = true;
								}
							}
						}
					}
				}
				
				//APPO(TMX, PRED)
				if(!findDepRel){
					for (Timex3 tmx: list_tmx){
						List<Dep> list_dep_from_tmx = new ArrayList<Dep> ();
						
						for(Term t: getTermsByWFs(tmx.getSpan().getTargets())){
							list_dep_from_tmx.addAll(nafFile.getDepsFromTerm(t));
						}
						if (list_dep_from_tmx.size() > 0){
							for (Dep d : list_dep_from_tmx){
								Term depTo = d.getTo();
								for (Predicate pred: list_pred){
									if (pred.getSpan().getTargets().get(0) == depTo && 
											d.getRfunc().contains("APPO")){
										depPredTmxStructure st = new depPredTmxStructure();
										st.predId = pred.getId();
										st.tmxId = tmx.getId();
										st.depRel = d.getRfunc();
										if (predTmxNMOD.containsKey(pred.getId())){
											predTmxNMOD.get(pred.getId()).add(st);
										}
										else{
											List<depPredTmxStructure> tmp = new ArrayList<depPredTmxStructure>();
											tmp.add(st);
											predTmxNMOD.put(pred.getId(), tmp);
										}
										findDepRel = true;
									}
								}
							}
						}
					}
				}
				
				if (!findDepRel){
					for (Predicate pr : list_pred){
						List<Dep> list_dep_from_pred = nafFile.getDepsFromTerm(pr.getSpan().getTargets().get(0));
						if (list_dep_from_pred.size() > 0){
							for (Dep d : list_dep_from_pred){
								Term depTo = d.getTo();
								if((depTo.getLemma().equals("da/det") || depTo.getLemma().equals("a/det") || depTo.getLemma().equals("per")
										|| depTo.getLemma().equals("dopo") || depTo.getLemma().equals("entro") || depTo.getLemma().equals("in") 
										|| depTo.getLemma().equals("in/det") || depTo.getLemma().equals("da") || depTo.getLemma().equals("a")
										|| depTo.getLemma().equals("di")) && 
										(d.getRfunc().equals("RMOD"))){
								
									List<Dep> list_dep_to_prep = nafFile.getDepsFromTerm(depTo);
									if (list_dep_to_prep.size() > 0){
										for (Dep dprep : list_dep_to_prep){
											Term depToPrep = dprep.getTo();
											for (Timex3 tmx: list_tmx){
												List<WF> tmx_span = tmx.getSpan().getTargets();
												for (Term tt: getTermsByWFs(tmx_span)){
													if (tt == depToPrep && 
														(dprep.getRfunc().contains("DENOM"))
														&& (!predEventType.containsKey(pr.getId()) || !predEventType.get(pr.getId()).equals("SPEECH_COGNITIVE"))){
														
														depPredTmxStructure st = new depPredTmxStructure();
														st.predId = pr.getId();
														st.tmxId = tmx.getId();
														st.depRel = d.getRfunc()+":"+dprep.getRfunc();
														st.signal = depTo.getLemma();
														if (predTmxNMOD.containsKey(pr.getId())){
															predTmxNMOD.get(pr.getId()).add(st);
														}
														else{
															List<depPredTmxStructure> tmp = new ArrayList<depPredTmxStructure>();
															tmp.add(st);
															predTmxNMOD.put(pr.getId(), tmp);
														}
														findDepRel = true;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		return predTmxNMOD;
	}
	
	
	
	/**
	 * build hash: key --> timex id; value --> timex value
	 * @param List of timex3
	 * @return
	 */
	private static HashMap<String,String> getTimex3IdVal(List<Timex3> list_timex){
		HashMap<String,String> timexValue = new HashMap<String,String> ();
		
		for (Timex3 tx : list_timex){
			timexValue.put(tx.getId(), tx.getValue());
		}
		
		return timexValue;
	}
	
	/**
	 * get list of TLINKs from the NAF file
	 * @return
	 */
	public static List<TLink> getTLinks(){
		 List<Annotation> temprels = nafFile.getLayer(Layer.TEMPORAL_RELATIONS);
		    List<TLink> tlinks = new ArrayList<TLink>();
		    for (Annotation temprel : temprels) {
		        if (temprel instanceof TLink) {
		          tlinks.add((TLink) temprel);
		        }
		    }
		    return tlinks;
	}
	
	/**
	 * For each predicate find if it has a time anchor using dependencies, tense-aspect and temporal signals.
	 * Main function
	 * @param listIdEvTenseAspect
	 */
	public static void addTimeAnchorPredicates(HashMap<String,List<String>> listIdEvTenseAspect){
		
		List<Predicate> list_pred = nafFile.getPredicates();
		
		List<TLink> list_tlink = getTLinks();
		
		HashMap<String,List<TLink>> predTLink = getTLinkByPred(list_tlink, nafFile);
		
		HashMap<String,List<depPredTmxStructure>> predTmxNMOD = getListPredTmxDep();
		
		List<Timex3> list_timex = nafFile.getTimeExs();
		HashMap<String,String> timexValue = getTimex3IdVal(list_timex);
		HashMap<String,String> timexTermId = getTimex3TermId(list_timex);
		
		HashMap<String, Timex3> listTimexById = getTimexById(nafFile.getTimeExs());
		
		HashMap<String,String> predEventType = getListPredEventType();
		
		HashMap<String,Timex3> listTimexId = new HashMap<String,Timex3> ();
		for (Timex3 tx : list_timex){
			listTimexId.put(tx.getId(), tx);
		}
		
		int numSent = 0;
		
		for (Predicate pred : list_pred){
			Boolean setTimeanchor = false;
			String timeAnchorPred = "";

			if(numSent != pred.getSent()){
				numSent = pred.getSent();
			}
			
			String evid = pred.getSpan().getTargets().get(0).getId().replace("t", "e");
			String tense = "";
			String aspect = "";
			if (listIdEvTenseAspect.containsKey(evid)){
				tense =  listIdEvTenseAspect.get(evid).get(0);
				aspect = listIdEvTenseAspect.get(evid).get(1);
			}
			
			if (predTmxNMOD.containsKey(pred.getId())){
				for (depPredTmxStructure st : predTmxNMOD.get(pred.getId())){
					String beginP = "";
					String endP = "";
					if(listTimexId.get(st.tmxId).hasBeginPoint()){
						beginP = listTimexId.get(st.tmxId).getBeginPoint().getId();
					}
					if(listTimexId.get(st.tmxId).hasEndPoint()){
						endP = listTimexId.get(st.tmxId).getEndPoint().getId();
					}
					
					if (st.signal == null || st.signal.equals("")){
						if(!beginP.equals("") || !endP.equals("")){
							timeAnchorPred = "b:"+beginP+"#e:"+endP;
						}
						else{
							timeAnchorPred = "t:"+st.tmxId;
						}
					}
					else{
						if (st.signal.equals("a") || st.signal.equals("a/det") || st.signal.equals("di") || st.signal.equals("di/det")){
							timeAnchorPred = "t:"+st.tmxId;
						}
						else if(st.signal.equals("da/det")){
							timeAnchorPred = "b:"+st.tmxId;
							if(!endP.equals("")){
								timeAnchorPred += "#e:"+endP;
							}
						}
						else if(st.signal.equals("da")){
							timeAnchorPred = "b:"+st.tmxId+"#e:tmx0";
							if(!endP.equals("")){
								timeAnchorPred += "#e:"+endP;
							}
						}
						else if(st.signal.equals("per")){
							timeAnchorPred = "d:"+st.tmxId;
						}
						else if(st.signal.equals("entro")){
							timeAnchorPred = "e:"+st.tmxId;
							if(!beginP.equals("")){
								timeAnchorPred = "b:"+beginP+"#e:"+timeAnchorPred;
							}
							else{
								timeAnchorPred = "b:tmx0#e:"+st.tmxId;
							}
						}
						else{
							if(!beginP.equals("") || !endP.equals("")){
								timeAnchorPred = "b:"+beginP+"#e:"+endP;
							}
							else{
								timeAnchorPred = "t:"+st.tmxId;
							}
						}
					}	
					setTimeanchor = true;
				}
			}
			
			if((pred.getExternalRefs().size()>1 
					|| (pred.getExternalRefs().size() == 1 && !pred.getExternalRefs().get(0).getResource().equals("NomBank"))) 
					&& !setTimeanchor){
				List<Role> listrole = pred.getRoles();
				for (Role r : listrole){
					if (r.getSemRole().equals("AM-TMP")){
						if(timexTermId.containsKey(r.getTerms().get(0).getId())){
							String txId = timexTermId.get(r.getTerms().get(0).getId());
							if(timexValue.containsKey(txId)){
								timeAnchorPred = "tmp:"+txId;
							}
						}
					}
				}
			}
			
			if(predTLink.containsKey(pred.getId()) && timeAnchorPred.equals("")){
				List<TLink> temp = predTLink.get(pred.getId());
				for (TLink t : temp){
					
					
					//if tlink of type SIMULTANEOUS or IS_INCLUDED
					if(t.getToType().equals("timex") 
							&& (t.getRelType().equals("SIMULTANEOUS") || t.getRelType().equals("IS_INCLUDED"))){
						String timeA = t.getTo().getId();
						timeAnchorPred = "t:"+timeA;
						setTimeanchor = true;
					}
					if(t.getToType().equals("timex") 
							&& (t.getRelType().equals("BEGINS") || t.getRelType().equals("IAFTER"))){
						String timeA = t.getTo().getId();
						timeAnchorPred = "b:"+timeA;
						setTimeanchor = true;
					}
					if(t.getToType().equals("timex") 
							&& (t.getRelType().equals("ENDS") || t.getRelType().equals("IBEFORE"))){
						String timeA = t.getTo().getId();
						if(!timeAnchorPred.equals("")){
							timeAnchorPred += "#e:"+timeA;
						}
						else{
							timeAnchorPred = "e:"+timeA;
						}
						setTimeanchor = true;
					}
					if(t.getToType().equals("timex") 
							&& t.getRelType().equals("DURING")){
						String timeA = t.getTo().getId();
						timeAnchorPred = "d:"+timeA;
						setTimeanchor = true;
					}
					if(t.getToType().equals("timex") 
							&& t.getRelType().equals("INCLUDES")){
						String timeA = t.getTo().getId();
						if(!timeAnchorPred.equals("")){
							timeAnchorPred += "#t:"+timeA;
						}
						else{
							timeAnchorPred = "t:"+timeA;
						}
						setTimeanchor = true;
					}
					if(t.getToType().equals("timex") 
							&& t.getRelType().equals("BEFORE")){

						String timeA = t.getTo().getId();
						if(t.getTo().getId().equals("tmx0") && 
								(tense.equals("PAST") && aspect.equals("NONE") 
										&& predEventType.containsKey(pred.getId()) && predEventType.get(pred.getId()).equals("REPORTING"))){
							if(!timeAnchorPred.equals("")){
								timeAnchorPred += "#t:"+timeA;
							}
							else{
								timeAnchorPred = "t:"+timeA;
							}
						}
					}
				}
			}
			
			if(timeAnchorPred.equals("")){
				if(!tense.equals("")){
					if(tense.equals("PRESENT") && aspect.equals("NONE")){
						timeAnchorPred = "t:"+"tmx0";
					}
					if(tense.equals("PRESPART") && timeAnchorPred.equals("")){
						timeAnchorPred = "t:"+"tmx0";
					}
				}
			}
			else if(tense.equals("FUTURE") && timeAnchorPred.equals("t:tmx0")){
				timeAnchorPred = "";
			}
			
			
			if(!timeAnchorPred.equals("")){
				Timex3 anchorTime = null;
				Timex3 beginPoint = null;
				Timex3 endPoint = null;
				Span span = nafFile.newSpan();
				span.addTarget(pred);
				
				String [] eltTA = timeAnchorPred.split("#");
				
				for (int i=0; i<eltTA.length; i++){
					String [] ta = eltTA[i].split(":");
					if (ta.length > 1 && listTimexById.containsKey(ta[1])){
						if (ta[0].equals("t") && !timeAnchorPred.contains("b:") && !timeAnchorPred.contains("e:") && !timeAnchorPred.contains("tmp:")){
							if(listTimexById.get(ta[1]).hasBeginPoint() || listTimexById.get(ta[1]).hasEndPoint()){
								if(listTimexById.get(ta[1]).hasBeginPoint()){
									beginPoint = listTimexById.get(ta[1]).getBeginPoint();
								}
								if(listTimexById.get(ta[1]).hasEndPoint()){
									endPoint = listTimexById.get(ta[1]).getEndPoint();
								}
							}
							else{
								anchorTime = listTimexById.get(ta[1]);
							}
						}
						else if(ta[0].equals("tmp") && !timeAnchorPred.contains("b:") && !timeAnchorPred.contains("e:")){
							anchorTime = listTimexById.get(ta[1]);
						}					
						else if(ta[0].equals("b")){
							beginPoint = listTimexById.get(ta[1]);
						}
						else if(ta[0].equals("e")){
							endPoint = listTimexById.get(ta[1]);
						}
					}
				}
				
				if (anchorTime != null || beginPoint != null || endPoint != null){
					nafFile.newPredicateAnchor(anchorTime, beginPoint, endPoint, span);
				}
			}
		}
		
	}

	/**
	 * build hash: key --> timex id, value --> timex object
	 * @param List of timex3
	 * @return
	 */
	private static HashMap<String, Timex3> getTimexById (List<Timex3> list_timex){
		HashMap<String, Timex3> listTimexById = new HashMap<String,Timex3> ();
		for (Timex3 t : list_timex){
			listTimexById.put(t.getId(), t);
		}
		
		return listTimexById;
	}
	
	/**
	 * build hash: key --> term id; value --> timex id
	 * @param List of timex3
	 * @return Hash
	 */
	private static HashMap<String,String> getTimex3TermId(List<Timex3> list_timex){
		HashMap<String,String> timexTermId = new HashMap<String,String> ();
		
		for (Timex3 tx : list_timex){
			if(tx.getSpan() != null && tx.getSpan().size() > 0 && tx.getSpan().getFirstTarget() != null){
				timexTermId.put(tx.getSpan().getTargets().get(0).getId().replace("w", "t"), tx.getId());
			}
		}
		return timexTermId;
	}
	
	/**
	 * build hash: key --> pred id, value --> list of tlinks
	 * @param List of tlinks, NAF file
	 * @return Hash
	 */
	private static HashMap<String,List<TLink>> getTLinkByPred(List<TLink> list_tlink, KAFDocument nafFile){
		HashMap<String,List<TLink>> predTLink = new HashMap<String,List<TLink>> ();
		for(TLink t : list_tlink){
			
			if(t.getFromType().equals("event")){
				String from = ((Predicate)t.getFrom()).getId();
				List<TLink> temp = new ArrayList<TLink>();
				if(predTLink.containsKey(from)){
					temp.addAll(predTLink.get(from));
				}
				temp.add(t);
				predTLink.put(from, temp);
			}
		}
		
		return predTLink;
	}
	
}

class timexFeatStructure{
	String signalBefore;
	String signalAfter;
	String eventBefore;
	String eventAfter;
	String eventTenseBefore;
	String eventTenseAfter;
	String signalInTimex;
	String value;
	Timex3 timexAfter;
	String period;
	Timex3 timexElt;
}

class depPredTmxStructure{
	String predId;
	String tmxId;
	String signal;
	String depRel;
}
