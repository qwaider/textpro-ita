# NewsReader Italian pipeline

****
Fondazione Bruno Kessler, Trento, Italy

Anne-Lyse Minard, Mohammed Qwaider, Christian Girardi, Paramita Mirza

2013-2016
****

Event Detection pipeline for Italian partly developed within the NewsReader project (http://www.newsreader-project.eu).

Online demo: http://hlt-services2.fbk.eu:8080/nwrDemo/nwr

Licenses: See the specific license of each tool and model.

- FBK-SRL: GNU AGPL v3

- FBK-timeAnchor: GNU AGPL v3

- FBK-eventCoref: GNU AGPL v3

- TextPro2.0-NWR: GNU AGPL v3

- textpro-models: CC-BY-NC-SA

Contact: minard@fbk.eu, qwaider@fbk.eu, manspera@fbk.eu


Installation:
```
git clone git@bitbucket.org:qwaider/textpro-ita.git
cd textpro-ita/ 
mkdir TextPro2.0/modules/MaltParser/model/
mkdir TextPro2.0/modules/MaltParser/model/ITA/
cp textpro-models/DepParser/* TextPro2.0/modules/MaltParser/model/ITA/
mkdir TextPro2.0/modules/TimePro/models/
cp textpro-models/TimePro/* TextPro2.0/modules/TimePro/models/
mkdir TextPro2.0/modules/TempRelPro/models/
cp textpro-models/TempRelPro/* TextPro2.0/modules/TempRelPro/models/
mkdir TextPro2.0/modules/EventPro/models/
cp textpro-models/EventPro/* TextPro2.0/modules/EventPro/models/
```

Installation of the external tools used by TextPro:
```
cd TextPro2.0/
./INSTALL.sh
```

Installation of the Named Entity Disambiguation module:
See https://github.com/ixa-ehu/ixa-pipe-ned

Installation of the wikification module:
See https://github.com/ixa-ehu/ixa-pipe-wikify


Usage:
```
sh run_italian_pipeline.sh input_file | cat >output_file
```

The input file should be a NAF file:
```
<NAF xml:lang="it" version="v3">
  <nafHeader>
    <fileDesc creationtime="" title="" />
    <public publicId="" uri="" />
  </nafHeader>
  <raw><![CDATA[ 
....
]]></raw>
</NAF>
```

If you do not want to install and/or run external modules, you have to comment the corresponding lines in the script run_italian_pipeline.sh.
