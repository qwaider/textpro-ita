package eu.fbk.newsreader.italian.eventDetection;

/**
 * Author: minard@fbk.eu
 * Date: Aug 2015
 * Description: Identification of intra-document event coreference relations in Italian
 *              We consider that events of class STATE are not part of coreference chain.
 * Lang: Italian
 * Usage: java -cp lib/kaflib-naf-1.1.9.jar:lib/jdom-2.05.jar eu.fbk.newsreader.italian.eventDetection.EventCoreferenceNAF input output

 */

import ixa.kaflib.Chunk;
import ixa.kaflib.Coref;
import ixa.kaflib.ExternalRef;
import ixa.kaflib.KAFDocument;
import ixa.kaflib.Predicate;
import ixa.kaflib.Span;
import ixa.kaflib.Target;
import ixa.kaflib.Term;
import ixa.kaflib.KAFDocument.LinguisticProcessor;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class EventCoreferenceNAF {

	static HashMap<String, List<String>> listTokenCoref = new HashMap<String, List<String>>(); 
	static HashMap<String, List<String>> listLemmaCoref = new HashMap<String, List<String>>(); 
	static HashMap<String, List<String>> listLemmaMorphCoref = new HashMap<String, List<String>>(); 
	static HashMap<String, List<String>> listWNCoref = new HashMap<String, List<String>> ();
	
	/**
	 * Get the string from a span
	 * @param listT: list of terms
	 * @param lemmaForm: lemma or form
	 * @return string
	 */
	private static String getSpanString (List<Term> listT, String lemmaForm){
		String spanString = "";
		for (Term t : listT){
			if(lemmaForm.equals("lemma")){
				String lemma = t.getLemma();
				if(lemma.contains(" ")){
					lemma = lemma.split(" ")[lemma.split(" ").length-1];
				}
				spanString += lemma+" ";
			}
			else{
				spanString += t.getForm()+" ";
			}
		}
		spanString = spanString.substring(0,spanString.length()-1);
		return spanString;
	}
	
	/**
	 * Return the type of an event (value of the external reference from the resource "EventType")
	 * @param pred
	 * @return string
	 */
	private static String getEventTypePred (Predicate pred){
		String eventType = "";
		for(ExternalRef ref : pred.getExternalRefs()){
			if (ref.getResource().equals("EventType")){
				eventType = ref.getReference();
				break;
			}
		}
		
		return eventType;
	}
	
	/**
	 * Represent event coreference relations in NAF
	 * @param nafFile
	 * @return KAFDocument
	 * @throws IOException
	 */
	private static KAFDocument addEventCoref (KAFDocument nafFile) throws IOException{
		
		List<Predicate> predicateL = nafFile.getPredicates();
		
		//HashMap: key --> term id, value --> Chunk
		HashMap<String,Chunk> listChunkByTerm = new HashMap<String, Chunk> ();
		for(Chunk ch : nafFile.getChunks()){
			for(Term t : ch.getSpan().getTargets()){
				listChunkByTerm.put(t.getId(), ch);
			}
		}
		
		//HashMap: key --> predicate id, value --> Predicate
		HashMap<String, Predicate> listPredById = new HashMap<String, Predicate> ();
		for (Predicate pred : predicateL){
			listPredById.put(pred.getId(), pred);
		}
		
		//HashMap: key --> term id, value --> Term
		HashMap<String, Term> listTermById = new HashMap<String, Term> ();
		for (Term t : nafFile.getTerms()){
			listTermById.put(t.getId(), t);
		}
		
		//HashMap: key --> predicate id, value --> morpho analysis
		HashMap<String, String> listPredMorpho = new HashMap<String,String> ();
		
		for(Predicate pred : predicateL){
			String predLemmaString = getSpanString(pred.getSpan().getTargets(), "lemma");
			String predFormString = getSpanString(pred.getSpan().getTargets(), "form");
			
			if (!getEventTypePred(pred).endsWith("STATE")){
				
				//List of Coref based only on tokens
				if(!listTokenCoref.containsKey(predFormString)){
					List<String> temp = new ArrayList<String> ();
					listTokenCoref.put(predFormString, temp);
				}	
				listTokenCoref.get(predFormString).add(pred.getId());	
				
				
				
				String lemmaMorph = predLemmaString.replace(" ", "_");
				
				String posPred = "";
				//get Morpho features for each term of the event
				for (Term t : pred.getSpan().getTargets()){
					if(t.hasMorphofeat()){
						String morphoFeat = t.getMorphofeat();
						if(morphoFeat.contains(" ")){
							morphoFeat = morphoFeat.split(" ")[morphoFeat.split(" ").length-1];
						}
						String [] info_morph = morphoFeat.split("\\+");
						//Verb: sing/plur
						if(t.getPos().startsWith("V")){
							posPred = "v";
							if(info_morph[info_morph.length-1].equals("sing") || info_morph[info_morph.length-1].equals("plur")){
								lemmaMorph += "#"+info_morph[info_morph.length-2]+"#"+info_morph[info_morph.length-1];
								break;
							}
						}
						//Noun: sing/plur
						else if(t.getPos().startsWith("S")){
							posPred = "n";
							if(info_morph[info_morph.length-1].equals("sing") || info_morph[info_morph.length-1].equals("plur")){
								lemmaMorph += "#"+info_morph[info_morph.length-1];
								break;
							}
							else if(Integer.parseInt(t.getId().replace("t", "")) > 0){
								Term prevTerm = listTermById.get("t"+Integer.toString(Integer.parseInt(t.getId().replace("t", ""))-1));
								
								if(prevTerm != null){
									if(prevTerm.hasMorphofeat() && prevTerm.getMorphofeat() != null && prevTerm.getMorphofeat().endsWith("sing")){
										lemmaMorph += "#sing";
										break;
									}
									else if(prevTerm.hasMorphofeat() && prevTerm.getMorphofeat() != null && prevTerm.getMorphofeat().endsWith("plur")){
										lemmaMorph += "#plur";
										break;
									}
								}
							}
						}
						//Adjective
						else if(t.getPos().startsWith("A")){
							posPred = "a";
						}
					}
				}
				
				
				//List of Coref based on lemma+morph (sing/plur)
				if(! listLemmaMorphCoref.containsKey(lemmaMorph)){
					List<String> temp = new ArrayList<String> ();
					listLemmaMorphCoref.put(lemmaMorph, temp);
				}
				listLemmaMorphCoref.get(lemmaMorph).add(pred.getId());
				listPredMorpho.put(pred.getId(), lemmaMorph);
				

				//Delete the ending of lemma (are/ire/ere for verbs; a/i/o/e for nouns)
				String stemPred = predLemmaString;
				if (posPred.equals("v") && !stemPred.contains(" ") && 
						(stemPred.endsWith("are") || stemPred.endsWith("ere") || stemPred.endsWith("ire"))){
					stemPred = stemPred.replaceFirst("are$", "");
					stemPred = stemPred.replaceFirst("ire$", "");
					stemPred = stemPred.replaceFirst("ere$", "");
				}				
				else if (posPred.equals("n") && !stemPred.contains(" ")){
					stemPred = stemPred.replaceFirst("a$", "");
					stemPred = stemPred.replaceFirst("i$", "");
					stemPred = stemPred.replaceFirst("o$", "");
					stemPred = stemPred.replaceFirst("e$", "");
				}

				//List of Coref based on stem (lemma without ending)
				if(!listLemmaCoref.containsKey(stemPred)){
					List<String> temp = new ArrayList<String> ();
					listLemmaCoref.put(stemPred, temp);
				}	
				listLemmaCoref.get(stemPred).add(pred.getId());	

				//List of Coref based on WordNet senses
				for(ExternalRef eref : pred.getExternalRefs()){
					if(eref.getResource().equals("WordNet")){
						if(!listWNCoref.containsKey(eref.getReference())){
							List<String> temp = new ArrayList<String> ();
							listWNCoref.put(eref.getReference(), temp);
						}
						listWNCoref.get(eref.getReference()).add(pred.getId());
					}
				}
			}
		}
		

		int idCoref = 1;

		List<String> predInCoref = new ArrayList<String> ();
		
		/** Annotation of coreference chains in NAF
		 * 1/ treat coreference chains based on lemma. Gather predicates according to their number (plur/sing).
		 * 2/ treat coreference chains based on WordNet senses. 
		 * 3/ add all predicate that are not part of a coreference chain as singleton.
		 */
		
		for (String tok : listLemmaCoref.keySet()){
			if(listLemmaCoref.get(tok).size() > 1){
				String list_eid = "";
				String list_eid_sing = "";
				String list_eid_plur = "";
				for(String eid : listLemmaCoref.get(tok)){
					if (listPredMorpho.containsKey(eid) && listPredMorpho.get(eid).endsWith("plur")){
						list_eid_plur += " "+eid;
					}
					else{
						list_eid_sing += " "+eid;
					}

					list_eid += " "+eid;
				}
				
				
				if(!list_eid_plur.equals("")){
					List<List<Target>> listTargetPred = new ArrayList<List<Target>> ();
					list_eid_plur = list_eid_plur.substring(1);
					for(String eid : list_eid_plur.split(" ")){
						predInCoref.add(eid);
						List<Target> temp = new ArrayList<Target> ();
						for (Term t : listPredById.get(eid).getSpan().getTargets()){
							temp.add(nafFile.createTarget(t));
						}
						listTargetPred.add(temp);
					}
					Coref cor = nafFile.createCoref("coevent"+Integer.toString(idCoref),listTargetPred);
					cor.setType("event");
					idCoref ++;
				
				}
			
				if(!list_eid_sing.equals("")){
					List<List<Target>> listTargetPred = new ArrayList<List<Target>> ();
					list_eid_sing = list_eid_sing.substring(1);
					for(String eid : list_eid_sing.split(" ")){
						predInCoref.add(eid);
						List<Target> temp = new ArrayList<Target> ();
						for (Term t : listPredById.get(eid).getSpan().getTargets()){
							temp.add(nafFile.createTarget(t));
						}
						listTargetPred.add(temp);
					}
					Coref cor = nafFile.createCoref("coevent"+Integer.toString(idCoref),listTargetPred);
					cor.setType("event");
					idCoref ++;
				
				}
			}
		}
		
		
		for (String tok : listWNCoref.keySet()){
			
			if(listWNCoref.get(tok).size() > 1){
				boolean toadd = true;
				List<List<Target>> listTargetPred = new ArrayList<List<Target>> ();
				for(String eid : listWNCoref.get(tok)){
					if(!predInCoref.contains(eid)){
						predInCoref.add(eid);
					}
					else{
						toadd = false;
					}
				}
				
				if(toadd){
					for(String eid : listWNCoref.get(tok)){
						List<Target> temp = new ArrayList<Target> ();
						for (Term t : listPredById.get(eid).getSpan().getTargets()){
							temp.add(nafFile.createTarget(t));
						}
						listTargetPred.add(temp);
					}
					Coref cor = nafFile.createCoref("coevent"+Integer.toString(idCoref),listTargetPred);
					cor.setType("event");
					idCoref ++;
				}
			}
		}
		
		for(Predicate pred : predicateL){
			if(!predInCoref.contains(pred.getId())){
				List<List<Target>> listTargetPred = new ArrayList<List<Target>> ();
				List<Target> temp = new ArrayList<Target> ();
				for (Term t : pred.getSpan().getTargets()){
					temp.add(nafFile.createTarget(t));
				}
				listTargetPred.add(temp);
				Coref cor = nafFile.createCoref("coevent"+Integer.toString(idCoref),listTargetPred);
				cor.setType("event");
				idCoref ++;
			}
		}
		
		return nafFile;
	}
	
	
	public static String getTodayDate (){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		Date date = new Date();
		String dateString = dateFormat.format(date).toString();
		return dateString;
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		KAFDocument nafFile = KAFDocument.createFromFile(new File(args[0]));
		LinguisticProcessor lp = nafFile.addLinguisticProcessor("coreferences","FBK-eventCoref","v0.1, Aug 2015");
		lp.setBeginTimestamp(getTodayDate());
		
		nafFile = addEventCoref(nafFile);
		
        lp.setEndTimestamp(getTodayDate());
		
		nafFile.save(args[1]);
		
	}

}
