#!/bin/bash
#rootDir=/home/newsreader/components-it/FBK-eventCoref

scratchDir=/tmp

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink 
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR

rootDir=$DIR

RANDOM=`bash -c 'echo $RANDOM'`

RANDOMFILE=$scratchDir/$RANDOM

cat > $RANDOMFILE

java -cp ${rootDir}/lib/EventCoref.jar:${rootDir}/lib/kaflib-naf-1.1.9.jar:${rootDir}/lib/jdom-2.0.5.jar eu.fbk.newsreader.italian.eventDetection.EventCoreferenceNAF $RANDOMFILE $RANDOMFILE.ev.naf 

cat $RANDOMFILE.ev.naf

rm $RANDOMFILE
rm $RANDOMFILE.ev.naf

