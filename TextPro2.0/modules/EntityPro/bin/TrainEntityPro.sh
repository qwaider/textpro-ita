#! /bin/tcsh -f

### EntityPro training	###
### v1.0.0, 2007      	###

### DESCRIPTION ###
### This is the script that you can use to train EntityPro2007 on different data sets.To do that the training data set needs to be in a particular format whereas TextPro has to have already been installed given that this script uses some information produced by some other TextPro modules (i.e. pos and lemma). 

### INPUT ###
### The training data set that consits of 2 columns separated by a tab character. Each word has been put on a separate line and there is an empty line after each sentence (including the last). The first item on each line is a word and the second is the named entity tag. The named entity tags are in the IOB2 format. (B for words at the BEGINING of a named entity,I for words INSIDE a named entity, and O for words that are not part of named entities),e.g.

###Bill		B-PER
###Clinton	I-PER
###said		O
###that		O
###Putin	B-PER
###can		O
###be		O
###trusted	O
###.
###


### OUTPUT ###
### The generated model that then can be used by EntityPro to annotate the data set to be annotated. 


### Usage:TrainEntityPro trainingDataSet modelName
### where:
### trainingDataSet: is the training data set
### modelName: is the name of the model to be created


### TrainEntityPro Version ###
set VERSION = "1.0.0, 2007"


### Configuration Parameters ###
set ENTITYPRO_DIR = $TEXTPRO/modules/EntityPro
set YAMCHA_DIR = $TEXTPRO/modules/tools/yamcha-0.33
set MEM = 800 #Memory(MB) reserved to the process of training 
set TMP_DIR = /tmp/ #the tmp directory where to put the temporary files 

### Initialize Parameters ###
set trainingDataSet = "" #the training data set
set modelName = "" #the name of the model to build
set tmpDir = ""
set language = "" #the language: ita or eng is needed

### Read Parameters ###
while ( {$1} != {} )
                
    if ( {$1} == "-h" || {$1} == "--help" ) then
        echo "TrainEntityPro"
        echo "Copyright (C) 2007 All rights reserved."
        echo ""
        echo "Usage: TrainEntityPro trainingDataSet modelName"
        echo "  -h, --help                        show this help and exit"
        echo "  -v, --version                     show the version and exit"
        echo "  -s, --system                      check the system and exit"
        echo ""
        exit 0
    else if ( {$1} == "-v" || {$1} == "--version" ) then
        echo "EntityPro Version: "$VERSION
        echo "based on:"
        $YAMCHA_DIR/usr/local/bin/yamcha $1
        exit 0
    else if ( {$1} == "-s" || {$1} == "--system" ) then
        set trainingDataSet = $ENTITYPRO_DIR/test/training.iob2
        set modelName = $ENTITYPRO_DIR/test/123
        if ( -f $ENTITYPRO_DIR/test/123.model ) then
    		rm $ENTITYPRO_DIR/test/123.model
	endif
    else if ( {$1} == "-l" || {$1} == "--language" ) then
        shift
        set language = $1
    else if ( $# == 2 ) then
        set trainingDataSet = $1
        set modelName = $2
        shift
    else
        $ENTITYPRO_DIR/bin/TrainEntityPro.sh -h
        exit 0
    endif

    shift

end


### Check Parameters ###
if ( $trainingDataSet != "" ) then
    if ( ! -f $trainingDataSet ) then
        echo "Training Data Set Error: No such file or directory!"
        exit 1
    endif
endif
if ( $language != "ita" && $language != "eng" ) then # currently Italian language is supported only
    echo "Error: Unrecognized language option "$language"!"
    exit 1
endif


### Splitting the data set in 2 files (the tokens file and the file containing the named entity tags).
### The tokens file will be entiched with addtional information and finally the 2 files (i.e the entiched token file
### and the original tags files will be merged to have a single file to train the system
cut -f1 -d'	' $trainingDataSet > $TMP_DIR/tokens.txt
cut -f2 -d'	' $trainingDataSet > $TMP_DIR/tags.txt

if ( ! -f $TMP_DIR/tokens.txt ) then
    if ( ! -f $TMP_DIR/tags.txt ) then
        echo "Data Set Splitting Error: the $TMP_DIR/tokens.txt and/or $TMP_DIR/tags.txt files have not been created!"
	exit 1
    endif
endif


### Feature Extraction: adding the features pos and lemma to the tokens file by using TextPro. The training data set has to be already tokenized so that we exlude the tolenization from the preprocessing.
#if ( ! -e $TMP_DIR/tokens.txt.txp ) then
	$TEXTPRO/textpro.sh -l $language -d tokenization -y -c token+tokennorm+pos+lemma $TMP_DIR/tokens.txt -o $TMP_DIR/
#endif

if ( ! -f $TMP_DIR/tokens.txt.txp ) then
    echo "Feature Extraction Error: the $TMP_DIR/tokens.txt.txp file has not been created!"
    exit 1
endif

if ( $language == "ita" || $language == "eng" ) then
	grep -v "^# " $TMP_DIR/tokens.txt.txp | awk -f $ENTITYPRO_DIR/model/$language"_rules_v1.0.0" | $ENTITYPRO_DIR/bin/dictionary.sh $language n > $TMP_DIR/tokens_and_features.txt
endif

if ( ! -f $TMP_DIR/tokens_and_features.txt || -z $TMP_DIR/tokens_and_features.txt ) then
    echo "Feature Extraction Error: the $TMP_DIR/tokens_and_features.txt file has not been created!"
    exit 1
endif


### Merging the words file with the added features and the named entities tags file; the output is in the tmp directory

set trainingDataSetFileName = `basename $trainingDataSet` # Getting the training data set file name
set file_suffix = `date | sed 's/ /_/g'`
set newTrainingDataSetName = $trainingDataSetFileName"_"$file_suffix
paste -d'	' $TMP_DIR/tokens_and_features.txt $TMP_DIR/tags.txt | sed 's/^ $//' > $TMP_DIR/$newTrainingDataSetName #The final data set to train the system

if ( ! -f $TMP_DIR/tokens_and_features.txt ) then
    echo "Merging Error: the $TMP_DIR/$newTrainingDataSetName file has not been created!"
    exit 1
endif



### Training ###
#make -f $YAMCHA_DIR/training/Makefile CORPUS=$TMP_DIR/$newTrainingDataSetName MULTI_CLASS=2 MODEL=$modelName YAMCHA=$YAMCHA_DIR/usr/local/bin/yamcha  FEATURE="F:-1,0:1 F:0:2 F:-1..1:3,4,5,6 T:-2..-1" SVM_PARAM="-t 1 -d 2 -c 1 -m "$MEM TOOLDIR=$YAMCHA_DIR/libexec train

make -f $YAMCHA_DIR/training/Makefile CORPUS=$TMP_DIR/$newTrainingDataSetName MULTI_CLASS=2 MODEL=$modelName YAMCHA=$YAMCHA_DIR/usr/local/bin/yamcha  FEATURE="F:-1:1 F:0,1:1,2 F:0:3,4,5,6 T:-3..-1" SVM_PARAM="-t 1 -d 2 -c 1 -m "$MEM TOOLDIR=$YAMCHA_DIR/libexec train


if ( ! -f $modelName.model ) then
    echo "\nTraining Error: the model has not been created!"
    exit 1
else
    set dirModel = `dirname $modelName`
    echo "\nTraining Success: the generated model is in: $dirModel"
endif


### Remove unused files ###
rm $modelName.txtmodel.gz
rm $modelName.se
rm $modelName.svmdata

rm $TMP_DIR/$newTrainingDataSetName
rm $TMP_DIR/tokens_and_features.txt
rm $TMP_DIR/tokens.txt.txp
rm $TMP_DIR/tokens.txt
rm $TMP_DIR/tags.txt

