#! /bin/tcsh -f

### The dictionary parameter generator v1.0.0 ###

set LANGUAGE = $argv[1]
### Activating Black and White List: "-C"|""
set BLACK_WHITE_LIST = $argv[2]

### Do not modify ###
set DICT_DIR = $TEXTPRO/modules/EntityPro/resource/$LANGUAGE/
set SCRIPT_DIR = $TEXTPRO/modules/EntityPro/bin/

### Dictionaries ###
set PER_DICT = "dictionary.PER.1.3-1.ser"
set ORG_DICT = "dictionary.ORG.1.3-1.ser"
set LOC_DICT = "dictionary.LOC.1.3-1.ser"
set GPE_DICT = "dictionary.GPE.1.3-1.ser"
set BLACK_WHITE_DICT = "bw_list_v1.0.0"

if ( "$BLACK_WHITE_LIST" == "-C" && -e $DICT_DIR/$BLACK_WHITE_DICT) then    
    #cat $_ | perl -X $SCRIPT_DIR/dictionary-ser.pl $DICT_DIR/$GPE_DICT | perl -X $SCRIPT_DIR/dictionary-ser.pl $DICT_DIR/$LOC_DICT | perl -X $SCRIPT_DIR/dictionary-ser.pl $DICT_DIR/$ORG_DICT | perl -X $SCRIPT_DIR/dictionary-ser.pl $DICT_DIR/$PER_DICT | perl -X $SCRIPT_DIR/dictionary.pl $DICT_DIR/$BLACK_WHITE_DICT | sed 's/\sO$//' | sed 's/\(\s\)[BI]\-NIL$/\1O/'
    cat $_ | perl -X $SCRIPT_DIR/dictionary-ser.pl $DICT_DIR/$GPE_DICT | perl -X $SCRIPT_DIR/dictionary-ser.pl $DICT_DIR/$LOC_DICT | perl -X $SCRIPT_DIR/dictionary-ser.pl $DICT_DIR/$ORG_DICT | perl -X $SCRIPT_DIR/dictionary-ser.pl $DICT_DIR/$PER_DICT | perl -X $SCRIPT_DIR/dictionary.pl $DICT_DIR/$BLACK_WHITE_DICT | sed 's/ O$//' | sed 's/ [BI]\-NIL$/ O/'
else
    cat $_ | perl -X $SCRIPT_DIR/dictionary-ser.pl $DICT_DIR/$GPE_DICT | perl -X $SCRIPT_DIR/dictionary-ser.pl $DICT_DIR/$LOC_DICT | perl -X $SCRIPT_DIR/dictionary-ser.pl $DICT_DIR/$ORG_DICT | perl -X $SCRIPT_DIR/dictionary-ser.pl $DICT_DIR/$PER_DICT
endif
