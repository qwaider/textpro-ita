#! /bin/tcsh -f

### TestEntityPro         ###
### V1.4.3, 2010          ###

### TestEntityPro Version ###
set VERSION = "1.4.3, 2010"

### Do not modify ###
set ENTITYPRO_DIR = $TEXTPRO/EntityPro

$ENTITYPRO_DIR/bin/EntityPro.sh -v

echo ""
echo "testing ..."

$ENTITYPRO_DIR/bin/EntityPro.sh -o $ENTITYPRO_DIR/test/test.pke.eng.model.tst $ENTITYPRO_DIR/test/test.pke.eng.model.txt
$ENTITYPRO_DIR/bin/EntityPro.sh -l ITA -o $ENTITYPRO_DIR/test/test.pke.ita.model.tst $ENTITYPRO_DIR/test/test.pke.ita.model.txt

echo

set diff_eng = "eng-model"
set diff_eng = `cmp $ENTITYPRO_DIR/test/test.pke.eng.model.tst $ENTITYPRO_DIR/test/test.pke.eng.model.result | cut -f1 -d' '`
set diff_ita = "ita-model"
set diff_ita = `cmp $ENTITYPRO_DIR/test/test.pke.ita.model.tst $ENTITYPRO_DIR/test/test.pke.ita.model.result | cut -f1 -d' '`
if ( !(-z $ENTITYPRO_DIR/test/test.pke.eng.model.tst) && !(-z $ENTITYPRO_DIR/test/test.pke.ita.model.tst) && $diff_ita == "" && $diff_eng == "") then
echo "================"
echo "All tests passed"
echo "================"
else
echo "================"
echo "TEST FAILED!"
echo "================"
endif

rm $ENTITYPRO_DIR/test/*.tst
