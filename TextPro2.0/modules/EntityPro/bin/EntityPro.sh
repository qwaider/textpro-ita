#! /bin/tcsh -f

### EntityPro         ###
### v1.0.0, 2007      ###

### EntityPro Version ###
set VERSION = "1.0.0, 2007"

### Do not modify ###
set ENTITYPRO_DIR = $TEXTPRO/modules/EntityPro
set YAMCHA_DIR = $TEXTPRO/modules/tools/yamcha-0.33
set MODEL_NAME = "" #current model name
set ITA_MODEL_NAME = "ita_model_v1.0.0" #model for Italian language
set ENG_MODEL_NAME = "eng_model_v1.0.0" #model trained on EU-Bridge training corpus
#set ENG_MODEL_NAME = "eng_eubridge_v1.0.0" #model for English language

### White and Black list ###
set BLACK_WHITE_LIST = "-C" # "-C"|""

### Initialize Parameters ###
set outFileName = "" #use this FILE as output file
set inFileName = "" #use this FILE as input file
set language = "" #the language: eng|ita


### Read Parameters ###
while ( {$1} != {} )
                
    if ( {$1} == "-h" || {$1} == "--help" ) then
        echo "EntityPro"
        echo "Copyright (C) 2007 All rights reserved."
        echo ""
        echo "Usage: EntityPro [options] file"
        echo "  -h, --help                        show this help and exit"
        echo "  -v, --version                     show the version and exit"
        echo "  -s, --system                      check the system and exit"
        echo "  -l, --language=LANGUAGE           the language [eng|ita]  (default "$language")"
        echo "  -o, --output=FILE                 use FILE as output file"
        echo ""
        exit 0
    else if ( {$1} == "-v" || {$1} == "--version" ) then
        echo "EntityPro Version: "$VERSION
        echo "based on:"
        $YAMCHA_DIR/usr/local/bin/yamcha $1
        exit 0
    else if ( {$1} == "-s" || {$1} == "--system" ) then
        $ENTITYPRO_DIR/bin/TestEntityPro.sh
        exit 0
    else if ( {$1} == "-l" || {$1} == "--language" ) then
        shift
        set language = $1
    else if ( {$1} == "-o" || {$1} == "--output" ) then
        shift
        set outFileName = $1
    else if ( $# == 1 ) then
        set inFileName = $1
    else
        $ENTITYPRO_DIR/bin/EntityPro.sh -h
        exit 0
    endif

    shift

end

### Check Parameters ###
if ( $inFileName != "" ) then
    if ( ! -f $inFileName ) then
        echo "Error: No such file or directory!"
        exit 1
    endif
endif
if ( $language != "ita" && $language != "eng") then
    echo "Error: Unrecognized language option "$language"!"
    exit 1
endif

### Set Variables ###
if ( $language == "eng" ) then
    set MODEL_NAME = $ENG_MODEL_NAME 
else if ( $language == "ita" ) then
    set MODEL_NAME = $ITA_MODEL_NAME
endif
if ( $outFileName != "" ) then
    set outFileName = "--output "$outFileName
endif

### Run ###
if ( $language == "ita" ) then
   if ( $inFileName != "" ) then
      awk -f $ENTITYPRO_DIR/model/$language"_rules_v1.0.0" $inFileName | $ENTITYPRO_DIR/bin/dictionary.sh $language $BLACK_WHITE_LIST | $YAMCHA_DIR/usr/local/bin/yamcha $BLACK_WHITE_LIST -m $ENTITYPRO_DIR/model/$MODEL_NAME $outFileName
   else
      cat $_ | awk -f $ENTITYPRO_DIR/model/$language"_rules_v1.0.0" | $ENTITYPRO_DIR/bin/dictionary.sh $language $BLACK_WHITE_LIST | $YAMCHA_DIR/usr/local/bin/yamcha $BLACK_WHITE_LIST -m $ENTITYPRO_DIR/model/$MODEL_NAME $outFileName
   endif
else
   if ( $inFileName != "" ) then
         awk -f $ENTITYPRO_DIR/model/$language"_rules_v1.0.0" $inFileName | $YAMCHA_DIR/usr/local/bin/yamcha $BLACK_WHITE_LIST -m $ENTITYPRO_DIR/model/$MODEL_NAME $outFileName
      else
         cat $_ | awk -f $ENTITYPRO_DIR/model/$language"_rules_v1.0.0" | $YAMCHA_DIR/usr/local/bin/yamcha $BLACK_WHITE_LIST -m $ENTITYPRO_DIR/model/$MODEL_NAME $outFileName
      endif
endif
