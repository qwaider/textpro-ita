
%{
 #include <stdio.h>
 #include <ctype.h>
 char ortho_token[30]; // the token
 void get_lowercase_token(char *string); // get the lowercase token
 void get_lowercase_character(char *c);  // get the lowercase character
%}

/* eol */
eol \n
/* end of senetence */
eos ^(eos|EOS)$
/* token */
token ^[^ \n]+/(" "|\n)
/* tag */
tag [^ \n]+$

%%
 /* name: end of sentence */
{eos} { printf("%s\n", yytext); }
 /* name: token */
{token} { get_lowercase_token(yytext); printf("%s\n", yytext); }
 /* name: tag */
{tag}
{eol}
. 
%%


/** Get normalized token
  * @param token
  * @return
  */
void get_lowercase_token(char *string) {

  char *s;
  for ( s=string; *s; s++) {
    if (isascii(*s)) {
      if (isupper(*s)) *s = tolower(*s);
    }
    else get_lowercase_character(s);
  }

}


/** Get normalized character
  * @param character
  * @return
  */
void get_lowercase_character(char *c) {

  switch( *c ) {
  case '�' : *c = '�'; break;
  case '�' : *c = '�'; break;
  case '�' : *c = '�'; break;
  case '�' : *c = '�'; break;
  case '�' : *c = '�'; break;
  case '�' : *c = '�'; break;
  case '�' : *c = '�'; break;
  case '�' : *c = '�'; break;
  case '�' : *c = '�'; break;
  case '�' : *c = '�'; break;
  default : break;
  }

}


/* redefine yywrap 
 */
int yywrap(void) 
{ 
  return (1);
}


main ( int argc, char **argv ) { 

  if ( argc >1 ) {
    FILE *file;
    file = fopen( argv[1], "r" );
    if ( !file ) {
      fprintf(stderr, "could not open %s\n", argv[1]);
      exit(1);
    }
    yyin = file;
  }
  else {
    fprintf(stderr, "%s\n", "usage: tolwercase filename");
    exit(1);
  }
  
  yylex ();
  
}    
