
/** invert v1.4.2
 *
 *  From:
 *  
 *  token f1 f2 f3 f4 fN tag1  tag2  tag3  tag4
 *
 *  to:
 *
 *  token tag1  tag2  tag3  tag4 f1 f2 f3 f4 fN
 *  
 */

%{
  #include <stdlib.h>
  #include <string.h>
  // the info token
  char info[100];
  void init_info();
%}

/* eol */
eol \n
/* end of sentence */
eos ^(eos|EOS)$
/* token */
token ^[^ \n]+/(" "|\n)
/* tag */
tags (" "[^ \n]+){6}$
/* info token */

%%
 /* name: end of sentence */
{eos} { printf( "%s", yytext ); }
 /* name: token */
{token} { init_info(); printf( "%s", yytext ); }
 /* name: tag */
{tags} { printf( "%s", yytext );
         printf( "%s", info );
          }
 /* name: end of line */
{eol} { printf( "%s", yytext ); }
 /* name: info token */
. { strncat(info, yytext, yyleng+1);
     /* if the info is too long */
     if ( strlen(info) > 100 ) {
	fprintf (stderr, "warning: %s\n\t%s\n", 
		 yytext, "yytext > 100 characters!");
	return 1; }
	 
     info[99] = '\0'; }
%%


/* redefine yywrap()
 */
int yywrap(void)
{ 
  return (1);
}

/** Initialize array
  * @param
  * @return
  */
void init_info() {

  short int i;
  for ( i = 0; i < 100; i++) {
    info[i] = 0;
  }

}

//flex invert.lex
//gcc lex.yy.c -o invert -lfl

main () {

  yylex();
  
}

