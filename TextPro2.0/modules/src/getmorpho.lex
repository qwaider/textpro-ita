
/** 
    Recognize the morphological feature 

    The morpho analysis is transformed in Y|N format

    Y if the i-feature is true
    N if the i-feature is false

    After that, features are saved in the morpho array in this order:

    verb noun art adj prep conj punc adv inter pn pron number
  */

%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>

  // the length of the morpho features array
  short int MORPHO_LENGTH = 30;
  /* result 1 for morpho inf
            2 if EOS|eos
            -1 otherwise */
  short int result;
  // the morpho array
  short int morpho[30];
  // the token
  char morpho_token[500];
  // initialise array
  void init_morpho_feat();
%}

/* end of senetence */
eos ^(eos|EOS)/(" ")(\n)
/* eol */
eol (" ")(\n)
/* token */
token ^[^ \n]+/(" ")

/* verb */
verb " "[^ ]+"+v+"
/* mood */
verb_indicative " "[^ ]+"+v+ind"
verb_indicative_pres " "[^ ]+"+v+indic+pres"
verb_indicative_past " "[^ ]+"+v+indic+past"
verb_infinite " "[^ ]+"+v+inf"
verb_infinite_pres " "[^ ]+"+v+"(infin|infinito)"+pres"
verb_infinite_past " "[^ ]+"+v+"(infin|infinito)"+past"
verb_participle " "[^ ]+"+v+par"
verb_participle_pres " "[^ ]+"+v+part+pres"
verb_participle_past " "[^ ]+"+v+part+past"
verb_gerund " "[^ ]+"+v+ger"
verb_gerund_pres " "[^ ]+"+v+"(gerund|gerundio)"+pres"
verb_gerund_past " "[^ ]+"+v+"(gerund|gerundio)"+past"
/*
vi_or_viy " "[^ ]+"+v+"(indic|conj|cond)
vf_or_vfy " "[^ ]+"+v+inf"
vsp_or_vspy " "[^ ]+("+v+part+past+"(sing|_|nil)
vpp_or_vppy " "[^ ]+"+v+part+past+"(plur|_|nil)
vg_or_vgy " "[^ ]+"+v+gerundio"
vm_or_vmy " "[^ ]+"+v+imp"
clitic " "[^ ]+"+v+"[^ ]+pron
*/

/* noun */
noun " "[^ ]+"+n+"
/* number */
noun_sing " "[^ ]+"+n+"(f+|m+|_+|nil+)*"sing"
noun_plur " "[^ ]+"+n+"(f+|m+|_+|nil+)*"plur"
noun_neut " "[^ ]+"+n+"(f+|m+|_+|nil+)*"_"
/* number */
/*
ss " "[^ ]+"+n+"[^ ]+(sing|nil)
sp " "[^ ]+"+n+"[^ ]+(plur|nil)
sn " "[^ ]+"+n+"[^ ]+(_|nil)
yf " "[^ ]+"+n+"[^ ]+(nil)
*/

/* pronoun */
pron " "[^ +]+"+pron"
/* number */
/*
ps " "[^ +]+"+n+"[^ ]+(sing|nil)
pp " "[^ +]+"+n+"[^ ]+(plur|nil)
pn " "[^ +]+"+n+"[^ ]+(_|nil)
*/

/* adjective */
adj " "[^ ]+"+adj"
/* grade */
adj_pst  " "[^ ]+"+adj+"[^ ]+(pst|zero)
adj_comp  " "[^ ]+"+adj+"[^ ]+comp
adj_sup " "[^ ]+"+adj+"[^ ]+sup
/* number */
/*
as " "[^ ]+"+adj+"[^ ]+(sing|nil)
ap " "[^ ]+"+adj+"[^ ]+(plur|nil)
an " "[^ ]+"+adj+"[^ ]+(_|nil)
ds " "[^ ]+"+adj+"[^ ]+(sing|nil)+[^ ]+dim
dp " "[^ ]+"+adj+"[^ ]+(plur|nil)+[^ ]+dim
dn " "[^ ]+"+adj+"[^ ]+(_|nil)+[^ ]+dim
*/

/* punctuation */
punc " "[^ ]+"+punc"
/* type */
/*
xps " "[^ ]+(full_stop|semicolon|esclamation_mark)"+punc"
xpw " "[^ ]+"comma+punc"
xpb " "[^ ]+(open_parenthesis|close_parenthesis)"+punc"
xpo " "[^ ]+(quotation_mark|ellipsis|hyphen)"+punc"
*/

/* adverb */
adv " "[^ ]+"+adv"

/* article */
art " "[^ +]+"+art"
/* number */
/*
rs " "[^ ]+"+art+"[^ ]+(sing|_|nil)
rp " "[^ ]+"+art+"[^ ]+(plur|_|nil)
*/

/* conj */
conj " "[^ ]+"+conj"

/* preposition */
prep " "[^ ]+"+prep"
/* number */
/*
es " "[^ ]+"+prep+"[^ ]+"+art+"[^ ]+(sing|_|nil)
ep " "[^ ]+"+prep[^ ]+"+art+"[^ ]+(plur|_|nil)
*/

/* che, chi */
/*
cche ^che/" "
cchi ^chi/" "
*/

/* cadv */
/*
cadv ^(come|dove|quando)/" "
*/

/* interjection */
inter " "[^ ]+"+inter"

/* prper noun */
proper_noun " "[^ ]+"+pn"

/* number */
number " "[^ ]+"+adj+"[^ ]+"+num"

%%
{eos} { strncpy(morpho_token, yytext, yyleng+1);
        morpho_token[499] = '\0';
	result = 2; }
{token} { init_morpho_feat();
          //printf("%s#a\n", morpho_token);
          strncpy(morpho_token, yytext, yyleng+1);
          morpho_token[499] = '\0';
          //printf("%s#b\n", morpho_token);
          REJECT; }
{verb} { morpho[0] = 1; result = 1; REJECT; }
{verb_indicative} { morpho[1] = 1; result = 1; REJECT; }
{verb_indicative_pres} { morpho[2] = 1; result = 1; REJECT; }
{verb_indicative_past} { morpho[3] = 1; result = 1; REJECT; }
{verb_infinite} { morpho[4] = 1; result = 1; REJECT; }
{verb_infinite_pres} { morpho[5] = 1; result = 1; REJECT; }
{verb_infinite_past} { morpho[6] = 1; result = 1; REJECT; }
{verb_participle} { morpho[7] = 1; result = 1; REJECT; }
{verb_participle_pres} { morpho[8] = 1; result = 1; REJECT; }
{verb_participle_past} { morpho[9] = 1; result = 1; REJECT; }
{verb_gerund} { morpho[10] = 1; result = 1; REJECT; }
{verb_gerund_pres} { morpho[11] = 1; result = 1; REJECT; }
{verb_gerund_past} { morpho[12] = 1; result = 1; REJECT; }
{noun} { morpho[13] = 1; result = 1; REJECT; }
{noun_sing} { morpho[14] = 1; result = 1; REJECT; }
{noun_plur} { morpho[15] = 1; result = 1; REJECT; }
{noun_neut} { morpho[16] = 1; result = 1; REJECT; }
{art} { morpho[17] = 1; result = 1; REJECT; }
{adj} { morpho[18] = 1; result = 1; REJECT; }
{adj_pst} { morpho[19] = 1; result = 1; REJECT; }
{adj_comp} { morpho[20] = 1; result = 1; REJECT; }
{adj_sup} { morpho[21] = 1; result = 1; REJECT; }
{prep} { morpho[22] = 1; result = 1; REJECT; }
{conj} { morpho[23] = 1; result = 1; REJECT; }
{punc} { morpho[24] = 1; result = 1; }
{adv} { morpho[25] = 1; result = 1; REJECT; }
{inter} { morpho[26] = 1; result = 1; REJECT; }
{proper_noun} { morpho[27] = 1; result = 1; REJECT; }
{pron} { morpho[28] = 1; result = 1; REJECT; }
{number} { morpho[29] = 1; result = 1; REJECT; }
{eol} { return result; }
.
%%


/** Initialize array
  * @param
  * @return
  */
void init_morpho_feat() {

  result = -1;
  short int i;
  for ( i = 0; i < MORPHO_LENGTH; i++) {
    morpho[i] = 0;
  }
  
}


/* redefine yywrap 
 */
int yywrap(void) 
{ 
  return (1);
}


/*
main () {

  int result;
  result = yylex ();
  printf("%s", morpho_token);
  // if morpho inf exists
  if (result == 1) {
    short int i;
    for ( i = 0; i < MORPHO_LENGTH; i++) {
      char value;
      value = (morpho[i] == 1) ? 'Y' : 'N';
      printf(" %c", value);
    }
  }
  else {
    short int i;
    for ( i = 0; i < MORPHO_LENGTH; i++) {
      printf(" %S", "__nil__");
    }
  }
  
}
*/
