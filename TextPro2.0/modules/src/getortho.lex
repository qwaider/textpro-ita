
/** Get orthographic features such as capitalized token, lowercase token ...
    
    A valid input format is like this:
    
    token1 feature1 feature2 .... featureN
    token2 feature1 feature2 .... featureN
    ----   --
    tokenN feature1 feature2 .... featureN
    
    Two othografic features are defined in this way:
    
    F1(ortho[0]): M|U|L|C|D|A
    L: all lowercase letters
    U: all uppercase letters
    M: mixed-case(involving a mixture of lowercase and uppercase letters)
    C: an initial uppercase letter followed by lower case letters
    D: all numbers
    A: alpha-num letters
    N: none
    
    F1(ortho[1]): S|H|P|A|X|W|B|_
    S: for any one-character tokens
    H: for tokens that contain a hyphen
    P: for tokens that contain a point
    A: for tokens that contain an apostrophe
    C: for tokens that contain a slash
    X: for puntuation
    W: for symbol
    B: for bracket
    _: for underscore
    N: none
    
 */

%{
  #include <stdlib.h>
  #include <string.h>
  // the length of the orthograpic features array
  short int ORTHO_LENGTH = 2;
  // the orthograpic features array
  char ortho[2];
  // the token
  char ortho_token[500];
  // the tag
  char tag[100];
  // keep track of the line number
  long int lineno = 0;
  /* state 1 for tag inf
           2 if EOS|eos
           -1 othervise */
  short int state;
  // initialise array
  void init_ortho_feat();
%}

/* end of sentence 2, linea vuota */

/* eol */
eol \n
/* end of sentence */
eos ^(eos|EOS)$

/* token */
token ^[^ \n]+/(" "|\n)
/* tag */
/* tag (" ")[^ \n]+$ */
tag (" "[^ \n]+)+$

/* lower-case characters */
lower_case_chrs [-a-z����������_\'\.\&]
/* upper-case characters */
upper_case_chrs [-A-Z����������_\'\.\&]
/* capitalized characters */
capitalized_chrs [A-Z����������]
/* digit characters */
num_chrs [-+0-9%\.,`'^\\\*:\/]
/* hyphen token */
hyphen_chr ^[a-zA-Z]+-[^ \n]*/(" "|\n)
/* undescore token */
underscore_chr ^[a-zA-Z]+_[^ \n]*/(" "|\n)
/* point token */
point_chr ^[a-zA-Z]+\.[^ \n]*/(" "|\n)
/* apostrophe token */
apostrophe_chr ^[a-zA-Z]+'[^ \n]*/(" "|\n)
/* slash token */
slash_chr ^[a-zA-Z]+\/[^ \n]*/(" "|\n)
/* single char token */
single_chr ^[^ \n]{1}/(" "|\n)
/* puctuation token */
punctuation_chr ^[\.;,\-:!?]+/(" "|\n)
/* puctuation token */
punctuation_chr2 ^["'`��]+/(" "|\n)
/* bracket */
bracket_chr ^[\(\)\{\}\[\]]+/(" "|\n)
/* symbol token */
symbol_chr ^[\@\#\$\%\^\&\*\_\=\+\\\|\~\<\>\/]+/(" "|\n)


%%
 /* name: end of sentence */
{eos} { strncpy(ortho_token, yytext, yyleng+1);
        /* if the word is too long */
        ortho_token[499] = '\0';
        state = 2; }

 /* come fine frase abilita la riga vuota */
 /* ^{eol} {strncpy(ortho_token, yytext, yyleng+1); */
 /*      /* if the word is too long */
 /*         ortho_token[0] = '\0'; */
 /*        state = 2; */
 /*        lineno++; */
 /*        return state;} */

 /* name: token */
{token} { init_ortho_feat();
          strncpy(ortho_token, yytext, yyleng+1);
          /* if the word is too long */
          ortho_token[499] = '\0';
          REJECT; }

 /* name: single char token */
 /* def: true for any one-character tokens */
{single_chr} { ortho[1] = 'S'; REJECT; }
 /* name: hyphen */
 /* def: true for tokens that contain a hyphen */
{hyphen_chr} { ortho[1] = 'H'; REJECT; }
 /* name: underscore */
 /* def: true for tokens that contain a _ */
{underscore_chr} { ortho[1] = '_'; REJECT; }
 /* name: slash */
 /* def: true for tokens that contain a slash */
{slash_chr} { ortho[1] = 'C'; REJECT; }
 /* name: point */
 /* def: true for tokens that contain a point */
{point_chr} { ortho[1] = 'P'; REJECT; }
 /* name: apostrophe */
 /* def: true for tokens that contain a apostrophe */
{apostrophe_chr} { ortho[1] = 'A'; REJECT; }
 /* name: punctuation */
 /* def: true for tokens that contain a puntuation */
{punctuation_chr} { ortho[1] = 'X'; }
 /* name: punctuation */
 /* def: true for tokens that contain a puntuation */
{punctuation_chr2} { ortho[1] = 'Y'; }
 /* name: symbol */
 /* def: true for tokens that contain a symbol */
{symbol_chr} { ortho[1] = 'W'; }
 /* name: bracket */
 /* def: true for bracket tokens */
{bracket_chr} { ortho[1] = 'B'; }

 /* name: alpha token */
 /* def: true for letters */
^({lower_case_chrs}|{upper_case_chrs})+/(" "|\n) {
  if (ortho[1] != 'X' && ortho[1] != 'W' && ortho[1] != 'B') 
    ortho[0] = 'M'; REJECT; }
 /* name: upper case token */
 /* def: true for tokens that contain only upper-case letters */
^{upper_case_chrs}+/(" "|\n) { 
  if (ortho[1] != 'X' && ortho[1] != 'W' && ortho[1] != 'B') 
    ortho[0] = 'U'; }
 /* name: lower case token */
 /* def: true for tokens that contain only lower-case letters */
^{lower_case_chrs}+/(" "|\n) { 
  if (ortho[1] != 'X' && ortho[1] != 'W' && ortho[1] != 'B') 
    ortho[0] = 'L'; }
 /* name: capitalized token */
 /* def: true for alpha tokens that begin with an upper-case letter */
^({capitalized_chrs}{lower_case_chrs}+)/(" "|\n) { 
  if (ortho[1] != 'X' && ortho[1] != 'W' && ortho[1] != 'B') 
    ortho[0] = 'C'; }
 /* name: num token */
 /* def: true for digits */
^{num_chrs}+s*/(" "|\n) { 
  if (ortho[1] != 'X' && ortho[1] != 'W' && ortho[1] != 'B') 
    ortho[0] = 'D'; }
 /* name: alpha_num token */
 /* def: true for numbers and letters */
^(((({lower_case_chrs}|{upper_case_chrs})+{num_chrs}+))|({num_chrs}+({lower_case_chrs}|{upper_case_chrs})+))({lower_case_chrs}|{upper_case_chrs}|{num_chrs})*/(" "|\n) { ortho[0] = 'A'; }
 /* name: tag */
{tag} { strncpy(tag, yytext, yyleng+1);
        /* if the word is too long */
        tag[99] = '\0';
        state = 1; }
{eol} { lineno++; return state; }
.
%%

/** Initialise array
  * @param
  * @return
  */
void init_ortho_feat() {
  state = -1;
  short int i;
  for ( i = 0; i < ORTHO_LENGTH; i++) {
    ortho[i] = 'N';
  }
}

/* redefine yywrap()
 */
int yywrap(void)
{ 
  return (1);
}


//flex getortho.lex
//code > cc lex.yy.c -o first -ll  
/*
main () {
  
  printf("%s", ortho_token);
  int state;
  state = yylex();
  short int i;
  for ( i = 0; i < ORTHO_LENGTH; i++) {
    char value;
    value = ( ortho[i] == 1 ) ? 'Y' : 'N';
  printf(" %c", value);
  }

  }*/

