# Temporal Relation extraction and classification for Italian

***
Author: Paramita Mirza (FBK) and Anne-Lyse Minard (FBK)

Contact: paramita@fbk.eu, minard@fbk.eu

Date: Sept 2014

version: v1.0
***

This module is machine learning based and it has been trained on EVENTI@Evalita2014 dataset. It detects temporal relations between two events. between one event and one time expression and between two time expressions. 


Installation
------------
Copy the models used by this module:
```
cp textpro-ita/textpro-models/TempRelPro/* textpro-ita/TextPro2.0/modules/TempRelPro/models/
```

Usage TempRelPro
----------------

INPUT

In input the module takes a column format file containing the following fields:

   - token
   - tokenid
   - pos
   - full_morpho
   - comp_morpho
   - lemma
   - chunk
   - entity
   - parserid
   - head
   - deprel
   - tmx
   - tmxid
   - tmxvalue
   - event
   - eventid
   - eventclass


OUTPUT
A line is added in the header containing all the temporal relations extracted by the module:

\# TLINKS: (e1;IS_INCLUDED;tmx1) (tmx0;AFTER;tmx1)

EXAMPLES

- Running the module through textpro:
```
textpro.sh -l ita -c tlink -o output_file input_file
```

- Running the module alone:
```
sh TempRelPro.sh input_file output_file
```


Third-party tools and resources (see NOTICE)
-------------------------------

* yamcha (http://chasen.org/~taku/software/yamcha/)
* TinySVM (http://chasen.org/~taku/software/TinySVM/)
* Snowball (http://snowball.tartarus.org/index.php)
* WN domain (http://wndomains.fbk.eu/)
* Derivatario (http://derivatario.sns.it/)


Reference
---------

Mirza, Paramita and Minard, Anne-Lyse. FBK-HLT-time: a complete Italian Temporal Processing system for EVENTI-Evalita 2014. Proceedings of the First Italian Conference on Computational Linguistic CLiC-it 2014 & the Fourth International Workshop EVALITA 2014 Vol. II, 2014, pp. 44-49.


