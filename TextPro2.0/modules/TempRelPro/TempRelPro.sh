#!/bin/tcsh -f

set HOME="$TEXTPRO/modules/TempRelPro"
set YAMCHA="$TEXTPRO/modules/tools/yamcha-0.33/usr/local/bin/yamcha"
set LANGUAGE=$1
set FILETXP=$2
set FILEOUT=$3

set TLINKIN=$FILETXP-ee.tlinks
set TLINKOUT=$FILETXP-bin-ee.tagged
set TLINKETIN=$FILETXP-et.tlinks
set TLINKETOUT=$FILETXP-bin-et.tagged
set TLINKFEAT=$FILETXP-feat.tlinks
set TLINKTTOUT=$FILETXP-tt.tlinks

cd $HOME

$JAVA_HOME/bin/java -cp "$HOME/lib/libstemmer_java/java/:$HOME/lib/preprocessLearning.jar" eu.fbk.newsreader.italian.eventDetection.PreProcessLearning_v3 $FILETXP $TLINKFEAT sentId+tense+depHRel+root+SRL+connectives+pairs TLINK
#cat $FILETXP >$TLINKOUT

###TEMPORAL RELATION IDENTIFICATION --- BINARY CLASSIFICATION

python buildTempRelPairFeatures.py $TLINKFEAT ee -type bin -lang it > $TLINKIN
$YAMCHA -m $HOME/models/eventi_bin-event-event.model < $TLINKIN > $TLINKOUT


python $HOME/buildTempRelPairFeatures.py $TLINKFEAT et -type bin -lang it > $TLINKETIN
$YAMCHA -m $HOME/models/eventi_bin-event-timex.model < $TLINKETIN > $TLINKETOUT


python $HOME/buildTempRelPairFeatures.py $TLINKFEAT tt -lang it > $TLINKTTOUT

#rm $TLINKIN $TLINKETIN

awk -F"\t" '$33=="REL" || NF==0 { print; }' $TLINKOUT | cut -f -31,33 > $TLINKIN
awk -F"\t" '$32=="REL" || NF==0 { print; }' $TLINKETOUT | cut -f -30,32  > $TLINKETIN


#rm $TLINKOUT $TLINKETOUT

###TEMPORAL RELATION CLASSIFICATION --- RELTYPE CLASSIFICATION

$YAMCHA -m $HOME/models/eventi_rel-event-event.model < $TLINKIN > $TLINKOUT
$YAMCHA -m $HOME/models/eventi_rel-event-timex.model < $TLINKETIN > $TLINKETOUT

#rm $TLINKIN 
#rm $TLINKETIN

#cut -f1,2,33 $TLINKOUT > $TLINKIN
echo "# FILE: " > $FILEOUT
echo -n "# TLINKS: " >> $FILEOUT

awk -F"\t" '$32=="REL" {printf "%s ", "("$1";"$33";"$2")"; }' $TLINKOUT >> $FILEOUT
awk -F"\t" '$3=="et" { printf "%s ", "("$1";"$32";"$2")"; }' $TLINKETOUT >> $FILEOUT
awk -F"\t" '$3=="te" { printf "%s ", "("$2";"$32";"$1")"; }' $TLINKETOUT >> $FILEOUT
awk -F"\t" '$1!="" {printf "%s ", "("$1";"$3";"$2")"; }' $TLINKTTOUT >>$FILEOUT

echo "" >> $FILEOUT
echo "# FIELDS: " >> $FILEOUT

#cat $TLINKIN $TLINKETIN | grep . > $FILEOUT

rm $TLINKIN
rm $TLINKETIN
rm $TLINKOUT
rm $TLINKETOUT
rm $TLINKFEAT
rm $TLINKTTOUT
