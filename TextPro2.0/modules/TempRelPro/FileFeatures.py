#!/usr/bin/python
# -*- coding: utf-8 -*-
import re

class FileFeatures_v2:
    def __init__(self, content, filename, language="en", inverse=False):
        self.filename = filename
        self.content = content
        self.lang = language
        self.inverse = inverse
        
        self.dependencies = {}
        self.tokens = {}
        self.sentences_start = []
        self.sentences_end = []
        self.entities = {}
        self.entity_array = []
        self.entity_pairs = {}

        self.arr_tokens = []
        self.arr_events = {}
        self.arr_timex_starts = {}
        self.arr_timex_ends = {}

        self.sent_id = 0
        self.entity_idx = 0
        self.entity_id = ""
        (self.timex_startid, self.timex_endid, self.timex_type, self.timex_value) = ("", "", "", "")
        self.timex_start = False

        self.tmxlink = {}

        #for EVENTI
        self.eventi_task = "C" # task C or D

    def __parseTenseAspectPol(self, tense_aspect_pol):
        if tense_aspect_pol != "O" and tense_aspect_pol != "_" :
            tap = tense_aspect_pol.rstrip().split('+')
            return (tap[0], tap[1], tap[2])
        else:
            return ("O", "O", "O")

    def __parseDependency(self, tokenid, dep):
        if dep != "O":
            for d in dep.rstrip().split('||'):
                dependent = d.split(':')
                tokendep = dependent[0]
                deprel = dependent[1]
                if tokendep[0] == "t": tokendep = tokendep[1:]	#english corpus: tokenid = t1, italian corpus: tokenid = 1
                self.dependencies[(tokenid, tokendep)] = deprel

    def __getFieldValue(self, fieldname, cols):
        if self.lang == "it":
            #different fields for Italian-EVENTI
            if self.eventi_task == "C": #task C
                fields = ["token", "token_id", "sent_id", "pos", "lemma", "dep_rel", "timex_id", "timex_type", "timex_value", "entity", "pred_class", "event_id", "sem_role1", "sem_role2", "sem_role3", "is_arg_pred", "has_semrole", "chunk", "main_verb", "connective", "comp_morpho", "morpho_pos", "tense+aspect+polarity", "temp_rel"]

            elif self.eventi_task == "D": #task D
                fields = ["token", "token_id", "sent_id", "pos", "lemma", "dep_rel", "timex_id", "timex_type", "timex_value", "entity", "pred_class", "event_id", "sem_role1", "sem_role2", "sem_role3", "is_arg_pred", "has_semrole", "chunk", "main_verb", "connective", "comp_morpho", "morpho_pos", "tense", "aspect", "polarity", "temp_rel"]
            
        else:   #default: en
            fields = ["token", "token_id", "sent_id", "pos", "lemma", "dep_rel", "timex_id", "timex_type", "timex_value", "entity", "pred_class", "event_id", "sem_role1", "sem_role2", "sem_role3", "is_arg_pred", "has_semrole", "chunk", "main_verb", "connective", "morpho", "tense+aspect+pol", "temp_rel"]
        
        if fieldname in fields:
            if fields.index(fieldname) < len(cols): return cols[fields.index(fieldname)]
            else: return "O"
        else: return "O" 

    def parseLine(self, line):
        cols = line.rstrip().split('\t')
        
        #token, tokenid, sentid
        token = self.__getFieldValue("token", cols)
        token = token.replace("°", "")  #doesn't work for  training by Yamcha
        tokenid = self.__getFieldValue("token_id", cols)
        if tokenid[0] == "t": tokenid = tokenid[1:]	#english corpus: tokenid = t1, italian corpus: tokenid = 1
        sentid = self.__getFieldValue("sent_id", cols)
        if self.sent_id != sentid and sentid != "O":
            self.sentences_start.append(tokenid)
            self.sent_id = sentid
            if tokenid != "O":
                if int(tokenid) > 1:
                    self.sentences_end.append(str(int(tokenid)-1))

        #entity features
        eventid = self.__getFieldValue("event_id", cols)        
        predclass = self.__getFieldValue("pred_class", cols)
        timexid = self.__getFieldValue("timex_id", cols)
        timextype = self.__getFieldValue("timex_type", cols)
        timexvalue = self.__getFieldValue("timex_value", cols)

        if token[0:3] == "DCT" or token[0:3] == "ETX":     #dct or empty token (for italian corpus)
            self.entities[timexid] = (self.entity_idx, tokenid, tokenid, timextype, timexvalue, True)
            self.entity_array.append(timexid)
            self.entity_idx += 1
        else:                       #token
            #token features
            pos = self.__getFieldValue("pos", cols)
            lemma = self.__getFieldValue("lemma", cols)
            lemma = lemma.replace("°", "") #doesn't work for training with Yamcha
            entity = self.__getFieldValue("entity", cols)
            chunk = self.__getFieldValue("chunk", cols)
            if self.__getFieldValue("main_verb", cols) == "mainVb": mainvb = True
            else: mainvb = False
            conn = self.__getFieldValue("connective", cols)
            morpho = self.__getFieldValue("morpho", cols)
            
            if self.eventi_task == "C": #task C
                (tense, aspect, pol) = self.__parseTenseAspectPol(self.__getFieldValue("tense+aspect+pol", cols))          
            elif self.eventi_task == "D": #task D
                (tense, aspect, pol) = (self.__getFieldValue("tense", cols), self.__getFieldValue("aspect", cols), self.__getFieldValue("polarity", cols))
            
            self.tokens[tokenid] = (token, sentid, lemma, pos, mainvb, entity, chunk, conn, morpho, tense, aspect, pol)
            self.arr_tokens.append(tokenid)

            #dependency features
            self.__parseDependency(tokenid, self.__getFieldValue("dep_rel", cols))

            #event
            if eventid != "O":
                self.entities[eventid] = (self.entity_idx, tokenid, tokenid, predclass, "O", False)
                self.arr_events[tokenid] = eventid
                self.entity_array.append(eventid)
                self.entity_idx += 1

            #timex
            if self.entity_id == "" and timexid != "O" and eventid == "O":
                self.entity_id = timexid
                (self.timex_startid, self.timex_endid, self.timex_type, self.timex_value) = (tokenid, tokenid, timextype[2:], timexvalue)
            elif self.entity_id == timexid:
                self.timex_endid = tokenid
            elif self.entity_id != timexid and self.timex_startid != "" and self.timex_endid != "":
                self.entities[self.entity_id] = (self.entity_idx, self.timex_startid, self.timex_endid, self.timex_type, self.timex_value, False)
                self.entity_array.append(self.entity_id)
                self.arr_timex_starts[self.timex_startid] = self.entity_id
                self.arr_timex_ends[self.timex_endid] = self.entity_id
                self.entity_idx += 1
                self.entity_id = ""
                (self.timex_startid, self.timex_endid, self.timex_type, self.timex_value) = ("", "", "", "")

        #entity pairs
        if eventid != "O" or timexid != "O":
            relations = self.__getFieldValue("temp_rel", cols)
            rels = []
            if relations.rstrip() != "O" and relations.rstrip() != "_NULL_":
                rels = relations.rstrip().split('||')
                for rel in rels:
                    strrel = rel.split(':')
                    (source, target, reltype) = (strrel[0], strrel[1], strrel[2])
                    if reltype != "": self.entity_pairs[source, target] = reltype
                    
                    #inverse relations
                    reltype_inverse = self.getInverseRelation(reltype)
                    if self.inverse == True and reltype_inverse != None:
                        self.entity_pairs[target, source] = reltype_inverse


        #tmxlink
        self.getTimexRel()



    def __findTemporalConnective(self, sid, tidx_start, tidx_end):
        temp_conn = "O"
        temp_position = "O"

        for i in xrange(tidx_start, tidx_end, -1):
            if sid == self.tokens[str(i)][1]:
                if self.tokens[str(i)][6] == "Temporal":
                    temp_conn = self.tokens[str(i)][2]			#lemma
                    #temp_conn = self.tokens[str(i)][0].lower()	#token.lower()
                    temp_position = "BEFORE"
            else:
                break

        return (temp_conn, temp_position)

    def isDCT(self, entity_id):
        return self.entities[entity_id][-1]

    def getTemporalConnective(self, entity_id):
        temp_conn = "O"
        temp_position = "O"

        if not self.isDCT(entity_id):
            (eidx, tid, _, _, _, _) = self.entities[entity_id]
            tidx = int(tid)
            sid = self.tokens[tid][1]
            
            tid_begin = self.sentences_start[int(sid)-1]
            tidx_begin = int(tid_begin)

            if eidx > 1:    #because eidx 0 is always DCT
                eid_before = self.entity_array[eidx-1]
                (_, _, tid_before, _, _, _) = self.entities[eid_before]
                if tid_before != "O":
                    tidx_before = int(tid_before)
                    (temp_conn, temp_position) = self.__findTemporalConnective(sid, tidx-1, tidx_before)
                
            else:    
                (temp_conn, temp_position) = self.__findTemporalConnective(sid, tidx-1, tidx_begin-1)

            if temp_position == "O":
                if self.tokens[tid_begin][6] == "Temporal":
                    temp_conn = self.tokens[tid_begin][2]
                    temp_position = "BEGIN"

        return (temp_conn, temp_position)

    def isTimex(self, entity_id):
        if entity_id[0:3] == "tmx":
            return True
        else:
            return False

    def __getContextSignal(self, sid, tidx_start, tidx_end):
        context = []
        for i in xrange(tidx_start, tidx_end, -1):
            if sid == self.tokens[str(i)][1]:
                context.append(self.tokens[str(i)][2])				#lemma as context for finding signals
                #context.append(self.tokens[str(i)][0].lower())		#token.lower() as context for finding signals
            else:
                break
        context.reverse()
        return context

    def getTemporalSignal(self, entity_id):
        temp_signal = "O"
        temp_position = "O"
        
        if self.lang == "it":
            #different signal files for italian
            signal_time_filename = "resources/it-signal-timex-lemma.list"
            signal_event_filename = "resources/it-signal-event-lemma.list"
        else:	#default: en
            signal_time_filename = "resources/signal-timex.list"
            signal_event_filename = "resources/signal-event.list"

        if not self.isDCT(entity_id):
            (eidx, tid, _, _, _, _) = self.entities[entity_id]
            tidx = int(tid)
            sid = self.tokens[tid][1]
            
            tid_begin = self.sentences_start[int(sid)-1]
            tidx_begin = int(tid_begin)
            
            context = []
            if self.isTimex(entity_id):
                signals = [x.strip() for x in open(signal_time_filename, "r").readlines()]
            else:
                signals = [x.strip() for x in open(signal_event_filename, "r").readlines()]

            if eidx > 1:    #because eidx 0 is always DCT
                eid_before = self.entity_array[eidx-1]
                (_, _, tid_before, _, _, _) = self.entities[eid_before]
                if tid_before != "O":
                    tidx_before = int(tid_before)
                    context = " ".join(self.__getContextSignal(sid, tidx-1, tidx_before))
            else:   
                context = " ".join(self.__getContextSignal(sid, tidx-1, tidx_begin-1))

            for signal in signals:
                if " " + signal + " " in context or (signal + " " in context and context.index(signal) == 0) or (signal in context and len(signal) == len(context)):
                    sig_tokens = signal.split(" ")
                    temp_signal = "-".join(sig_tokens)
                    temp_position = "BEFORE"

            if not self.isTimex(entity_id) and temp_position == "O":
                if self.lang == "it":
                    context = " ".join(self.__getContextSignal(sid, tidx_begin+2, tidx_begin-1))
                else:	#default: en
                    context = " ".join(self.__getContextSignal(sid, tidx_begin+4, tidx_begin-1))
                for signal in signals:
                    if signal + " " in context and context.index(signal) == 0:
                        sig_tokens = signal.split(" ")
                        temp_signal = "-".join(sig_tokens)
                        temp_position = "BEGIN"

        return (temp_signal, temp_position)
        
    def getTimexRule(self, entity_id):
        #For QA-TempEval: add new feature for timespan timexes, e.g. "between" tmx1 "and" tmx2, "from" tmx1 "to" tmx 2, tmx1 "-" tmx2, tmx1 "until" tmx2 
        #                 we said that timex is "TMX-BEGIN" or "TMX-END"

        if self.isTimex(entity_id) and not self.isDCT(entity_id):
            (eidx, tid_start, tid_end, _, _, _) = self.entities[entity_id]
            tidx_start = int(tid_start)
            tidx_end = int(tid_end)
            sid = self.tokens[str(tidx_start)][1]
            tidx_begin = int(self.sentences_start[int(sid)-1])
            tidx_last = int(self.sentences_end[int(sid)-1])
            if tidx_start > tidx_begin and tidx_start < tidx_last:
                if eidx < len(self.entity_array)-1 and self.isTimex(self.entity_array[eidx+1]):
                    if self.tokens[str(tidx_start-1)][2] == "between" and self.tokens[str(tidx_end+1)][2] == "and":
                        return "TMX-BEGIN"
                    elif self.tokens[str(tidx_start-1)][2] == "from" and (self.tokens[str(tidx_end+1)][2] == "to" or self.tokens[str(tidx_end+1)][2] == "until" or self.tokens[str(tidx_end+1)][2] == "till"):
                        return "TMX-BEGIN"
                    elif self.tokens[str(tidx_end+1)][2] == "-":
                        return "TMX-BEGIN"
                    elif self.tokens[str(tidx_end+1)][2] == "until" or self.tokens[str(tidx_end+1)][2] == "till":
                        return "TMX-BEGIN"
                elif eidx > 1 and self.isTimex(self.entity_array[eidx-1]):
                    eid_before = self.entity_array[eidx-1]
                    (_, _, tid_before, _, _, _) = self.entities[eid_before]
                    tidx_before = int(tid_before)
                    if self.tokens[str(tidx_start-1)][2] == "and" and self.tokens[str(tidx_before-1)][2] == "between":
                        return "TMX-END"
                    elif (self.tokens[str(tidx_start-1)][2] == "to" or self.tokens[str(tidx_start-1)][2] == "until" or self.tokens[str(tidx_start-1)][2] == "till") and self.tokens[str(tidx_before-1)][2] == "from":
                        return "TMX-END"
                    elif self.tokens[str(tidx_end-1)][2] == "-":
                        return "TMX-END"
                    elif self.tokens[str(tidx_end-1)][2] == "until" or self.tokens[str(tidx_end-1)][2] == "till":
                        return "TMX-END"
        return "O"


    def __containDigits(self, expression):
        _digits = re.compile('\d')
        return bool(_digits.search(expression))
        
    def getRelTypeRule(self, temp_signal, pair_order, predclass, timex_type, timex_value):
        reltype_rule = "O"
        if self.lang == "it":
            if temp_signal == "entro":
                if timex_type == "TIME":
                    if pair_order == "et": reltype_rule = "BEFORE"
                    else: reltype_rule = self.getInverseRelation("BEFORE")
                elif timex_type == "DATE": 
                    if pair_order == "et": reltype_rule = "ENDED_BY"
                    else: reltype_rule = self.getInverseRelation("ENDED_BY")
                elif timex_type == "DURATION":
                    reltype_rule = "NO-REL"
                    #TODO: empty timex
            elif temp_signal == "tra":
                if timex_type == "DURATION":
                    if pair_order == "et": reltype_rule = "AFTER"
                    else: reltype_rule = self.getInverseRelation("AFTER")
            elif (temp_signal == "fino a/det" or temp_signal == "fino a" or temp_signal == "fino ad") and timex_type == "DURATION":
                reltype_rule = "NO-REL"
                #TODO: empty timex
            elif temp_signal == "da" or temp_signal == "da/det":
                if timex_type == "DATE":
                    if pair_order == "et": reltype_rule = "BEGUN_BY"
                    else: reltype_rule = self.getInverseRelation("BEGUN_BY")
                elif timex_type == "DURATION":
                    reltype_rule = "MEASURE"
            elif temp_signal == "in" or temp_signal == "in/det":
                if timex_type == "DURATION" and self.__containDigits(timex_value):
                    if pair_order == "et": reltype_rule = "AFTER"
                    else: reltype_rule = self.getInverseRelation("AFTER")
                elif timex_type == "DURATION" and not self.__containDigits(timex_value):
                    if pair_order == "et": reltype_rule = "IS_INCLUDED"
                    else: reltype_rule = self.getInverseRelation("IS_INCLUDED")
            elif temp_signal == "per":
                if timex_type == "DURATION":
                    reltype_rule = "MEASURE"
            
        return reltype_rule

    def __isMainEvent(self, event_id):
        tid = self.entities[event_id][1]
        sid = self.tokens[tid][1]
        tid_end = self.sentences_end[int(sid)-1]
        if (tid, tid_end) in self.dependencies and self.dependencies[(tid, tid_end)] == "P":
            return True
        else:
            return False

    def getDependency(self, tid1, tid2):
        if (tid1, tid2) in self.dependencies:
            dep_rel = self.dependencies[(tid1, tid2)]
            dep_order = "normal"
        elif (tid2, tid1) in self.dependencies:
            dep_rel = self.dependencies[(tid2, tid1)]
            dep_order = "reverse"
        else:
            dep_rel = "O"
            dep_order = "O"
        return (dep_rel, dep_order)                                

    def getDistance(self, sentid1, sentid2, eidx1, eidx2):
        if sentid1 == "O" or sentid2 == "O":
            sent_distance = -1
        else:
            sent_distance = abs(int(sentid1) - int(sentid2))
        if sent_distance == 0:
            ent_distance = abs(eidx1 - (eidx2-1))
        else:
            ent_distance = -1
        if eidx1 - (eidx2-1) < 0: ent_order = "normal"
        else: ent_order = "reverse"
        return (sent_distance, ent_distance, ent_order)

    def getLemmaTokenTimex(self, eid):
        (eidx, tid_start, tid_end, _, _, _) = self.entities[eid]
        lemma = "O"
        token = "O"
        if not self.isDCT(eid):
            lemma = ""
            token = ""
            for i in range(int(tid_start), int(tid_end)):
                lemma += self.tokens[str(i)][2] + "_"
                token += self.tokens[str(i)][0] + "_"
            lemma += self.tokens[tid_end][2]
            token += self.tokens[tid_end][0]
        return (lemma, token)
        
    def getInverseRelation(self, rel):
        relations = ["BEFORE", "AFTER", "INCLUDES", "IS_INCLUDED", "DURING", "DURING_INV", "IBEFORE", "IAFTER", "BEGINS", "BEGUN_BY", "ENDS", "ENDED_BY"]
        if rel in relations:
            relIdx = relations.index(rel)
            inverse_rel = ""
            if relIdx % 2 == 0:
                inverse_rel = relations[relIdx+1]
            else:
                inverse_rel = relations[relIdx-1]
            return inverse_rel
        else:
            return None

    def getFeatures(self):
        i = 0
        for line in self.content.rstrip().split('\n'):
            if line != "" and "# FILE:" not in line and "# DATE:" not in line and "# FIELDS:" not in line:    #skip the file descriptor
                self.parseLine(line)
            i += 1

        ###testing purpose###
        #print self.dependencies
        #print self.tokens
        #print self.sentences_start
        #print self.sentences_end
        #print self.entities
        #print self.entity_array
        #print self.entity_pairs
        #for (e1, e2) in self.entity_pairs:
            #print e1, e2, self.entity_pairs[(e1, e2)]
            #print e1, e2, self.getTemporalConnective(e1)
            #print e1, e2, self.getTemporalConnective(e2)
            #print e1, e2, self.getTemporalSignal(e1)
            #print e1, e2, self.getTemporalSignal(e2)
                
        #print self.__isMainEvent("pr1")
        

    def __getDateComponents(self, date):
        date_cols = date.split("-")
        (y, m, d, mr, era) = (0,0,0,[],"")
        if len(date_cols) == 1:
            if date_cols[0].isdigit(): y = int(date_cols[0])
            else: era = date_cols[0]
        elif len(date_cols) == 2:
            if date_cols[1].isdigit(): (y, m) = (int(date_cols[0]), int(date_cols[1]))
            elif date_cols[1][0] == "Q":
                if date_cols[1][1].isdigit(): m = int(date_cols[1][1])
                if date_cols[1][1] == "1":
                    mr.append(1)
                    mr.append(2)
                    mr.append(3)
                elif date_cols[1][1] == "2":
                    mr.append(4)
                    mr.append(4)
                    mr.append(6)
                elif date_cols[1][1] == "3":
                    mr.append(7)
                    mr.append(8)
                    mr.append(9)
                elif date_cols[1][1] == "4":
                    mr.append(10)
                    mr.append(11)
                    mr.append(12)

        elif len(date_cols) == 3 and date_cols[1].isdigit():
            day = date_cols[2]
            if "T" in day:
                day = date_cols[2].split("T")[0]
            if day.isdigit():
                (y, m, d) = (int(date_cols[0]), int(date_cols[1]), int(day))

        return (y, m, d, mr, era)

    def __getDateRelation(self, y1, m1, d1, mr1, y2, m2, d2, mr2):
        if y1 < y2: return "BEFORE"
        elif y1 > y2: return "AFTER"
        elif y1 == y2 and m1 == m2 and d1 == d2:
            return "SIMULTANEOUS"
        else:
            if mr1 != [] and mr2 == [] and m2 in mr1: return "INCLUDES"
            elif mr1 == [] and mr2 != [] and m1 in mr2: return "IS_INCLUDED"
            elif mr1 != [] and mr2 != []:
                if m1 < m2: return "BEFORE"
                elif m1 > m2: return "AFTER"
                else: return "SIMULTANEOUS"
            else:
               if m1 < m2: return "BEFORE"
               elif m1 > m2: return "AFTER"
               elif d1 != 0 and d2 != 0:
                   if d1 < d2: return "BEFORE"
                   elif d1 > d2: return "AFTER"
                   else: return "SIMULTANEOUS"
               else:
                   if d1 == 0:
                       return "INCLUDES"
                   else:
                       return "IS_INCLUDED"

    def __getTmxDateRelation(self, date1, date2, dct):
        era = ["PAST_REF", "PRESENT_REF", "FUTURE_REF"]
        (y1, m1, d1, mr1, era1) = self.__getDateComponents(date1)
        (y2, m2, d2, mr2, era2) = self.__getDateComponents(date2)
        (y3, m3, d3, mr3, era3) = self.__getDateComponents(dct.split("T")[0])

        if not "XX" in date1 and not "XX" in date2:

            if era1 != "" and era2 != "" and era1 in era and era2 in era:
                if era.index(era1) < era.index(era2): return "BEFORE"
                elif era.index(era1) > era.index(era2): return "AFTER"
                else: return "SIMULTANEOUS"
            elif era1 != "" and era1 in era and era2 == "":
                if era1 == "PAST_REF":
                    if self.__getDateRelation(y2, m2, d2, mr2, y3, m3, d3, mr3) == "BEFORE": return "INCLUDES"
                    else: return "BEFORE"
                elif era1 == "PRESENT_REF":
                    if self.__getDateRelation(y3, m3, d3, mr3, y2, m2, d2, mr2) == "SIMULTANEOUS": return "INCLUDES"
                    else: return self.__getDateRelation(y3, m3, d3, mr3, y2, m2, d2, mr2)
                elif era1 == "FUTURE_REF":
                    if self.__getDateRelation(y2, m2, d2, mr2, y3, m3, d3, mr3) == "AFTER": return "INCLUDES"
                    else: return "AFTER"
            elif era1 == "" and era2 != "" and era2 in era:
                if era2 == "PAST_REF":
                    if self.__getDateRelation(y1, m1, d1, mr1, y3, m3, d3, mr3) == "BEFORE": return "IS_INCLUDED"
                    else: return "AFTER"
                elif era2 == "PRESENT_REF":
                    if self.__getDateRelation(y1, m1, d1, mr1, y3, m3, d3, mr3) == "SIMULTANEOUS": return "IS_INCLUDED"
                    else: return self.__getDateRelation(y1, m1, d1, mr1, y3, m3, d3, mr3)
                elif era2 == "FUTURE_REF":
                    if self.__getDateRelation(y1, m1, d1, mr1, y3, m3, d3, mr3) == "AFTER": return "IS_INCLUDED"
                    else: return "BEFORE"
            else:
                return self.__getDateRelation(y1, m1, d1, mr1, y2, m2, d2, mr2)

        return "NONE"

    def __getTimeComponents(self, time):
        time_cols = time.split(":")
        (h, m, s, r) = (0, 0, 0, [])
        if len(time_cols) == 1:
            if time_cols[0].isdigit(): h = int(time_cols[0])
            else:
                if time_cols[0] == "MO":
                    r.append(1)
                    r.append(2)
                    r.append(3)
                    r.append(4)
                    r.append(5)
                    r.append(6)
                    r.append(7)
                    r.append(8)
                    r.append(9)
                    r.append(10)
                    r.append(11)
                elif time_cols[0] == "AF":
                    r.append(13)
                    r.append(14)
                    r.append(15)
                    r.append(16)
                elif time_cols[0] == "EV":
                    r.append(17)
                    r.append(18)
                    r.append(19)
                    r.append(20)
                elif time_cols[0] == "NI":
                    r.append(21)
                    r.append(22)
                    r.append(23)
                    r.append(24)
                    r.append(0)
        elif len(time_cols) == 2:
            h = int(time_cols[0])
            m = int(time_cols[1])
        elif len(time_cols) == 3:
            h = int(time_cols[0])
            m = int(time_cols[1])
            s = int(time_cols[2])
        return (h, m, s, r)

    def __getTimeRelation(self, time1, time2):
        partday = ["MO", "AF", "EV", "NI"]
        (h1, m1, s1, r1) = self.__getTimeComponents(time1)
        (h2, m2, s2, r2) = self.__getTimeComponents(time2)

        if r1 != [] and r2 == [] and h2 in r1: return "INCLUDES"
        elif r1 == [] and r2 != [] and h1 in r2: return "IS_INCLUDED"
        elif r1 != [] and r2 != []:
            if partday.index(time1) < partday.index(time2): return "BEFORE"
            elif partday.index(time1) > partday.index(time2): return "AFTER"
            else: return "SIMULTANEOUS"
        else: 
            if h1 < h2: return "BEFORE"
            elif h1 > h2: return "AFTER"
            else:
                if m1 < m2: return "BEFORE"
                elif m1 > m2: return "AFTER"
                else:
                    if s1 < s2: return "BEFORE"
                    elif s1 > s2: return "AFTER"
                    else: return "SIMULTANEOUS"

    def getTimexRel(self):
        for i in range(0,len(self.entity_array)):
            eid1 = self.entity_array[i]
            for j in range(i+1, len(self.entity_array)):
                if j<len(self.entity_array):
                    eid2 = self.entity_array[j]
                    
                    if self.isTimex(eid1) and self.isTimex(eid2) and eid1 != eid2:
                        (_, _, _, eid1_type, eid1_value, _) = self.entities[eid1]
                        (_, _, _, eid2_type, eid2_value, _) = self.entities[eid2]
                        (_, _, _, eid_dct_type, eid_dct_value, _) = self.entities[self.entity_array[0]]
                        if eid1_type == "DATE" and eid2_type == "TIME" and eid1_value in eid2_value:
                            self.tmxlink[(eid1, eid2)] = "INCLUDES"
                        elif eid1_type == "TIME" and eid2_type == "DATE" and eid2_value in eid1_value:
                            self.tmxlink[(eid1, eid2)] = "IS_INCLUDED"
                        elif eid1_type == "DATE" and eid2_type == "DATE":
                            tttlink = self.__getTmxDateRelation(eid1_value, eid2_value, eid_dct_value)
                            if tttlink != "NONE":
                                self.tmxlink[(eid1, eid2)] = tttlink
                        elif eid1_type == "TIME" and eid2_type == "TIME":
                            if len(eid1_value.split("T")) == 1: (eid1_date, eid1_time) = (eid1_value, "0")
                            else: (eid1_date, eid1_time) = eid1_value.split("T")
                            if len(eid2_value.split("T")) == 1: (eid2_date, eid2_time) = (eid2_value, "0")
                            else: (eid2_date, eid2_time) = eid2_value.split("T")
                            if eid1_date == eid2_date:
                                tttlink = self.__getTimeRelation(eid1_time, eid2_time)
                                if tttlink != "NONE":
                                    self.tmxlink[(eid1, eid2)] = tttlink
                            else:
                                tttlink = self.__getTmxDateRelation(eid1_date, eid2_date, eid_dct_value)
                                if tttlink != "NONE":
                                    self.tmxlink[(eid1, eid2)] = tttlink
