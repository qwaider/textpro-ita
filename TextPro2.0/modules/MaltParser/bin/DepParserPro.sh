#! /bin/tcsh -f

### MaltParser         ###
### V1.4.3, 2010      ###

### MaltParser Version ###
set VERSION = "1.4.3, 2010"

### Do not modify ###
set MALTPARSER_DIR = $TEXTPRO/modules/MaltParser
set MODEL_DIR = $MALTPARSER_DIR/model
set MODEL_NAME = "" #current model name
set ITA_MODEL_NAME = "tut-tagpro-fulltrain.mco" # ITA model

### Initialize Parameters ###
set outFileName = "" #use this FILE as output file
set inFileName = "" #use this FILE as input file
set language = "ITA" #the language: ENG|ITA
set verb = "" #verbose mode

### Read Parameters ###
while ( {$1} != {} )
                
    if ( {$1} == "-h" || {$1} == "--help" ) then
        echo "MaltParser"
        echo "Copyright (C) 2007 All rights reserved."
        echo ""
        echo "Usage: MaltParser [options] file"
        echo "  -h, --help                        show this help and exit"
        echo "  -v, --version                     show the version and exit"
        echo "  -s, --system                      check the system and exit"
        echo "  -V, --verbose                     verbose mode"
        echo "  -l, --language=LANGUAGE           the language [ENG|ITA]  (default "$language")"
#        echo "  -a, --algorithm=ALGORITHM         use this algorithm [PKE|PKI]  (default "$algorithm")"
        echo "  -o, --output=FILE                 use FILE as output file"
        echo ""
        exit 0
    else if ( {$1} == "-v" || {$1} == "--version" ) then
        echo "MaltParser Version: "$VERSION
        exit 0
    else if ( {$1} == "-s" || {$1} == "--system" ) then
        $MALTPARSER_DIR/bin/TestMaltParser.sh
        exit 0
    else if ( {$1} == "-V" || {$1} == "--verbose" ) then
        set verb = $1
    else if ( {$1} == "-a" || {$1} == "--algorithm" ) then
        shift
        set algorithm = $1
    else if ( {$1} == "-l" || {$1} == "--language" ) then
        shift
        set language = $1
    else if ( {$1} == "-o" || {$1} == "--output" ) then
        shift
        set outFileName = $1
    else if ( $# == 1 ) then
        set inFileName = $1
    else
        $MALTPARSER_DIR/bin/MaltParser.sh -h
        exit 0
    endif

    shift

end

### Check Parameters ###
if ( $inFileName == "" ) then
    echo "Error: Missing file name!"
    exit 1
endif
if ( ! -f $inFileName ) then
    echo "Error: No such file or directory!"
    exit 1
endif
# if ( $algorithm != "PKE" && $algorithm != "PKI" ) then
#     echo "Error: Unrecognized algorithm "$algorithm"!"
#     exit 1
# endif
if ( $language != "ENG" && $language != "ITA" ) then
    echo "Error: Unrecognized language option "$language"!"
    exit 1
endif

### Set Variables ###
# if ( $algorithm == "PKE" && $language == "ENG" ) then
#     set MODEL_NAME = $PKE_ENG_MODEL_NAME 
# else if ( $algorithm == "PKE" && $language == "ITA" ) then
#     set MODEL_NAME = $ITA_MODEL_NAME
# else if ( $algorithm == "PKI" && $language == "ENG" ) then
#     set MODEL_NAME = $PKI_ENG_MODEL_NAME 
# else if ( $algorithm == "PKI" && $language == "ITA" ) then
#     set MODEL_NAME = $ITA_MODEL_NAME
# endif


if ( $outFileName != "" ) then
    set outFileName = "-o "$outFileName
endif

#echo "$JAVA_HOME/bin/java -mx3800m -jar $MALTPARSER_DIR/malt.jar -c $MODEL_NAME -w $MODEL_DIR/$language -i $inFileName $outFileName -m parse"

### Run the System ###
if ( $language == "ITA" ) then
    set MODEL_NAME = $ITA_MODEL_NAME
    $JAVA_HOME/bin/java -mx3800m -jar $MALTPARSER_DIR/malt.jar -c $MODEL_NAME -w $MODEL_DIR/$language -i $inFileName $outFileName -m parse
endif
