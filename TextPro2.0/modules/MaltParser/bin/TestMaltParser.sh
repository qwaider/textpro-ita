#! /bin/tcsh -f

### TestMaltParser         ###
### V1.4.3, 2010          ###

### TestMaltParser Version ###
set VERSION = "1.4.3, 2010"

### Do not modify ###
set MALTPARSER_DIR = $TEXTPRO/MaltParser

$MALTPARSER_DIR/bin/MaltParser.sh -v

echo ""
echo "testing ..."

$MALTPARSER_DIR/bin/MaltParser.sh -o $MALTPARSER_DIR/test/test.pke.eng.model.tst $MALTPARSER_DIR/test/test.pke.eng.model.txt
$MALTPARSER_DIR/bin/MaltParser.sh -l ITA -o $MALTPARSER_DIR/test/test.pke.ita.model.tst $MALTPARSER_DIR/test/test.pke.ita.model.txt

echo

set diff_eng = "eng-model"
set diff_eng = `cmp $MALTPARSER_DIR/test/test.pke.eng.model.tst $MALTPARSER_DIR/test/test.pke.eng.model.result | cut -f1 -d' '`
set diff_ita = "ita-model"
set diff_ita = `cmp $MALTPARSER_DIR/test/test.pke.ita.model.tst $MALTPARSER_DIR/test/test.pke.ita.model.result | cut -f1 -d' '`
if ( !(-z $MALTPARSER_DIR/test/test.pke.eng.model.tst) && !(-z $MALTPARSER_DIR/test/test.pke.ita.model.tst) && $diff_ita == "" && $diff_eng == "") then
echo "================"
echo "All tests passed"
echo "================"
else
echo "================"
echo "TEST FAILED!"
echo "================"
endif

rm $MALTPARSER_DIR/test/*.tst
