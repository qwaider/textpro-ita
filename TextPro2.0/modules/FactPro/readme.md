# Event factuality annotation for Italian

***
Author: Federico Nanni and Anne-Lyse Minard (FBK)

Contact: minard@fbk.eu

Date: Sept 2014

version: v1.0
***

 It is trained on Fact-Ita Bank (http://hlt-nlp.fbk.eu/technologies/fact-ita-bank). It describes the factuality of an event (detected by the EventPro module) through 3 attributes: polarity, certainty and time. For each token it adds the following information (as columns):

- polarity: the polarity of the event (POL_POS, POL_NEG, O);
- certainty: how certain the source is about the event (CERTAIN, UNCERTAIN, O);
- evtime: when an event occured or will occur (NON_FUTURE, FUTURE, O).


Usage FactPro
-------------

INPUT

In input the module takes a column format file containing the following fields (and no header):

   - token
   - tokenid
   - pos
   - comp_morpho
   - lemma
   - chunk
   - entity
   - event
   - eventclass


OUTPUT

The output is a column format file containing three columns:

   - polarity
   - certainty
   - evtime


EXAMPLES

- Running the module through textpro:
```
textpro.sh -l ita -c polarity+certainty+evtime -o output_file input_file
```

- Running the module alone:
```
sh FactPro.sh input_file output_file
```


Third-party tools and resources (see NOTICE)
-------------------------------

* yamcha (http://chasen.org/~taku/software/yamcha/)
* TinySVM (http://chasen.org/~taku/software/TinySVM/)
* Derivatario (http://derivatario.sns.it/)

