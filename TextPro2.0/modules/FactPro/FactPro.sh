#!/bin/tcsh -f

set HOME="$TEXTPRO/modules/FactPro/"
set YAMCHA="$TEXTPRO/modules/tools/yamcha-0.33/usr/local/bin/yamcha"
set LANGUAGE=$1
set FILETXP=$2
set FILEOUT=$3

set FILEFEAT=$FILETXP.feat
set FILERES=$FILETXP.res

cd $HOME

python $HOME/preprocess_factuality_apply.py $FILETXP $FILEFEAT

cat $FILEFEAT | $YAMCHA -m $HOME/models/negation_factuality_all.model | $YAMCHA -m $HOME/models/certainty_factuality_all.model | $YAMCHA -m $HOME/models/time_factuality_all.model | cat > $FILERES

#res in col 30, 31 and 32
cut -f 30-32 $FILERES >$FILEOUT

rm $FILEFEAT
rm $FILERES
