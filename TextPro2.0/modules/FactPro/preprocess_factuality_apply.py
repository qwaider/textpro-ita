#!/usr/bin/python2.6

#usage: python evaluation_NAF.py


import os 
import re
import sys 
import math
import commands
import codecs
from xml.dom import minidom
import csv
from pprint import pprint

col_token = 0
col_pos = 2
col_morpho = 3
col_chunk = 5

def get_arg(index):
    return sys.argv[index]   #sys.argv gives the list of command line arguments passed to the script


def input_and_evaluate():   #questo prende l'input e analizza se una cartella o un singolo file
    global lemmasplit
    invalid = 'false' 
    #arg1 = get_arg(1) # this takes the second argument in the command line
    input_textpro = get_arg(1) #questo prende il file textpro
    out_folder = get_arg(2) #questo prende l'output
    global directory_path 
    directory_path = get_directory_path(sys.argv[0]) #sys.argv gives the list of command line arguments passed to the script

    # both arguments are directories 
    if os.path.isdir(input_textpro):
        evaluate_folder(input_textpro, out_folder)
    elif os.path.isfile(input_textpro):
        read_CAT_file(input_textpro, out_folder)
        


     
def evaluate_folder(input_textpro, out_folder): #qui prende la cartella e esamina se dentro ci sono file o cartelle
    for file in os.listdir(input_textpro):
        if os.path.isdir(input_textpro+file):
            subdir = file+'/'
            if debug >= 1: 
                print 'Traverse files in Directory', gold+subdir
            evaluate_folder(input_textpro+subdir, out_folder)
        else:
            fileName = input_textpro +  file
            if not re.search('DS_Store', file): 
                read_CAT_file(fileName, out_folder+extract_name(fileName))


def extract_name(filename): #questo individua il nome del singolo file
    parts = re.split('/', filename)
    length = len(parts)
    return parts[length-1]


def get_directory_path(path):  
    name = extract_name(path)
    dir = re.sub(name, '', path) 
    if dir == '': 
        dir = './'
    return dir 

# read the files, extract a list of token for each file and put it in a txt file in the folder output
def read_CAT_file(filetxp, out_folder):

    lemmasplit = create_lemmaList(filetxp)

    print filetxp
    
    # da qui in poi le feature vengono create e aggiunte alla lista lemmasplit

    #add_event_feature (list_events, lemmasplit, list_token)
    find_report(lemmasplit)#certainty
    modal_verbs(lemmasplit)#certainty
    modal_verbs_close(lemmasplit)#certainty
    modal_trigger(lemmasplit)#certainty
    modal_value(lemmasplit)#certainty
    derivatario(lemmasplit)#certainty
    mood_verb(lemmasplit)#certainty and time
    adverbs_before(lemmasplit)#certainty
    
    add_trigger_feature (lemmasplit)#negation
    add_before_trigger (lemmasplit)#negation
    add_close_trigger (lemmasplit)#negation
    add_distance_trigger (lemmasplit)#negation
    add_multiple_triggers (lemmasplit)#negation
    get_conditional_construction(lemmasplit)#negation and certainty

    tense_event(lemmasplit) #time
    tense_verb_around(lemmasplit) #time
    mood_verb_around(lemmasplit) #time
    preposition(lemmasplit) #time
    preposition_before(lemmasplit) #time
    
    #add_last_col(lemmasplit)

  

    WriteFile = codecs.open(out_folder, 'w')


    #qui lemmasplit viene salvata nel file che stato specificato in output
    for inner_list in lemmasplit:
        for i in range(0, len(inner_list)):
            item = inner_list[i]
            WriteFile.write(str(item))
            if i < len(inner_list)-1:
                WriteFile.write("\t")
        WriteFile.write("\n")
        

def create_lemmaList (fileName):

    lemmasplit = []
    file_name = extract_name(fileName)

    lemmaList = open (fileName).readlines()
    for line in lemmaList:
        if line != "\n":
            line = line[:-1]
            elements = line.split('\t')
            lemmasplit.append(elements)
        else:
            lemmasplit.append([])
    lemmasplit.append([])

# questo per meglio gestire i casi in cui ci sono piu morpho
    for i in range (0, len(lemmasplit)):
        line = lemmasplit[i]
        if len(line)>1:
            if re.search(" ", line[3]):
                morph = line[3].split(" ")
                line[3] = morph[0]

# qui uguale ma per i lemma
    for i in range (0, len(lemmasplit)):
        line = lemmasplit[i]
        if len(line)>1:
            if re.search(" ", line[4]):
                lemma = line[4].split(" ")
                line[4] = lemma[0]


    for i in range (0, len(lemmasplit)):
        if len(lemmasplit[i]) > 1 and (lemmasplit[i][8] == "STATE" or lemmasplit[i][8] == "I_STATE"):
            lemmasplit[i][7] = "O"
            lemmasplit[i][8] = "O"

    return lemmasplit




def preposition (lemmasplit):


   for j in range(0, len(lemmasplit)):
        line = lemmasplit[j]


        if len(line) > 1:
            if line[2] == "E" or line[2] == "ES" or line[2] == "EP":
                line.append("PREP")

            else:
                line.append("O")




def preposition_before (lemmasplit):


   for j in range(0, len(lemmasplit)):
        line = lemmasplit[j]

        if len(line) > 1 and "EVENT" in line[7]:

            line_before = lemmasplit[j-1]
            if len(line_before) > 1:
                if line_before[0] == "per" or line_before[2] == "Per":
                    line.append("PER_BEFORE")

                else:
                    line.append("O")


            elif len(line) > 1:
                line.append("O")

            else:
                break

        elif len(line) > 1:
            line.append("O")




def mood_verb_around (lemmasplit):

    for j in range (0, len(lemmasplit)):
        line = lemmasplit[j]
        if len(line)>1 and "EVENT" in line[7]:
            for i in range (j-1, j-(len(lemmasplit)-j+1), -1):
                f = 0
                if len(lemmasplit[i]) > 1 and lemmasplit[i][0] != ":" and lemmasplit[i][0] != ";":
                    if "V" in lemmasplit[i][2]:
                        pos = lemmasplit[i][3].split("+")
                        if len(pos) > 3:
                            f =pos[2]
                            break

                elif len(lemmasplit[i])<1:
                    for k in range(j+1, j+(len(lemmasplit)-j-1)):
                        if len(lemmasplit[k]) > 1 and lemmasplit[k][0] != ":" and lemmasplit[k][0] != ";":
                            if "V" in lemmasplit[k][2]:
                                pos = lemmasplit[k][3].split("+")
                                if len(pos) > 3:
                                    f =pos[2]
                                    break
                                else:
                                    break


                        else:
                            break

            if f>0:
                line.append(f)
            else:
                line.append("O")

        elif len(line)>1:
            line.append("O")     

def tense_verb_around (lemmasplit):

    for j in range (0, len(lemmasplit)):
        line = lemmasplit[j]
        if len(line)>1 and "EVENT" in line[7]:
            for i in range (j-1, j-(len(lemmasplit)-j+1), -1):
                f = 0
                if len(lemmasplit[i]) > 1 and lemmasplit[i][0] != ":" and lemmasplit[i][0] != ";":
                    if "V" in lemmasplit[i][2]:
                        pos = lemmasplit[i][3].split("+")
                        if len(pos) > 3:
                            f =pos[3]
                            break

                elif len(lemmasplit[i])<1:
                    for k in range(j+1, j+(len(lemmasplit)-j-1)):
                        if len(lemmasplit[k]) > 1 and lemmasplit[k][0] != ":" and lemmasplit[k][0] != ";":
                            if "V" in lemmasplit[k][2]:
                                pos = lemmasplit[k][3].split("+")
                                if len(pos) > 3:
                                    f =pos[3]
                                    break
                                else:
                                    break


                        else:
                            break

            if f>0:
                line.append(f)
            else:
                line.append("O")

        elif len(line)>1:
            line.append("O")              



def tense_event (lemmasplit):

    for i in range(0, len(lemmasplit)):
        line = lemmasplit[i]
        if len(line)>1 and "EVENT" in line[7]:
            if re.search("pres", line[3]):
                line.append("VER_PRES")

            elif re.search("pass", line[3]):
                line.append("VER_PASS")
       
            elif re.search("fut", line[3]):
                line.append("VER_FUT")
            else:
                line.append("O")

        elif len(line)>1:
            line.append("O")



def find_report (lemmasplit):
    
    for j in range(0, len(lemmasplit)):
        line = lemmasplit[j]
        if len(line) > 1 and "EVENT" in line[7] and line[8] == "REPORTING":
            line.append("REPORTING")
        elif len(line) > 1:
            line.append("O")



def modal_verbs (lemmasplit):

    modallist = open ("resources/modal_list.txt").readlines()

    for i in range (0, len(lemmasplit)):
        line = lemmasplit [i]

        if len(line) > 1:
            token = line [4] + '\n'
            if token in modallist:
                line.append("MODAL")

            else:
                line.append("O")



def modal_verbs_close (lemmasplit):

    for j in range(0, len(lemmasplit)):
        line = lemmasplit[j]
        a = 0
        if len(line) > 1 and "EVENT" in line[7]:
            for i in range (j-1, j-100, -1):
                if len(lemmasplit[i]) > 1 and lemmasplit[i][0] != ":" and lemmasplit[i][0] != ";" and i>0:
                    if lemmasplit[i][10] == "MODAL":
                            a = a + 1
                else:
                    break


            if a > 0:
                line.append("MODAL_BEFORE")

            else:
                line.append("O")


        elif len(line) > 1:
            line.append("O")



def modal_trigger (lemmasplit):

    modallist = codecs.open ("resources/modallist.txt",'r',encoding='utf-8').readlines()

    for i in range (0, len(lemmasplit)):
        line = lemmasplit [i]

        if len(line) > 1:
            token = line [4] + '\n'
            if token in modallist:
                line.append("MODAL_TRIGGER")

            else:
                line.append("O")




def modal_value (lemmasplit):

    for j in range(0, len(lemmasplit)):
        line = lemmasplit[j]
        a = 0
        if len(line) > 1 and "EVENT" in line[7]:
            for i in range (j-1, j-100, -1):
                if len(lemmasplit[i]) > 1 and lemmasplit[i][0] != ":" and lemmasplit[i][0] != ";" and i>0:
                    if lemmasplit[i][10] == "MODAL":
                        a = a + 1
                        b = lemmasplit[i][4]
                else:
                    break


            if a > 0:
                line.append(b)

            else:
                line.append("O")


        elif len(line) > 1:
            line.append("O")



def derivatario (lemmasplit):

    derivatario = codecs.open ("resources/derivatario.csv.txt", ).readlines()

    derivatario_dict = {}

    for i in range (0, len(derivatario)):
        elements = derivatario[i]
        line = elements.split('\t')
        key = line[0]
        value = line[1]

        derivatario_dict.update({key:value})

    for i in range(0, len(lemmasplit)):
        line = lemmasplit[i]

        if len(line)>1:
            if line[2] == "SS" or line[2] == "SP" or line[2] == "SN" or line[2] == "SPN":
                word = line[4]
                if word in derivatario_dict:

                    value_deriv = derivatario_dict[line[4]]
                    line.append(value_deriv)

                else:
                    line.append("O")

            else:
                line.append("O")


def mood_verb (lemmasplit):

    for i in range (0, len(lemmasplit)):
        line = lemmasplit[i]

        if len(line)>1 and re.search("infinito", line[3]):
            line.append("VER_INF")

        elif len(line)>1 and re.search("cong", line[3]):
            line.append("VER_CONG")
       
        elif len(line)>1 and re.search("cond", line[3]):
            line.append("VER_COND")

        elif len(line)>1 and re.search("indic", line[3]):
            line.append("VER_INDIC")

        elif len(line)>1 and re.search("part", line[3]):
            line.append("VER_PART")

        elif len(line)>1:
            line.append("O")


def adverbs_before (lemmasplit):

  for j in range(0, len(lemmasplit)):
        line = lemmasplit[j]
        checker = None

        if len(line) > 1 and "EVENT" in line[7]: 


            for i in range (j-1, j-5, -1):
                if len(lemmasplit[i]) > 1 and lemmasplit[i][0] != ":" and lemmasplit[i][0] != ";":
                    if lemmasplit[i][2] == "B":
                        checker = True
                        b = lemmasplit[i][0]
                        break

                elif len(lemmasplit[i])<1:
                    break

        if checker:
            line.append(b)


        elif len(line) > 1:
            line.append("O")



        
# questo aggiunge la feature trigger ai trigger, viene meglio gestito il caso di dovere, trigger solo all'imperfetto

def add_trigger_feature(lemmasplit):

    triggerList = open ("resources/before_trigger_time.txt").readlines()
    for line in lemmasplit:
        if len(line) > 1:
            token = line [4] + '\n'
            if token in triggerList:
                line.append("TRIGGER")

            elif line[4] == "dovere" and re.search("dovere\+v\+indic\+imperf", line[3]):
                line.append("TRIGGER")

            else:
                line.append("O")


# qui si aggiunge la feature trigger_before se c'e un trigger nella stessa frase
#puo ogni tanto creare problemi con trigger molto lontani che comunque influenzano
def add_before_trigger(lemmasplit):

    for j in range(0, len(lemmasplit)):
        line = lemmasplit[j]
        a = 0
        if len(line) > 1 and "B-EVENT" in line[7]:
            for i in range (j-1, j-100, -1):
                if len(lemmasplit[i]) > 1 and lemmasplit[i][0] != ":" and lemmasplit[i][0] != ";":
                    if lemmasplit[i][17] == "TRIGGER":
                            a = a + 1
                else:
                    break


            if a > 0:
                line.append("TRIGGER_BEFORE")

            else:
                line.append("O")


        elif len(line) > 1 and "I-EVENT" in line[7]:

            for i in range (j-1, j-100, -1):
                if len(lemmasplit[i]) < 7:
                    break
                elif "B-EVENT" in lemmasplit[i][7]:
                    line.append(lemmasplit[i][17])
                    break


        elif len(line) > 1:
            line.append("O")


# questa e una feature uguale a quella di prima ma solo per il trigger close (quindi in un range di 4)
def add_close_trigger(lemmasplit):

    for j in range(0, len(lemmasplit)):
        line = lemmasplit[j]
        a = 0
        if len(line) > 1 and "B-EVENT" in line[7]:
            for i in range (j-1, j-5, -1):
                if len(lemmasplit[i]) > 1:
                    if lemmasplit[i][17] == "TRIGGER":
                            a = a + 1
                else:
                    break


            if a > 0:
                line.append("TRIGGER_CLOSE")

            else:
                line.append("O")



        elif len(line) > 1 and "I-EVENT" in line[7]:

            for i in range (j-1, j-100, -1):
                if len(lemmasplit[i])<7:
                    break
                elif "B-EVENT" in lemmasplit[i][7]:
                    line.append(lemmasplit[i][18])
                    break

        elif len(line) > 1:
            line.append("O")

#qui si calcola la distanza dal trigger_close
def add_distance_trigger(lemmasplit):

    for j in range (0, len(lemmasplit)):
        line = lemmasplit[j]
        a = 0 
        if len(line) > 1:            
            for i in range (j-1, j-5, -1):                        
                if len(lemmasplit[i])>1 and lemmasplit [i][17] == "TRIGGER":
                    a=a+1
                    b = i 
                    break
            if a>0 and "B-EVENT" in line[7]:
                line.append(j-b)

            elif "I-EVENT" in line[7]:
                for i in range (j-1, j-100, -1):
                    if len(lemmasplit[i]) < 7:
                        break
                    elif "B-EVENT" in lemmasplit[i][7]:
                        line.append(lemmasplit[i][20])
                        break

            else:
                line.append("O")

#questa feature servirebbe per gestire casi con piu trigger che danno effetto evento positivo
#andrebbe migliorata, al momento non so quanto sia utile
def add_multiple_triggers (lemmasplit):

    for j in range (0, len(lemmasplit)):
        a = 0
        line = lemmasplit[j]
        if len(line)>1:
            for i in range (j-1, j-5, -1):
                if len(lemmasplit[i])>1 and lemmasplit [i][17] == "TRIGGER":
                    a=a+1
            if a>0 and "B-EVENT" in line[7]:
                line.append(a)


            elif "I-EVENT" in line[7]:
                for i in range (j-1, j-100, -1):
                    if len(lemmasplit[i]) < 7:
                        break
                    elif "B-EVENT" in lemmasplit[i][7]:
                        line.append(lemmasplit[i][21])
                        break


            else:
                line.append("O")


def get_conditional_construction (lemmasplit):
    nbCol = -1
    sentences = []
    numSent = 0
    for i in range(0,len(lemmasplit)):
        line = lemmasplit[i]
        if len(line) > 1:
            if nbCol == -1:
                nbCol = len(line)
                sentences.append([i])
            if lemmasplit[i][col_token].lower() == "se" and lemmasplit[i][col_pos] == "C":
                if i > 0 and len(lemmasplit[i-1])>1 and lemmasplit[i-1][col_token].lower() != "anche" and lemmasplit[i-1][col_token].lower() != "come":
                    lemmasplit[i].append('SE')
            elif lemmasplit[i][col_token].lower() == "quando":
                for j in range(i+1,len(lemmasplit)):
                    if len(lemmasplit[j]) > 1:
                        if lemmasplit[j][col_token] == "che":
                            lemmasplit[i].append('QUANDO')
                            lemmasplit[j].append('CHE')
                            break
                    else:
                        break
            elif lemmasplit[i][col_token].lower() == "allora" or lemmasplit[i][col_token].lower() == "che":
                for j in range(i-1,0,-1):
                    if len(lemmasplit[j]) > 1:
                        if lemmasplit[j][col_token].lower() == "se" and lemmasplit[j][len(lemmasplit[j])-1] == "SE":
                            lemmasplit[i].append(lemmasplit[i][col_token].upper())
                            break
                    else:
                        break

                for j in range(i+1,len(lemmasplit)):
                    if len(lemmasplit[j]) > 1:
                        if lemmasplit[j][col_token].lower() == "se" and lemmasplit[j][len(lemmasplit[j])-1] == "SE":
                            lemmasplit[i].append(lemmasplit[i][col_token].upper())
                            break
                    else:
                        break
            elif ("+cond+" in line[col_morpho] or "+cong+" in line[col_morpho] or "+fut+" in line[col_morpho]) and line[col_chunk] == 'B-VX':
                mood = 'COND'
                if "+cong+" in line[col_morpho]:
                    mood = 'CONG'
                elif "+fut+" in line[col_morpho]:
                    mood = 'FUTURE'
                line.append(mood)
                for j in range(i+1,len(lemmasplit)):
                    if lemmasplit[j][col_chunk] == "I-VX":
                        lemmasplit[j].append(mood)
                    else:
                        break

        else:
            #save the id of the first token of a sentence and the last token
            sentences[numSent].append(i-1)
            sentences.append([i+1])
            numSent += 1

    # in the last col of line, if it's empty put 'O'
    for i in range(0,len(lemmasplit)):
        line = lemmasplit[i]
        if len(line) > 1:
            if len(line) == nbCol:
                line.append('O')

    

   #for each sentence
    for sent in sentences:
        quando = False
        che = False
        se = False
        se_end = False
        se_VX = False
        allora = False
        colon = False
        quando_end = False

        #for the last sentence there is no token id for the last token
        if len(sent) == 1:
            sent.append(len(lemmasplit))

        #for each token in the sentence
        for i in range(sent[0], sent[1]):
            if len(lemmasplit[i]) > 1 and len(lemmasplit[i])>=nbCol:
                if lemmasplit[i][nbCol] == "QUANDO":
                    quando = True
                    che = False
                elif lemmasplit[i][nbCol] == "CHE":
                    che = True
                elif lemmasplit[i][nbCol] == "ALLORA":
                    allora = True
                elif lemmasplit[i][nbCol] == "SE":
                    se = True
                    allora = False
                    if lemmasplit[i+1][col_token] == ":":
                        colon = True

                #quando ... che ...
                if quando and "VX" in lemmasplit[i][col_chunk]:
                    if ((quando_end and not "+infinito+" in lemmasplit[i][col_morpho]) or not quando_end) and  not che:
                        lemmasplit[i].append('COND_IF_CLAUSE')
                        quando_end = True
                    elif che:
                        lemmasplit[i].append('COND_MAIN_CLAUSE')
                        
                elif quando_end and "XP" in lemmasplit[i][col_pos]:
                    quando = False

                #se ...
                if se and not se_end and "VX" in lemmasplit[i][col_chunk]:
                    if allora and len(lemmasplit[i]) == nbCol+1:
                        lemmasplit[i].append('COND_MAIN_CLAUSE')
                    else:
                        if lemmasplit[i][nbCol] == "CONG" and len(lemmasplit[i]) == nbCol+1:
                            lemmasplit[i].append('COND_IF_CLAUSE')
                        elif lemmasplit[i][nbCol] == "FUTURE" and len(lemmasplit[i]) == nbCol+1:
                            lemmasplit[i].append('COND_IF_CLAUSE')
                        elif lemmasplit[i][nbCol] == "O" and "+indic+" in lemmasplit[i][col_morpho] and len(lemmasplit[i]) == nbCol+1:
                            lemmasplit[i].append('COND_IF_CLAUSE')
                        se_VX = True
                elif se and not colon and se_VX:
                    se_end = True

        #CHE ... SE ...
        if che and not quando and se and not allora:
            for i in range(sent[0], sent[1]):
                if lemmasplit[i][nbCol] == "SE":
                    break
                if lemmasplit[i][nbCol] == "CHE":
                    che = False
                if not che and "VX" in lemmasplit[i][col_chunk]:
                    if lemmasplit[i][nbCol] == "COND" and len(lemmasplit[i]) == nbCol+1:
                        lemmasplit[i].append('COND_MAIN_CLAUSE')
                    elif lemmasplit[i][nbCol] == "FUTURE" and len(lemmasplit[i]) == nbCol+1:
                        lemmasplit[i].append('COND_MAIN_CLAUSE')
                    elif lemmasplit[i][nbCol] == "O" and "+indic+" in lemmasplit[i][col_morpho] and len(lemmasplit[i]) == nbCol+1:
                        lemmasplit[i].append('COND_MAIN_CLAUSE')
                    else:
                        break

        # ... SE ...  or SE ... ... or ... SE: ...
        if se and not allora and not che:
            se_end = False
            se = False
            for i in range(sent[1]-1, sent[0],-1):
                if lemmasplit[i][nbCol] == "SE":
                    se = True
                if  se and "VX" in lemmasplit[i][col_chunk]:
                    if lemmasplit[i][nbCol] == "COND" and len(lemmasplit[i]) == nbCol+1:
                        lemmasplit[i].append('COND_MAIN_CLAUSE')
                    elif lemmasplit[i][nbCol] == "FUTURE" and len(lemmasplit[i]) == nbCol+1:
                        lemmasplit[i].append('COND_MAIN_CLAUSE')
                    elif lemmasplit[i][nbCol] == "O" and "+indic+" in lemmasplit[i][col_morpho]:
                    #elif len(lemmasplit[i]) == nbCol+1:
                        lemmasplit[i].append('COND_MAIN_CLAUSE')
                    se_end = True
                elif se_end:
                    break

    # in the last col of line, if it's empty put 'O'
    for i in range(0,len(lemmasplit)):
        line = lemmasplit[i]
        if len(line) > 1:
            if len(line) == nbCol+1:
                line.append('O')



def add_last_col (lemmasplit):
    for line in lemmasplit:
        if len(line)>1:
            line.append("O")





input_and_evaluate()




