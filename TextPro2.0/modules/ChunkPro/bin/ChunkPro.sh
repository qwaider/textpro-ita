#! /bin/tcsh -f

### ChunkPro   ###
### V2.0, 2010 ###

### ChunkPro Version ###
set VERSION = "2.0, 2010"

### Do not modify ###
set CHUNKPRO_DIR = $TEXTPRO/ChunkPro
set YAMCHA_DIR = $TEXTPRO/tools/yamcha-0.33
set MODEL_NAME = "" #the selected model name
set PKE_ITA_MODEL = "pke_ita_model" #pke ITA model
set PKI_ITA_MODEL = "pki_ita_model" #pki ITA model
set PKE_ENG_MODEL = "pke_eng_model" #pke ENG model
set PKI_ENG_MODEL = "pki_eng_model" #pki ENG model

### Initialize Parameters ###
set outFileName = "" #use this FILE as output file
set inFileName = "" #use this FILE as input file
set mode = "1" #mode [1|2]
set language = "ENG" #language [ENG|ITA]

### Read Parameters ###
while ( {$1} != {} )

    if ( {$1} == "-h" || {$1} == "--help" ) then
        echo "ChunkPro"
        echo "Copyright (C) 2010 All rights reserved."
        echo "Description: ChunkPro is based on YamCha (http://chasen.org/~taku/software/yamcha/)"
        echo ""
        echo "Usage: ChunkPro [options] file"
        echo "  -h, --help                        show this help and exit"
        echo "  -v, --version                     show the version and exit"
        echo "  -l, --language=LANGUAGE           language [ENG|ITA] (default "$language")"
        echo "  -m, --mode=MODE                   mode 2 for the best accuracy (default "$mode")"
        echo "  -o, --output=FILE                 use the FILE as the standard output (default "stdout")"
        echo ""
        exit 0
    else if ( {$1} == "-v" || {$1} == "--version" ) then
        echo "ChunkPro Version: "$VERSION
        exit 0
    else if ( {$1} == "-m" || {$1} == "--mode" ) then
        shift
        set mode = $1
    else if ( {$1} == "-l" || {$1} == "--language" ) then
        shift
        set language = $1
    else if ( {$1} == "-o" || {$1} == "--output" ) then
        shift
        set outFileName = $1
    else if ( $# == 1 ) then
        set inFileName = $1
    else
        $CHUNKPRO_DIR/bin/ChunkPro.sh -h
        exit 0
    endif

    shift

end

### Check Parameters ###
if ( $inFileName != "" ) then
    if ( ! -f $inFileName ) then
        echo "Error: No such file or directory!"
        exit 1
    endif
endif

if ( $mode != "1" && $mode != "2" ) then
    echo "Error: Unrecognized mode $mode!"
    exit 1
endif
if ( $language != "ENG" && $language != "ITA" ) then
    echo "Error: Unrecognized language option "$language"!"
    exit 1
endif


### Set Variables ###
if ( $mode == "1" && $language == "ITA" ) then
    set MODEL_NAME = `ls -tlf1 $CHUNKPRO_DIR/models/$PKE_ITA_MODEL* | tail -1`
else if ( $mode == "2" && $language == "ITA" ) then
    set MODEL_NAME = `ls -tlf1 $CHUNKPRO_DIR/models/$PKI_ITA_MODEL* | tail -1`
else if ( $mode == "1" && $language == "ENG" ) then
    set MODEL_NAME = `ls -tlf1 $CHUNKPRO_DIR/models/$PKE_ENG_MODEL* | tail -1`
else if ( $mode == "2" && $language == "ENG" ) then
    set MODEL_NAME = `ls -tlf1 $CHUNKPRO_DIR/models/$PKI_ENG_MODEL* | tail -1`
endif
if ( $MODEL_NAME == "") then
    echo "No model available!"
    exit 1
else
    #echo "model $MODEL_NAME is being used"
endif
if ( $outFileName != "" ) then
    set outFileName = "--output "$outFileName
endif


### Run the System ###
if ( $inFileName != "" ) then
    echo "$YAMCHA_DIR/usr/local/bin/yamcha -m $MODEL_NAME $outFileName $inFileName"
    $YAMCHA_DIR/usr/local/bin/yamcha -m $MODEL_NAME $outFileName $inFileName
else
    cat $_ | $YAMCHA_DIR/usr/local/bin/yamcha -m $MODEL_NAME $outFileName
endif
