package eu.fbk.newsreader.italian.eventDetection;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.tartarus.snowball.ext.italianStemmer;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * User: minard@fbk.eu
 * Date: Aug 2014
 * Description: prepare files for training models for Event Dectection or for applying models on a new file.
 * Lang: Italian 
 * Usage: java -cp lib/libstemmer_java/java/ eu.fbk.newsreader.italian.eventDetection.PreProcessLearning_v3 file_txp file_out tense+stem+posSent+freq+derivatario+wnDomain+depHRel+root+sufpref+headWord+event_id+mainVb EVENT_eval
 */

public class PreProcessLearning_v3 {
	
	private static HashMap<String,Integer> idCol = new HashMap<String,Integer>();
	private static HashMap<String,Integer> newIdCol = new HashMap<String,Integer>();
	
	private static String localPath = getTextProPath();
	
	static String frequency_file = localPath+"/resources/list_event_freq.txt";

	static String fileDerivatario = localPath+"/resources/derivatario.csv";
	
	static String fileWNDomainIta = localPath+"/resources/wn_info_domain.csv";
	
	static String listColOut = localPath+"/resources/list_id_col";
	
	static int colChunk;
	static int colPOS;
	static int colMorph;
	static int colFullMorph;
	static int colLemma;
	static int colTimex;
	static int colTenseVP;
	static int colHeadWord;
	static int nbCol;
	static int lastColFix;
	static int lastCol;
	static int colTokenId;
	static int colHeadId;
	static int colDepRel;
	static int colParserId;
	
	static HashMap<Integer,eventStructure> listEvent = new HashMap<Integer,eventStructure> ();
	static HashMap<Integer,timexStructure> listTimex = new HashMap<Integer,timexStructure> ();
	static HashMap<String,timexStructure> listTimexEmpty = new HashMap<String,timexStructure> ();
	static HashMap<String,List<linkStructure>> listTlink = new HashMap<String,List<linkStructure>> ();
	
	static HashMap<String,List<String>> listDeriv = new HashMap<String,List<String>> ();
	static HashMap<String,List<String>> listWNDomain = new HashMap<String,List<String>> ();
	static HashMap<String,Double> listFreq = new HashMap<String,Double> ();
	
	static italianStemmer stemmer = new italianStemmer();

	static timexStructure dct;
	
	/**
	 * Get the path to TextPro
	 * @return
	 */
	public static String getTextProPath (){
        String temp = PreProcessLearning_v3.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        File textprotmp = new File(temp);
        if (textprotmp.isDirectory()) {
            textprotmp = textprotmp.getParentFile();
        } else {
            textprotmp = textprotmp.getParentFile().getParentFile();
        }

        if (!textprotmp.exists() || !textprotmp.isDirectory()) {
            System.err.println("Couldn't initialize TextPro path!\nMake sure that textproX.X.jar is inside lib directory!");
            System.exit(0);
        }
        String canonicalpath = "";
        try {
			canonicalpath = textprotmp.getCanonicalPath()+File.separator;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return canonicalpath;
    }
	
	
	/**
	 * Initialize the idCol hash: key --> field name, value --> field id
	 * @param listColNew
	 * @param listColOrig
	 */
	private static void init (String [] listColNew, String [] listColOrig, String objectLearning) {
		if (objectLearning.equals("TLINK")){
			setIdCol (listColOut+"_tlink"+"_+fact");
		}
		else if(objectLearning.contains("EVENT")){
			setIdCol (listColOut+"_event");
		}
		
		int idColInt = 0;
		for(int i=0; i<listColOrig.length; i++){
			idCol.put(listColOrig[i], idColInt);
			idColInt++;
		}
		for(int i=0; i<listColNew.length; i++){
			if(listColNew[i].equals("eventid") && ! idCol.containsKey("eventid")){
				idCol.put("eventid", idColInt);
				idCol.put("eventclass", idColInt+1);
				idColInt+=2;
			}
			else if(listColNew[i].equals("tmx") && ! idCol.containsKey("tmx")){
				idCol.put("tmxid", idColInt);
				idCol.put("tmx", idColInt+1);
				idCol.put("tmxvalue", idColInt+2);
				idColInt+=3;
			}
			else if(listColNew[i].equals("derivatario")){
				idCol.put("derivatario", idColInt);
				idCol.put("deriv_pos", idColInt+1);
				idColInt+=2;
			}
			else if(listColNew[i].equals("SRL")){
				idCol.put("role1", idColInt);
				idCol.put("role2", idColInt+1);
				idCol.put("role3", idColInt+2);
				idCol.put("is_arg_pred", idColInt+3);
				idCol.put("has_semrole", idColInt+4);
				idColInt+=5;
			}
			else{
				idCol.put(listColNew[i], idColInt);
				idColInt ++;
			}
		}
		
		//lastColFix = idCol.get("rule"); //less 1 for timexType, less 1 for morph_pos and less 1 for idFirstCol
		nbCol = idColInt;
		if(idCol.containsKey("tmxid")){
			colTimex = idCol.get("tmxid");
		}
		colChunk = idCol.get("chunk");
		colPOS = idCol.get("pos");
		colMorph = idCol.get("comp_morpho");
		colLemma = idCol.get("lemma");

		if(idCol.containsKey("tense")){
			colTenseVP = idCol.get("tense");
		}
		if(idCol.containsKey("headWord")){
			colHeadWord = idCol.get("headWord");
		}
		colTokenId = idCol.get("tokenid");
		colFullMorph = idCol.get("full_morpho");
		
		colParserId = idCol.get("parserid");
		colHeadId = idCol.get("head");
		colDepRel = idCol.get("deprel");
		
		
		listDeriv = readDerivatario(fileDerivatario);
		listWNDomain = readWNDomain(fileWNDomainIta);
		listFreq = readFileFrequency(frequency_file);
	}
	
	
	/**
	 * Read the file containing the list of fields (columns) that should contain the output and return a hash:
	 * Return a hash: key --> name field, value -> id field
	 * @param fileName
	 * @return
	 */
	public static HashMap<String, Integer> readFileStringId (String fileName){
		HashMap<String, Integer> hash = new HashMap<String, Integer>();
		try{
			InputStream ips=new FileInputStream(fileName); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String line;
			
			while ((line=br.readLine())!=null){
				if(!line.matches("^#.*") && line.contains("\t")){
					String [] idNameFeat = line.split("\t");
					hash.put(idNameFeat[0],Integer.parseInt(idNameFeat[1]));
				}
			}
			br.close(); 
		}		
		catch (Exception e){
			System.out.println(e.toString());
		}
		return hash;
	}
	
	
	/**
	 * Read the file containing the frequency of events in the training corpus
	 * Return a hash: key --> event (string), value --> frequency (double)
	 * @param fileName
	 * @return
	 */
	public static HashMap<String, Double> readFileFrequency (String fileName){
		HashMap<String, Double> hash = new HashMap<String, Double>();
		try{
			InputStream ips=new FileInputStream(fileName); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String line;
			
			while ((line=br.readLine())!=null){
				if(!line.matches("^#.*") && line.contains("\t")){
					String [] idNameFeat = line.split("\t");
					if(Integer.parseInt(idNameFeat[1]) > 2){
						hash.put(idNameFeat[0].substring(0,idNameFeat[0].length()-1),Double.parseDouble(idNameFeat[2]));
					}
				}
			}
			br.close(); 
		}		
		catch (Exception e){
			System.out.println(e.toString());
		}
		return hash;
	}
	
	/**
	 * Fill an hash with the col number of each column
	 * @param fileIdCol
	 */
	public static void setIdCol (String fileIdCol){
		newIdCol.putAll(readFileStringId(fileIdCol));
	}
	
	
	/**
	 * Add a feature related to the frequency of appearance of events in the training corpus using the hash produced by the function readFileFrequency() 
	 * @param lines
	 * @param listFreq
	 * @param col
	 * @return
	 */
	private static String [][] addFreqCol (String[][] lines, HashMap<String,Double> listFreq, int col){
		for (int i=0; i<lines.length; i++){
			if (lines[i][colLemma] != null && listFreq.containsKey(lines[i][colLemma])){
				if (listFreq.get(lines[i][colLemma]) == 1){
					lines[i][col] = "yes";
				}
				else if (listFreq.get(lines[i][colLemma]) == 0){
					lines[i][col] = "no";
				}
				else if (listFreq.get(lines[i][colLemma]) > 0.5){
					lines[i][col] = "yes";
				}
				else if (listFreq.get(lines[i][colLemma]) <= 0.5){
					lines[i][col] = "yes_no";
				}
			}
			else if(lines[i][1] != null && listFreq.containsKey(lines[i][1])){
				if (listFreq.get(lines[i][1]) == 1){
					lines[i][col] = "yes";
				}
				else if (listFreq.get(lines[i][1]) == 0){
					lines[i][col] = "no";
				}
				else if (listFreq.get(lines[i][1]) > 0.5){
					lines[i][col] = "yes";
				}
				else if (listFreq.get(lines[i][1]) <= 0.5){
					lines[i][col] = "yes_no";
				}
			}
			else if (lines[i][colLemma] != null){
				lines[i][col] = "0";
			}
		}
		
		return lines;
	}
	
	/**
	 * Add a column containing the id of the sentence
	 * @param lines
	 * @param col
	 * @return
	 */
	private static String [][] addSentId (String [][] lines, int col){
		int cptSent = 0;
		boolean prevEmpty = true;
		for(int i=0; i<lines.length; i++){
			if((lines[i][0] == null || lines[i][0].equals("")) && i>1 && prevEmpty){
				cptSent++;
				prevEmpty = false;
			}
			else if(lines[i][0] != null && !lines[i][0].equals("")){
				prevEmpty = true;
				lines[i][col] = Integer.toString(cptSent);
			}
		}
		return lines;
	}
	
	
	/**
	 * add a feature if a token is the head word of a NP or VP
	 * @param lines
	 * @return 
	 */
	private static String [][] headWord (String [][] lines, int col){
		boolean inNP = false;
		boolean inVP = false;
		int firstIdNP = -1;
		int lastIdNP = -1;

		int firstIdVP = -1;
		int lastIdVP = -1;
		for (int i=0; i<lines.length; i++){
			/* NP */
			if(inNP && (colChunk < 0 || lines[i][colChunk] == null || !lines[i][colChunk].equals("I-NP"))){
				inNP = false;
				
				if(lastIdNP == -1){
					// one word in NP
					if(lines[firstIdNP][colPOS].matches("^S.*")){
						lines[firstIdNP][col] = "HWNP";
					}
					else{
						lines[firstIdNP][col] = "WNP";
					}
				}
				else{
					int nbNN = 0;
					int idNN = -1;
					int nbNNP = 0;
					//count the number of NN and NNP
					for(int j=firstIdNP; j<lastIdNP+1; j++){
						lines[j][col] = "WNP";
						if(lines[j][colPOS].matches("^SP.*")){
							nbNNP++;
						}
						else if(lines[j][colPOS].matches("^S.*")){
							nbNN++;
							idNN=j;
						}
					}
					
					if(colMorph > 0 && lines[lastIdNP][colMorph].contains("gerund")){
						lines[lastIdNP][col] = "HWNP";
					}
					//if only NN, the last one is the headword
					else if(nbNN >= 1 && nbNNP == 0){
						lines[idNN][col] = "HWNP";
					}
					//else if there are also some NNP, all the NNP are headwords
					else if(nbNNP >= 1){
						for(int j=firstIdNP; j<lastIdNP+1; j++){
							if(lines[j][colPOS].matches("^SP.*")){
								lines[j][col] = "HWNP";
							}
						}
					}
				}
				lastIdNP = -1;
				firstIdNP = -1;
			}
			
			//firstId = id of the first token of the NP and lastId the id of the last one
			if(colChunk > 0 && lines[i][colChunk] != null && lines[i][colChunk].contains("NP")){
				if(lines[i][colChunk].equals("B-NP")){
					firstIdNP = i;
					inNP = true;
				}
				else if(lines[i][colChunk].equals("I-NP")){
					lastIdNP = i;
				}
			}
			
			/* VP */
			if(inVP && (colChunk < 0 || lines[i][colChunk] == null || !lines[i][colChunk].equals("I-VX"))){
				inVP = false;
				
				if(lastIdVP == -1){
					// one verb in VP
					lines[firstIdVP][col] = "HWVP";
				}
				else{
					int indPrevTo = -1;
					for(int j=firstIdVP; j<lastIdVP; j++){
						if(lines[i][colMorph] != null && colMorph > 0 && lines[i][colMorph].contains("gerund")){
							lines[j][col] = "HWVP";
						}
						else{
							lines[j][col] = "WVP";
						}
						if(lines[j][colLemma].equals("a") && j>firstIdVP){
							indPrevTo = j-1;
						}
					}
					if(indPrevTo > -1){
						lines[indPrevTo][col] = "HWVP";
					}
					lines[lastIdVP][col] = "HWVP";
				}
				lastIdVP = -1;
				firstIdVP = -1;
			}
			
			//firstId = id of the first token of the VP and lastId the id of the last one
			if(colChunk > 0 && lines[i][colChunk] != null && lines[i][colChunk].contains("VX")){
				if(lines[i][colChunk].equals("B-VX")){
					firstIdVP = i;
					inVP = true;
				}
				else if(lines[i][colChunk].equals("I-VX")){
					lastIdVP = i;
				}
			}
		}
		return lines;
	}
	
	
	
	/**
	 * read the file containing the list of nouns associated to the event class in WordNet
	 * @param name of the file
	 * @return list of the nouns
	 */
	public static List<String> readWNfile (String WNFileName){
		List<String> listNoun = new ArrayList<String>();
		
		try{
			InputStream ips=new FileInputStream(WNFileName); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String line;
			
			while ((line=br.readLine())!=null){
				if(!line.equals("")){
					listNoun.add(line);
				}
			}
			br.close(); 
		}		
		catch (Exception e){
			System.out.println(e.toString());
		}
		
		return listNoun;
	}
	
	
	
	/**
	 * read the file containing the list of nouns associated to the event class in WordNet
	 * @param name of the file
	 * @return list of the nouns
	 */
	public static List<String> readNominalizationfile (String NMZFileName){
		List<String> listNoun = new ArrayList<String>();
		
		try{
			InputStream ips=new FileInputStream(NMZFileName); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String line;
			
			while ((line=br.readLine())!=null){
				if(!line.equals("")){
					listNoun.add(line);
				}
			}
			br.close(); 
		}		
		catch (Exception e){
			System.out.println(e.toString());
		}
		
		return listNoun;
	}
	
	
	/**
	 * fill the hash list_event and list_timex (key --> id, value --> eventStructure or timexStructure)
	 * @param lines
	 */
	public static void readTXPevtx (String[][] lines){
		
		for (int i=0; i<lines.length; i++){
			if(lines[i].length > 2 && lines[i][1] != null && idCol.containsKey("event") 
					&& lines[i].length >= idCol.get("event") && lines[i][idCol.get("event")].startsWith("B-")){
				eventStructure tmp = new eventStructure ();
		
				tmp.classEvent = lines[i][idCol.get("eventid")+2];
				tmp.eventID = lines[i][idCol.get("eventid")];
			
				ArrayList<String> list_tok_id = new ArrayList<String> ();
				list_tok_id.add(lines[i][1]);
				
				for (int k=i+1; k<lines.length; k++){
					if(lines[k][1] != null && lines[k][idCol.get("event")].startsWith("I-")){
						list_tok_id.add(lines[k][1]);
					}
					else{
						break;
					}
				}
				
				String[] list_tok = new String[list_tok_id.size()];
				int l = 0;
				for(String tokid : list_tok_id){
					list_tok[l] = tokid;
				}
				
				tmp.instance = list_tok;

				listEvent.put(Integer.parseInt(list_tok[0]), tmp);
		
			}
			else if (lines[i].length > 2 && lines[i][1] != null && lines[i][idCol.get("tmx")].startsWith("B-")){
				
				timexStructure tmp = new timexStructure ();
		
				tmp.timexID = lines[i][idCol.get("tmxid")];
		
				tmp.typeTimex = lines[i][idCol.get("tmx")].substring(2);
		
				tmp.value = lines[i][idCol.get("tmxvalue")];
				
				if(tmp.value==""){
					tmp.value = "XXXX-XX-XX";
				}
		
				ArrayList<String> list_tok_id = new ArrayList<String> ();
				list_tok_id.add(lines[i][1]);
				
				for (int k=i+1; k<lines.length; k++){
					if(lines[k][1] != null && lines[k][idCol.get("tmx")].startsWith("I-")){
						list_tok_id.add(lines[k][1]);
					}
					else{
						break;
					}
				}
				
				String[] list_tok = new String[list_tok_id.size()];
				int l = 0;
				for(String tokid : list_tok_id){
					list_tok[l] = tokid;
				}
				
				tmp.instance = list_tok;
		
				listTimex.put(Integer.parseInt(list_tok[0]), tmp);
			}
		}
		
	}
	
	
	/**
	 * read the timeML file ref and put information about event and timex in two hash: listEvent and listTimex
	 * @param file name
	 */
	public static void readCATRef (String refFileName){
		List<String> idEvent = new ArrayList<String> ();
		List<String> idTimex = new ArrayList<String> ();
 		try{
			File fileCAT = new File(refFileName);
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fileCAT);
			
			ArrayList<String> list_token_empty = new ArrayList<String> ();
			
			NodeList nTokenList = doc.getElementsByTagName("token");
			
			for (int temp = 0; temp <nTokenList.getLength(); temp++){
				
				Node tokNode = nTokenList.item(temp);
				
				if (tokNode.getNodeType() == Node.ELEMENT_NODE){
					if (tokNode.getTextContent().equals("")){
						Element tok = (Element) tokNode;
						list_token_empty.add(tok.getAttribute("t_id").toString());
					}
				}
				
			}
			
			
			/* EVENT */
			NodeList nEventList = doc.getElementsByTagName("EVENT");
			for (int temp = 0; temp < nEventList.getLength(); temp++) {
				 
				Node eventNode = nEventList.item(temp);
		 
				if (eventNode.getNodeType() == Node.ELEMENT_NODE) {
		 
					Element eElement = (Element) eventNode;
					
					eventStructure tmp = new eventStructure ();
					
					tmp.classEvent = eElement.getAttribute("class");
					tmp.eventID = eElement.getAttribute("m_id");
					idEvent.add(tmp.eventID);
					
					NodeList tokAnchorList = eElement.getElementsByTagName("token_anchor");
					String [] list_tok = new String [tokAnchorList.getLength()];

						for (int it = 0; it < tokAnchorList.getLength(); it++){
							Node tokNode = tokAnchorList.item(it);
							String idTok = (((Element) tokNode).getAttribute("t_id"));
							if (tokNode.getNodeType() == Node.ELEMENT_NODE){
								int nbDiff = 0;
								if (list_token_empty.size()>0 && Integer.parseInt(idTok) > Integer.parseInt(list_token_empty.get(0))){
									for (int j=0; j<list_token_empty.size(); j++){
										if (Integer.parseInt(idTok) > Integer.parseInt(list_token_empty.get(j))){
											nbDiff ++;
										}
									}
								}
								
								list_tok[it] = Integer.toString(Integer.parseInt(idTok)-nbDiff);
							}
						}
						tmp.instance = list_tok;

						listEvent.put(Integer.parseInt(list_tok[0]), tmp);
				}
			}
		

			/* TIMEX3 */
			NodeList nTimexList = doc.getElementsByTagName("TIMEX3");
			
			for (int temp = 0; temp < nTimexList.getLength(); temp++) {
				 
				Node timexNode = nTimexList.item(temp);
		 
				if (timexNode.getNodeType() == Node.ELEMENT_NODE) {
		 
					Element eElement = (Element) timexNode;
					
					timexStructure tmp = new timexStructure ();
					
					tmp.timexID = eElement.getAttribute("m_id");
					idTimex.add(tmp.timexID);
					
					tmp.typeTimex = eElement.getAttribute("type");
					tmp.value = eElement.getAttribute("value");
					if(tmp.value==""){
						tmp.value = "XXXX-XX-XX";
					}
					NodeList tokAnchorList = eElement.getElementsByTagName("token_anchor");
					
					if(tokAnchorList.getLength() > 0){
						String [] list_tok = new String [tokAnchorList.getLength()];
						for (int it = 0; it < tokAnchorList.getLength(); it++){
							Node tokNode = tokAnchorList.item(it);
							String idTok = (((Element) tokNode).getAttribute("t_id"));
							if (tokNode.getNodeType() == Node.ELEMENT_NODE){
								int nbDiff = 0;
								if (list_token_empty.size()>0 && Integer.parseInt(idTok) > Integer.parseInt(list_token_empty.get(0))){
									for (int j=0; j<list_token_empty.size(); j++){
										if (Integer.parseInt(idTok) > Integer.parseInt(list_token_empty.get(j))){
											nbDiff ++;
										}
									}
								}
								
								list_tok[it] = Integer.toString(Integer.parseInt(idTok)-nbDiff);
							}
						}
						
						tmp.instance = list_tok;
						
						listTimex.put(Integer.parseInt(list_tok[0]), tmp);
					}
					else{
						String [] list_tok = new String [2];
						if(!eElement.getAttribute("beginPoint").equals("") && !eElement.getAttribute("endPoint").equals("")){
							list_tok[0] = eElement.getAttribute("beginPoint");
							list_tok[1] = eElement.getAttribute("endPoint");
						}
						else if(!eElement.getAttribute("anchorTimeID").equals("")){
							list_tok[0] = eElement.getAttribute("anchorTimeID");
							list_tok[1] = "";
						}
						if(eElement.getAttribute("functionInDocument").equals("PUBLICATION_TIME")){
							dct = tmp;
						}
						else if(list_tok.length == 2 && list_tok[0] != null && list_tok[1] != null){
							tmp.instance = list_tok;
							if(!listTimexEmpty.containsKey(list_tok[0]+"-"+list_tok[1])){
								listTimexEmpty.put(list_tok[0]+"-"+list_tok[1],tmp);
							}
						}
					}

					
					
				}
			}
			
			/* TLINK */
			NodeList nTlinkList = doc.getElementsByTagName("TLINK");
			
			for (int temp = 0; temp < nTlinkList.getLength(); temp++) {
				 
				Node tlinkNode = nTlinkList.item(temp);
		 
				if (tlinkNode.getNodeType() == Node.ELEMENT_NODE) {
		 
					Element eElement = (Element) tlinkNode;
					
					linkStructure tmp = new linkStructure ();
					
					tmp.tlinkID = eElement.getAttribute("r_id");
					tmp.relType = eElement.getAttribute("relType");
					if(tmp.relType.equals("")){
						tmp.relType = "REL";
					}
					
					Node sourceNode = eElement.getElementsByTagName("source").item(0);
					Node targetNode = eElement.getElementsByTagName("target").item(0);
					
					tmp.target = ((Element) targetNode).getAttribute("m_id");
					tmp.source = ((Element) sourceNode).getAttribute("m_id");
				
					if(idEvent.contains(tmp.target)){
						tmp.target = "e"+tmp.target;
					}
					else if(idTimex.contains(tmp.target)){
						tmp.target = "tmx"+tmp.target;
						if(tmp.target.equals(dct.timexID)){
							tmp.target = "tmx0";
						}
					}
					
					if(idEvent.contains(tmp.source)){
						tmp.source = "e"+tmp.source;
					}
					else if(idTimex.contains(tmp.source)){
						tmp.source = "tmx"+tmp.source;
						if(tmp.source.equals(dct.timexID)){
							tmp.source = "tmx0";
						}
					}
					
					List<linkStructure> list_sce_rel = new ArrayList<linkStructure> ();
					if(listTlink.containsKey(tmp.source)){
						list_sce_rel = listTlink.get(tmp.source);
					}
								
					list_sce_rel.add(tmp);
					listTlink.put(tmp.source,list_sce_rel);
				}
			}
				
		}		
		catch (Exception e){
			System.out.println(e.toString());
		}
	}
	
	/**
	 * cat the elements of a collection into a string, each element is separated by a delimiter
	 * @param s the collection
	 * @param delimiter
	 * @return a string
	 */
	public static String join(Collection s, String delimiter) {
	    StringBuffer buffer = new StringBuffer();
	    Iterator iter = s.iterator();
	    while (iter.hasNext()) {
	        buffer.append(iter.next());
	        if (iter.hasNext()) {
	            buffer.append(delimiter);
	        }
	    }
	    return buffer.toString();
	}
	
	/**
	 * add the ref annotation of event (for the training corpus)
	 * @param lines
	 * @param refFileName file name of the timeML ref
	 * @return
	 */
	private static String [][] addEventRef (String [][] lines, int col){
		int colClass = idCol.get("eventclass");
		int colEv = idCol.get("event");
		int colId = idCol.get("eventid");
		
		for(int j=0; j<lines.length; j++){
			if (lines[j][colTokenId] != null && !lines[j][colTokenId].equals("O") 
					&& listEvent.containsKey(Integer.parseInt(lines[j][colTokenId]))){
				if(listEvent.get(Integer.parseInt(lines[j][colTokenId])).instance.length == 1){
					lines[j][colEv] = "EVENT";
					lines[j][colClass] = listEvent.get(Integer.parseInt(lines[j][colTokenId])).classEvent;
					lines[j][colId] = "e"+listEvent.get(Integer.parseInt(lines[j][colTokenId])).eventID;
				}
				else{
					int head = j;
					int head_noun = -1;
					int head_verb = -1;
					if(head_verb > -1){
						head = head_verb;
					}
					else if(head_noun > -1){
						head = head_noun;
					}
					
					lines[head][colEv] = "EVENT";
					lines[head][colClass] = listEvent.get(Integer.parseInt(lines[j][colTokenId])).classEvent;
					lines[head][colId] = "e"+listEvent.get(Integer.parseInt(lines[j][colTokenId])).eventID;
					
					for (int k=j; k<j+listEvent.get(Integer.parseInt(lines[j][colTokenId])).instance.length; k++){
						if(lines[k][colEv] == null){
							lines[k][colEv] = "O";
							lines[k][colClass] = "O";
							lines[k][colId] = "O";
						}
					}
				}
			}
			
			else {
				if(lines[j][colId] == null){
					lines[j][colId] = "O";
				}
				
			}
		}
		return lines;
	}
	
	/**
	 * add the ref annotation of timex (for the training corpus)
	 * @param lines
	 * @param refFileName file name of the timeML ref
	 * @return
	 */
	private static String [][] addTimexRef (String [][] lines, int col){
		
		for(int j=0; j<lines.length; j++){
			
			if (lines[j][colTokenId] != null && listTimex.containsKey(Integer.parseInt(lines[j][colTokenId]))){
				for (int k=j; k<j+listTimex.get(Integer.parseInt(lines[j][colTokenId])).instance.length; k++){
					lines[k][col] = "tmx"+listTimex.get(Integer.parseInt(lines[j][colTokenId])).timexID;
					if (k==j){
						lines[k][col+1] = "B-"+listTimex.get(Integer.parseInt(lines[j][colTokenId])).typeTimex;
					}
					else{
						lines[k][col+1] = "I-"+listTimex.get(Integer.parseInt(lines[j][colTokenId])).typeTimex;
					}
					lines[k][col+2] = listTimex.get(Integer.parseInt(lines[j][colTokenId])).value;
				}
			}
			
			else {
				if(lines[j][col+1] == null){
					lines[j][col] = "O";
					lines[j][col+1] = "O";
					lines[j][col+2] = "O";
					
				}
				
			}
		}
		return lines;
	}
	
	
	/**
	 * add the ref annotation of empty timex (for the training corpus)
	 * @param lines
	 * @param refFileName file name of the timeML ref
	 * @return
	 */
	private static String [][] addEmptyTimexRef (String [][] lines, int col){
		int i=0;
		int it=0;
		int nbColLines = 0;
		nbColLines = nbCol;
		
		String [][] emptyTmx = null;
		
		if(listTimexEmpty.size()>0){
		
			emptyTmx = new String [listTimexEmpty.size()+2][nbColLines];

		i=2;
		for(String id_ETX: listTimexEmpty.keySet()){
			emptyTmx[i][0] = "ETX_"+listTimexEmpty.get(id_ETX).instance[0];
			emptyTmx[i][col+1] = "B-"+listTimexEmpty.get(id_ETX).typeTimex;
			emptyTmx[i][col+2] = listTimexEmpty.get(id_ETX).value;
			emptyTmx[i][col] = "tmx"+listTimexEmpty.get(id_ETX).timexID;
			
			for(int j=0; j<emptyTmx[i].length; j++){
				if(emptyTmx[i][j] == null){
					emptyTmx[i][j] = "O";
				}
			}
			
			i++;
		}
		}
		else{
			emptyTmx = new String [2][nbColLines];

			i=2;
		}
		
		String [][] newLines = new String[lines.length+emptyTmx.length][nbColLines];
		for(int j=0; j<emptyTmx.length; j++){
			newLines[it] = emptyTmx[j];
			it++;
		}
		for(int j=0; j<lines.length; j++){
			newLines[it] = lines[j];
			it++;
		}
		return newLines;
		
	}
	
	/**
	 * Read the derivatario file and fill an hash (key --> lemma, value --> list of word from which it can derive
	 * @param fileName
	 * @return
	 */
	private static HashMap<String,List<String>> readDerivatario (String fileName){
		HashMap<String,List<String>> hash = new HashMap<String,List<String>> ();
		
		try{
			InputStream ips=new FileInputStream(fileName); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String line;
			
			while ((line=br.readLine())!=null){
				String [] elts = line.split(";");
				List<String> list_temp = new ArrayList<String> ();
				if(hash.containsKey(elts[1])){
					list_temp = hash.get(elts[1]);
				}
				list_temp.add(elts[2]);
				hash.put(elts[1],list_temp);
			}
		}
		catch (Exception e){
			System.out.println(e.toString());
		}
		
		return hash;
	}
	
	/**
	 * Add a feature indicating if a noun is derived from a verb
	 * @param lines
	 * @param list_derivatario
	 * @param col
	 * @return
	 */
	private static String[][] addDerivatario (String [][] lines, HashMap<String,List<String>> list_derivatario, int col){
		for(int j=0; j<lines.length; j++){
			if(lines[j][colLemma] != null){
				if(list_derivatario.containsKey(lines[j][colLemma].toUpperCase())){
					List<String> deriv = list_derivatario.get(lines[j][colLemma].toUpperCase());
					String feat = "";
					for(int k=0; k<deriv.size(); k++){
						if(deriv.get(k).contains(":root")){
							feat = deriv.get(k);
						}
					}
					if(feat == ""){
						feat = deriv.get(0);
					}
					
					if(feat.startsWith("BASELESS")){
						lines[j][col] = "O";
						lines[j][col+1] = "O";
					}
					else{
						String [] elt = feat.split(":");
						lines[j][col] = elt[0].toLowerCase();
						if(elt[0].toLowerCase().endsWith("are") || elt[0].toLowerCase().endsWith("ire") 
								|| elt[0].toLowerCase().endsWith("ere")){
							lines[j][col+1] = "v";
						}
					}
				}
				else{
					if(lines[j][col] == null){
						lines[j][col] = "O";
					}
					if(lines[j][col+1] == null){
						lines[j][col+1] = "O";
					}
				}
			}
		}
		
		return lines;
	}
	
	/**
	 * Read a file containing a list of words and their WN domain
	 * Return a hash: key --> word, value --> list of WN domain
	 * @param fileName
	 * @return
	 */
	private static HashMap<String,List<String>> readWNDomain (String fileName){
		HashMap<String,List<String>> hash = new HashMap<String,List<String>> ();
		try{
			InputStream ips=new FileInputStream(fileName); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String line;
			
			while ((line=br.readLine())!=null){
				String [] elts = line.split("\t");
				
				if(elts.length > 1){
				
					String [] word = elts[1].split(" ");
					for(int k=0; k<word.length; k++){
						
						String w = word[k];
							
						if(w.contains("_")){
							
						}
						else if(elts.length > 2){
							List<String> list_temp = new ArrayList<String> ();
							if(hash.containsKey(w.toLowerCase())){
								list_temp = hash.get(w.toLowerCase());
							}
							String [] dom = elts[2].split(" ");
							for (int j=0; j<dom.length; j++){
								if (! list_temp.contains(dom[j]) && !dom[j].equals("")){
									list_temp.add(dom[j]);
								}
							}
							hash.put(w.toLowerCase(),list_temp);
						}
					}
				}
			}
		}
		catch (Exception e){
			System.out.println(e.toString());
		}
		
		return hash;
	}
	
	/**
	 * Add the WN domain of a word
	 * @param lines
	 * @param list_wn_domain
	 * @param col
	 * @return
	 */
	private static String[][] addWNDomain (String [][] lines, HashMap<String,List<String>> list_wn_domain, int col){
		for(int j=0; j<lines.length; j++){
			if(lines[j][colLemma] != null){
				if(lines[j][colMorph].matches(".+\t.+")){
				String morph = lines[j][colMorph].split("\t")[1];
				if(morph.matches("(n)|(adj)|(v)|(adv)") && list_wn_domain.containsKey(lines[j][colLemma])){
					List<String> dom = list_wn_domain.get(lines[j][colLemma]);
					String feat = "";
					for(int k=0; k<dom.size(); k++){
						if(dom.get(k).matches("(Factotum)|(Time_Period)|(Person)|(Quality)|(Psychological_Features)")){
							feat = dom.get(k);
						}
					}
					if(feat == ""){
						feat = "O";
					}
					
					
					lines[j][col] = feat;
				}
				else{
					lines[j][col] = "O";
				}
				}
			}
		}
		
		return lines;
	}
	
	/**
	 * Add a feature indicating if the verb is the main verb of the sentence.
	 * @param lines
	 * @param col
	 * @return
	 */
	private static String [][] addRoot (String [][] lines, int col){
		for (int j=0; j<lines.length; j++){
			if(lines[j][colDepRel] != null){
				if(lines[j][colDepRel].equals("ROOT") && lines[j][colChunk].endsWith("VX")){
					lines[j][col] = "mainVb";
				}
				else{
					lines[j][col] = "O";
				}
			}
		}
		return lines;
	}
	
	
	/**
	 * Add features related to the dependency relations
	 * @param lines
	 * @param col
	 * @return
	 */
	private static String [][] addDepRel (String [][] lines, int col, String objectLearning){
		int fSent = 0;
		int lSent = 0;
		
		HashMap<String,String> idFileIdSent = new HashMap<String,String> ();
		HashMap<String,String> idTokSent = new HashMap<String,String> ();
		
		for (int j=0; j<=lines.length; j++){
			if(j == lines.length || lines[j][colHeadId] == null || lines[j].length < colHeadId){
				lSent = j;
				for (int k=fSent; k<lSent; k++){
					if(lines[k][colHeadId].equals("0")){
						lines[k][col] = "O";
					}
					else if (!lines[k][colDepRel].equals("O")){
						if(objectLearning.equals("TLINK")){
							lines[k][col] = idFileIdSent.get(lines[k][colHeadId])+":"+lines[k][colDepRel];
						}
						else if(objectLearning.contains("EVENT")){
							lines[k][col] = idTokSent.get(idFileIdSent.get(lines[k][colHeadId]));
						}
					}
				}
				idFileIdSent.clear();
				idTokSent.clear();
				fSent = j+1;
			}
			else{
				idTokSent.put(lines[j][colTokenId], lines[j][colLemma]);
				idFileIdSent.put(lines[j][colParserId], lines[j][colTokenId]);
			}
		}
		return lines;
	}
	
	
	/**
	 * Add a feature if the word is formed with one of the following prefix: zione, mento, tura and aggio.
	 * @param lines
	 * @param col
	 * @return
	 */
	private static String [][] addSuffixPrefix (String [][] lines, int col){
		for (int j=0; j<lines.length; j++){
			if(lines[j][colLemma] != null){
				String suf = "O";
				
				if(lines[j][colLemma].endsWith("zione")){
					suf = "zione";
				}
				else if(lines[j][colLemma].endsWith("mento")){
					suf = "mento";
				}
				else if(lines[j][colLemma].endsWith("tura")){
					suf = "tura";
				}
				else if(lines[j][colLemma].endsWith("aggio")){
					suf = "aggio";
				}
				
				lines[j][col] = suf;
			}
		}
		return lines;
	}
	
	
	/**
	 * add feature related to morphology analyses (simplified pos)
	 * @param lines
	 * @param colDeriv
	 * @return
	 */
	private static String [][] addPOSMorphAnalysis (String [][] lines, int colDeriv){
		for (int j=0; j<lines.length; j++){
			String feat = "";
			String deriv = "";
			if(lines[j][colFullMorph] != null){
				if(lines[j][colFullMorph].contains(" ")){
					String [] morph = lines[j][colFullMorph].split(" ");
					List<String> all_pos = new ArrayList<String> ();
					for (int k=0; k<morph.length; k++){
						String [] elt = morph[k].split("\\+");
						if(! all_pos.contains(elt[1])){
							all_pos.add(elt[1]);
						}
						if(elt[1].equals("v") && !elt[0].equals(lines[j][colLemma])){
							deriv = elt[0];
						}
					}
					
					if(all_pos.size() > 1){
						if(all_pos.contains("v")){
							feat = "v";
						}
						if(all_pos.contains("adj")){
							feat += "a";
						}
						if(all_pos.contains("n")){
							feat += "n";
						}
					}
					if(feat == ""){
						feat = "O";
					}
					lines[j][colFullMorph] = feat;
					if(colDeriv > 0 && !deriv.equals("")){
						lines[j][colDeriv] = deriv;
						lines[j][colDeriv+1] = "v";
					}
				}
				else{
					if(lines[j][colFullMorph].contains("+v+")){	
						if(colDeriv > 0){
							lines[j][colDeriv] = lines[j][colLemma];
							lines[j][colDeriv+1] = "v";
						}
					}
					lines[j][colFullMorph] = "O";
					
				}
			}
		}
		return lines;
	}
	
	
	/**
	 * get the number of sentences in a doc
	 * @param lines
	 * @return
	 */
	public static int getNbSent (String [][] lines){
		int nbLines = 0;
		boolean first = true;
		
		for(int i=0;i<lines.length;i++){
			if(lines[i][0] != null && (!lines[i][0].equals("")||first)){
        		if(lines[i][0].equals("")){
        			first=false;
        			nbLines++;
        		}
        		else{
        			first=true;
        		}
			}
		}
		
		return nbLines;
	}
	
	
	/**
	 * Modify the value of the lemma's column in order to have only one value
	 * @param lines
	 * @return
	 */
	private static String[][] modifyLemma (String [][] lines){
		/* if several lemma are given for the same words then selection of only one:
		 *  if one lemma is ended by a s, ignore it
		 *  and chose the first proposition
		 * */
		for(int i=0;i<lines.length;i++){
			if(lines[i][colLemma] != null && lines[i][colLemma].contains(" ")){
				String [] lemma = lines[i][colLemma].split(" ");
				for(String l : lemma){
					if(!l.matches(".*s$")){
						lines[i][colLemma] = l;
						break;
					}
				}
				if(lines[i][colLemma].contains(" ")){
					lines[i][colLemma] = lemma[0];
				}
			}
			
			//preposozioni articolate
			if(lines[i][0] != null && lines[i][colLemma] != null && lines[i][colMorph] != null &&
					lines[i][colLemma].contains("/") && lines[i][colMorph].contains("/")){
				if(lines[i][0].matches("(del|dell'|dello|della|dei|degli|delle|dell)")){
					lines[i][colLemma] = "del";
				}
				else if(lines[i][0].matches("(al|all'|allo|alla|ai|agli|alle|all)")){
					lines[i][colLemma] = "al";
				}
				else if(lines[i][0].matches("(dal|dall'|dallo|dalla|dai|dagli|dalle|dall)")){
					lines[i][colLemma] = "dal";
				}
				else if(lines[i][0].matches("(nel|nell'|nello|nella|nei|negli|nelle|nell)")){
					lines[i][colLemma] = "nel";
				}
				else if(lines[i][0].matches("(col|coll'|collo|colla|coi|cogli|colle|coll)")){
					lines[i][colLemma] = "col";
				}
			}
			
			else if(lines[i][0] != null && lines[i][colLemma] != null && lines[i][colLemma].equals("indet")){
				lines[i][colLemma] = "un";
			}
			else if (lines[i][0] != null && lines[i][colLemma] != null && lines[i][colLemma].equals("det")){
				lines[i][colLemma] = "il";
			}
			else if(lines[i][0] != null  && lines[i][colLemma] != null && lines[i][colLemma].equals("__NULL__")){
				lines[i][colLemma] = lines[i][0].toLowerCase();
			}
		}
		return lines;
	}
	
	
	/**
	 * Select one morphological analysis if several are given to the same token
	 * @param lines
	 * @return
	 */
	private static String [][] modifyMorph (String [][] lines){
		for(int i=0;i<lines.length;i++){
			/* if several morphology analyzes are given then select only one:
			 *  the first analyse that contains the same lemma as the one give by the lemmapro module
			 *  else the first analyse
			 * */
			if(colMorph > 0 && lines[i][colMorph] != null && lines[i][colMorph].contains(" ")){
				String [] morph = lines[i][colMorph].split(" ");
				for(String m : morph){
					if(m.matches("^"+lines[i][colLemma]+"\\+.*")){
						lines[i][colMorph] = m;
						break;
					}
				}
				if(lines[i][colMorph].contains(" ")){
					lines[i][colMorph] = morph[0];
				}
				
			}
			
			//preposizioni articolare
			if(lines[i][0] != null && lines[i][colMorph] != null && colMorph > 0 && lines[i][colMorph].contains("prep/")){
				//dalla	665	ES	da~da+prep/la~det+art+f+sing	da/det	O	O
				String [] infoMorpho = lines[i][colMorph].split("\\+");
				String posMorpho = "prep-det";
				String genderNum = infoMorpho[infoMorpho.length-2]+"+"+infoMorpho[infoMorpho.length-1];
				lines[i][colMorph] = genderNum+"\t"+posMorpho;
			}
			//verbi reflessivi
			else if(lines[i][0] != null && lines[i][colMorph] != null && colMorph > 0 && lines[i][colMorph].contains("/") && lines[i][colMorph].contains("~rifl")){
				//strapparsi	669	VF+E	strappare~strappare+v+infinito+pres+nil+nil+nil/si~rifl+pron+accdat+_+3+_	strappare/rifl	B-VX	O
				String [] infoVerbPron = lines[i][colMorph].split("/");
				String [] infoMorphoVerb = infoVerbPron[0].split("\\+");
				String posMorpho = "v-rifl";
				String otherInfo = join(infoMorphoVerb,2,infoMorphoVerb.length, '+');
				lines[i][colMorph] = otherInfo+"\t"+posMorpho;
			}
			else{
			
			
				/* split the morphology analyze into two features:
				 * 	the morph information
				 *  the POS
				 * */
				if(colMorph > 0 && lines[i][colMorph] != null){
					String [] eltMorph = lines[i][colMorph].split("\\+");
					if(eltMorph.length>2){
						lines[i][colMorph] = join(eltMorph,2,eltMorph.length,'+')+"\t"+eltMorph[1];
					}
					else if(eltMorph.length == 2){
						lines[i][colMorph] = "O"+"\t"+eltMorph[1];
					}
					else{
						lines[i][colMorph] = "O\tO";
					}
				}
			}
			
		}
		return lines;
	}

	
	/**
	 * Add the stem of each token
	 * @param lines
	 * @param col
	 * @return
	 */
	private static String[][] addStem (String [][] lines, int col){
		for(int i=0;i<lines.length;i++){
			/* add the stem of the word */
			if(lines[i][0] != null){
				stemmer.setCurrent(lines[i][0]);
				stemmer.stem();
				lines[i][col] = stemmer.getCurrent();
			}
			if(lines[i][col] == null || lines[i][col].matches("")){
				lines[i][col] = lines[i][1];
			}
			
		}
		return lines;
	}
	
	
	/**
	 * Add the position of the sentence in the document for each token.
	 * @param lines
	 * @param col
	 * @return
	 */
	private static String [][] addPosSentence(String [][] lines, int col){
		int idLine = 0;
		int nbLine = getNbSent (lines);
		boolean first = true;
		
		int half = nbLine/2;
		for(int i=0;i<lines.length;i++){
			
			if(lines[i][0] != null && (!lines[i][0].equals("")||first)){
        		if(lines[i][0].equals("")){
        			first=false;
        			idLine++;
        		}
        		else{
        			first=true;
        		}
			}

		
			/* feature representing the position of the sentence in the doc 
			 * f: first sentence
			 * mB: sentences in the first half
			 * m: the middle sentence
			 * mA: sentences in the second half
			 * l: last sentence
			 * */
			if(lines[i][1] != null){
				if(idLine == 1){
					lines[i][col] = "f";
				}
				else if(idLine == nbLine-1){
					lines[i][col] = "l";
				}
				else if(idLine == half){
					lines[i][col] = "m";
				}
				else if(idLine < half){
					lines[i][col] = "mB";
				}
				else{
					lines[i][col] = "mA";
				}
			}
		}
		return lines;
	}


	/**
	 * cat the elements of an array into a string from the b index to the e index, each element is separated by a character
	 * @param tab the array
	 * @param b the begin index
	 * @param e the end index
	 * @param s the delimiter
	 * @return
	 */
	public static String join (String [] tab, int b, int e, char s){
		String content = "";
		for(int i=b; i<e; i++){
			content += tab[i]+s;
		}
		content = content.substring(0, content.length()-1);
		return content;
	}
	
	/**
	 * Sort the col according to a predefined order
	 * @param lines
	 * @param newIdCol
	 * @return
	 */
	public static String[][] sortCol (String [][] lines){
		int max = 0;
		for(String nameCol : newIdCol.keySet()){
			if(newIdCol.get(nameCol) > max){
				max = newIdCol.get(nameCol);
			}
		}
		String [][] linesSorted = new String[lines.length][max+1];
		for(int i=0; i<lines.length; i++){
			if(lines[i][1] != null){
				for(String nameCol : newIdCol.keySet()){
					if(idCol.containsKey(nameCol)){
						if(newIdCol.containsKey(nameCol)){
							linesSorted[i][newIdCol.get(nameCol)] = lines[i][idCol.get(nameCol)];
						}
					}
				}
			}
			else{
				linesSorted[i] = lines[i];
			}
		}
		return linesSorted;
	}
	
	/**
	 * @param args
	 * @throws StemmerException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws JAXBException 
	 */
	public static void main(String[] args) throws JAXBException, ParserConfigurationException, SAXException, IOException {
		
		String fileIn = args[0];
		File file = new File (fileIn);
		
		String repOut = args[1];
		
		String listColString = args[2];
		String [] listCol = listColString.split("\\+");
		
		String objectLearning = args[3]; //EVENT_eval or TLINK or EVENT
			
		String head = "";
			
		String fileNameOut = repOut;
			
		String fileNameToRead = file.toString();
			
			
		String listColOrigString = TextProFileFormat.getHeadFileTextPro(fileNameToRead);

		timexStructure tmp = new timexStructure ();
		String valueDCT = TextProFileFormat.getDate(fileNameToRead);
		tmp.value = valueDCT;
		tmp.timexID = "tmx0";
		tmp.typeTimex = "DATE";
		dct = tmp;
		
		String [] listColOrig = listColOrigString.split("\t");
		init(listCol, listColOrig, objectLearning);
		
		if(idCol.containsKey("tlink")){
			readCATRef(args[4]);
		}
		
		if(listTlink.size()>0 || !idCol.containsKey("tlink")){
			
			String [][] lines = TextProFileFormat.readFileTextPro(fileNameToRead, nbCol, true);
		
			//readTXPevtx(lines);
			
			// Add TENSE, ASPECT and POL features (using rules)
			if(idCol.containsKey("chunk") && idCol.containsKey("lemma") 
					&& idCol.containsKey("comp_morpho") && idCol.containsKey("pos")){
				VerbPhraseTenseAnnotator vb = new VerbPhraseTenseAnnotator();
				vb.colChunk = idCol.get("chunk");
				vb.colLemma = idCol.get("lemma");
				vb.colMorph = idCol.get("comp_morpho");
				vb.colPOS = idCol.get("pos");
				
				//add tense, aspect and polarity (using rules)
				if(objectLearning.equals("EVENT") || objectLearning.equals("TLINK") || objectLearning.equals("EVENT_eval")){
					lines = vb.addTenseAspectVP(lines,colTenseVP);
					
					for(int i=0; i<lines.length; i++){
						/* convert the tense+aspect+polarity feature into three independent features */
						if(lines[i][colTenseVP] != null && lines[i][colTenseVP].contains("+") 
								&& !idCol.containsKey("pairs") && !idCol.containsKey("tlink")){
							lines[i][colTenseVP] = lines[i][colTenseVP].replaceAll("\\+", "\t");
						}
						else if(!idCol.containsKey("pairs") && !idCol.containsKey("tlink")){
							lines[i][colTenseVP] = "_\t_\t_";
						}
						else if(lines[i][colTenseVP] != null && lines[i][colTenseVP].contains("+") ){}
						else{
							lines[i][colTenseVP] = "_";
						}
					}
				}
			}

			if(idCol.containsKey("tmx") && !listColOrigString.contains("tmx")){
				for (int i=0; i<lines.length; i++){
					if(lines[i][colLemma] != null){
						lines[i][idCol.get("tmx")] = "O";
						lines[i][idCol.get("tmxid")] = "O";
						lines[i][idCol.get("tmxvalue")] = "O";
					}
				}
			}
			
			
			if(idCol.containsKey("sentId")){
				lines = addSentId(lines, idCol.get("sentId"));
			}
			
			lines = modifyLemma(lines);
			lines = modifyMorph(lines);
			if(idCol.containsKey("stem")){
				lines = addStem(lines, idCol.get("stem"));
			}
			
			if(idCol.containsKey("posSent")){
				lines = addPosSentence(lines, idCol.get("posSent"));
			}
			
			if(idCol.containsKey("wnDomain")){
				//add the wordnet domain of a word
				lines = addWNDomain(lines, listWNDomain, idCol.get("wnDomain"));
			}
			
			//from full_morpho, add the different POS that a token can be
			int colDeriv = -1;
			if(idCol.containsKey("derivatario")){
				colDeriv = idCol.get("derivatario");
			}
			lines = addPOSMorphAnalysis(lines, colDeriv);
			
			if(idCol.containsKey("derivatario")){
				//add derivatario of a word + POS
				lines = addDerivatario(lines, listDeriv, idCol.get("derivatario"));
			}
			
			if(idCol.containsKey("freq")){
				//add the frequency of a word as an event
				lines = addFreqCol(lines, listFreq, idCol.get("freq"));
			}
			
			if(idCol.containsKey("depHRel")){
				//add dep rel with the format: headId:depRel
				lines = addDepRel(lines, idCol.get("depHRel"), objectLearning);
			}
			
			if(idCol.containsKey("root")){
				//indicate if a word is the root of the sentence
				lines = addRoot(lines, idCol.get("root"));
			}
			
			if(idCol.containsKey("sufpref")){
				//add the suffix or prefix of a word
				lines = addSuffixPrefix(lines, idCol.get("sufpref"));
			}
			
			if(idCol.containsKey("headWord")){
				//is a word the head of an NP or VP
				lines = headWord(lines, idCol.get("headWord"));
			}
			
			if(objectLearning.equals("EVENT") || objectLearning.equals("TLINK") || objectLearning.equals("EVENT_eval")){
				//add timex annotation from ref
				lines = addTimexRef(lines, idCol.get("tmx"));
				if(objectLearning.equals("TLINK") || objectLearning.equals("TLINKCl") || objectLearning.equals("pairs")){
					lines = addEmptyTimexRef(lines, idCol.get("tmx"));
				}		
			}
			if(objectLearning.equals("EVENT") || objectLearning.equals("TLINK")){
				//add event annotation from ref
				lines = addEventRef(lines, idCol.get("eventid"));
			}
			if(objectLearning.equals("EVENT_eval")){
				for (int i=0; i<lines.length; i++){
					if(lines[i][colLemma] != null && idCol.containsKey("event_id")){
						lines[i][idCol.get("event_id")] = "O";
					}
				}
			}
			

			if(idCol.containsKey("tlink")){
				lines = AddTLINK.add_candidate_pairs(lines, nbCol, idCol, listTimexEmpty);
				lines = AddTLINK.addTlinkRefToTXP (lines, idCol.get("tlink"), idCol.get("eventid"), idCol.get("tmx"), "train",
						listEvent, listTimex, listTimexEmpty, listTlink);
			}
			else if(idCol.containsKey("pairs")){
				lines = AddTLINK.add_candidate_pairs(lines, nbCol, idCol, listTimexEmpty);
			}
			else if(idCol.containsKey("tlinkCl")){
				lines = AddTLINK.addTlinkRefToTXP (lines, idCol.get("tlinkCl"), idCol.get("eventid"), idCol.get("tmx"), "train",
						listEvent, listTimex, listTimexEmpty, listTlink);
			}
			
			if(idCol.containsKey("connectives")){
				for (int i=0; i<lines.length; i++){
					if(lines[i][colTokenId] != null){
						lines[i][idCol.get("connectives")] = "_";
					}
				}
			}
			
			if(idCol.containsKey("role1")){
				for (int i=0; i<lines.length; i++){
					if(lines[i][colTokenId] != null){
						for(int k=0; k<5; k++){
							lines[i][idCol.get("role1")+k] = "_";
						}
					}
				}
			}
			

			// Sort columns according to id's in newIdCol (from a file) --> for TempRel
			if(idCol.containsKey("tlink") || idCol.containsKey("pairs") || idCol.containsKey("tlinkCl") || objectLearning.contains("EVENT")){
				lines = sortCol(lines);
			}
			
			if(newIdCol.size() == 0){
				newIdCol.putAll(idCol);
			}
			
			//completed and ordered the list of field for the header
			String fieldsList = "";
			
			ArrayList<Map.Entry<?, Integer>> l = new ArrayList(newIdCol.entrySet());
			Collections.sort(l, new Comparator<Map.Entry<?, Integer>>(){

					public int compare(Map.Entry<?, Integer> o1, Map.Entry<?, Integer> o2) {
		            	return o1.getValue().compareTo(o2.getValue());
		        	}});
		       
			for(Map.Entry<?, Integer> entryCol : l){
				String nameCol = entryCol.getKey().toString();
				if(nameCol.equals("tense") && !idCol.containsKey("tlink") && !idCol.containsKey("pairs")){
					fieldsList += "tense\taspect\tpol\t";
				}
				else if(nameCol.equals("comp_morpho")){
					fieldsList += "comp_morpho\tmorpho_pos\t";
				}
				else{
					fieldsList += nameCol+"\t";
				}
			}
			
			head += "# FILE: "+file.getName()+"\n";
			head += "# DATE: "+dct.value+"\n";
			head += "# FIELDS: ";
			head += fieldsList+"\n";
			
			if(idCol.containsKey("tlink") || idCol.containsKey("pairs") || idCol.containsKey("tlinkCl")){
				
				
				lines[0][0] = head;
				if(dct != null){
					lines[1][0] = "DCT_"+dct.value;
					lines[1][newIdCol.get("tmxid")] = "tmx0";
					lines[1][newIdCol.get("tmx")] = dct.typeTimex;
					lines[1][newIdCol.get("tmxvalue")] = dct.value;
					for(int k=0; k<nbCol; k++){
						if(lines[1][k] == null){
							lines[1][k] = "O";
						}
					}
				}
				TextProFileFormat.writeTextProFile(lines,fileNameOut,newIdCol.size());
			}
			else{
				lines[0][0] = head;
				TextProFileFormat.writeTextProFile(lines,fileNameOut,newIdCol.size());
			}
		
		}
	}

}


class eventStructure {
	String classEvent;
	String eventID;
	String eventEID;
	String[] instance;
	String relation;
}

class timexStructure {
	String typeTimex;
	String timexID;
	String[] instance;
	//String firstTok;
	String value;
	String anchor;
}

class linkStructure{
	String tlinkID;
	String source;
	String target;
	String relType;
}
