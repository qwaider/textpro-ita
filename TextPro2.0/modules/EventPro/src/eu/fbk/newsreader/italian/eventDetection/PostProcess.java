package eu.fbk.newsreader.italian.eventDetection;

/**
 * User: minard@fbk.eu
 * Date: Aug 2014
 * Description: post-process files after applying the event detection models in order to annotate multi-words events (using the poliremEVENTI.txt list)
 * Lang: Italian 
 * Usage: java eu.fbk.newsreader.italian.eventDetection.PostProcess file_in file_out
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class PostProcess {

	static int colTokenID = 1;
	static int colTense = 12;
	static int colAspect = 13;
	static int colPolarity = 14;
	static int colLemma = 6;
	static int colToken = 0;
	static int colEvent = 29;
	static int colEventClass = 30;
	static int colEventId = 28;
	
	static int nbCol = 31;
	
	private static String localPath = getTextProPath();
	
	static String file_polirem = localPath+"/resources/poliremEVENTI.txt";
	
	static HashMap<String,ArrayList<String>> list_polirem = new HashMap<String,ArrayList<String>> ();
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		read_polirem_file(file_polirem);
		

	    String [][] lines = TextProFileFormat.readFileTextPro(args[0], nbCol, true);
	    lines = add_event_id(lines);
	    lines = add_multi_tok_event_lines(lines);
	    TextProFileFormat.writeTextProFile(lines,args[1],nbCol);
	}

	/**
	 * Get the path to TextPro
	 * @return
	 */
	public static String getTextProPath (){
        String temp = PostProcess.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        File textprotmp = new File(temp);
        if (textprotmp.isDirectory()) {
            textprotmp = textprotmp.getParentFile();
        } else {
            textprotmp = textprotmp.getParentFile().getParentFile();
        }

        if (!textprotmp.exists() || !textprotmp.isDirectory()) {
            System.err.println("Couldn't initialize TextPro path!\nMake sure that textproX.X.jar is inside lib directory!");
            System.exit(0);
        }
        String canonicalpath = "";
        try {
			canonicalpath = textprotmp.getCanonicalPath()+File.separator;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return canonicalpath;
    }
	
	
	/**
	 * Add an id to each detected event
	 * @param lines
	 * @return
	 */
	public static String[][] add_event_id (String[][] lines){
		int id_ev = 1;
		for(int i=0; i<lines.length; i++){
			if(lines[i][1] != null && !lines[i][colEvent].equals("O")){
				lines[i][colEventId] = "e"+id_ev;
				lines[i][colEvent] = "B-EVENT";
				id_ev++;
			}
		}
		
		return lines;
	}
	
	/**
	 * Read the file containing the list of multi-words events in Italian
	 * Fill the hash list_polirem: key --> single word, value --> list of polirem in which this word appear
	 * @param filePolirem
	 */
	public static void read_polirem_file(String filePolirem){
		try{
			InputStream ips=new FileInputStream(filePolirem); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String line;
			
			Boolean inHead = true;
			
			while ((line=br.readLine())!=null && inHead){
				line = line.replace(" $","");
				if (!line.matches("^#")){
					ArrayList<String> elt_line = new ArrayList<String> ();
					if (line.contains("\t")){
						String [] temp = line.split("\t");
						for (String t : temp){
							elt_line.add(t);
						}
					}
					else{
						elt_line.add(line);
					}
					if (elt_line.size() > 0){
						elt_line.add(0, elt_line.get(0).replace("\'","\' "));
					    String [] elt_polirem = elt_line.get(0).split(" ");
					    if (elt_polirem.length > 1){
						    for (String elt : elt_polirem){
						    	ArrayList<String> list_polirem_elt = new ArrayList<String>();
						        if (list_polirem.containsKey(elt)){
						        	list_polirem_elt = list_polirem.get(elt);
						        }
						        list_polirem_elt.add(elt_line.get(0));
						        list_polirem.put(elt,list_polirem_elt);
						    }
					    }
					}
				}
			}
		}
		catch (Exception e){
			System.out.println(e.toString());
		}
		
		

	}
	
	/**
	 * Get the index of an element in a table
	 * @param tab
	 * @param elt
	 * @return
	 */
	public static int get_index (String [] tab, String elt){
		for (int i=0; i<tab.length; i++){
			if (tab[i].equals(elt)){
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * For each event that has been detected, check if it appear in a polirem and if all the words of the multi-words event appear.
	 * @param lines
	 * @return
	 */
	public static String[][] add_multi_tok_event_lines (String [][] lines){
	    for (int i=0; i<lines.length; i++){
	    	String [] line = lines[i];
	    	
	        if (line[1] != null && ! line[colEvent].equals("O")){

	        	//replace the lemma "essere_stare" by "stare"
	            line[colLemma] = line[colLemma].replace("essere_stare", "stare");

	            //Check if the lemma is in the list
	            if (list_polirem.containsKey(line[colLemma])){
	            	
	            	ArrayList<String> list_polirem_tok = list_polirem.get(line[colLemma]);
	                for (String polirem : list_polirem_tok){
	                    Boolean match = true;
	                    
	                    String [] elt_polirem = polirem.split(" ");
	                    
	                	int index_elt = get_index(elt_polirem, line[colLemma]);
	                    int j = 0;
	                    
	                    for (int k=(i-index_elt); k < i-index_elt+elt_polirem.length; k++){
	                        if (k >= 0 && k < lines.length && lines[k].length > 2 && lines[k][colLemma] != null){
	                            String [] line_temp_elt = lines[k];
	                            if (line_temp_elt.length == 1 || 
	                            		(line_temp_elt[1] != null && !line_temp_elt[colLemma].equals(elt_polirem[j]) 
	                            		&& !line_temp_elt[colToken].equals(elt_polirem[j]))){
	                                match = false;
	                            }
	                        }
	                        else{
	                            match = false;
	                        }
	                        j+=1;
	                        
	                        if(!match){
	                        	break;
	                        }
	                    }
	                    if (match && j>0){
	                    	String pref="B-";
	                        for (int k = i-index_elt; k<i-index_elt+elt_polirem.length; k++){
	                        	lines[k][colEvent] = pref+"EVENT";
	                        	pref = "I-";	
	                        	lines[k][colEventClass] = lines[i][colEventClass];
	                        	lines[k][colEventId] = lines[i][colEventId];
	                        }
	                    }

	                }
	            }   
	            // check if the token is in the list (in case of wrong lemmatization)
	            else if (list_polirem.containsKey(line[colToken])){
	                ArrayList<String> list_polirem_tok = list_polirem.get(line[colToken]);
	                for (String polirem : list_polirem_tok){
	                    Boolean match = true;
	                    String [] elt_polirem = polirem.split(" ");
	                    int index_elt = get_index(elt_polirem, line[colToken]);
	                    int j=0;
	                    Boolean infor = false;
	                    for (int k = i-index_elt; k<i-index_elt+elt_polirem.length; k++){
	                    	infor = true;
	                        if (k >= 0 && k < lines.length && lines[k].length > 1){
	                            String [] line_temp_elt = lines[k];
	                            
	                            if (line_temp_elt.length == 1 || (line_temp_elt[1] != null 
	                            		&& !line_temp_elt[colLemma].equals(elt_polirem[j]) 
	                            		&& !line_temp_elt[colToken].equals(elt_polirem[j]))){
	                                match = false;
	                            }
	                            else if (line_temp_elt[1] == null){
	                            	match = false;
	                            }
	                        }
	                        else{
	                            match = false;
	                        }
	                        j+=1;
	                    }
	                    
	                    if (match && infor){
	                    	String pref = "B-";
	                        for (int k = i-index_elt; k<i-index_elt+elt_polirem.length; k++){
	                        	lines[k][colEvent] = pref+"EVENT";
	                        	pref = "I-";	
	                        	lines[k][colEventClass] = lines[i][colEventClass];
	                        	lines[k][colEventId] = lines[i][colEventId];
	                        }
	                    }
	                }
	            }
	            // case of "esserci"
	            if (i>1 && line[1] != null && lines[i-1][colLemma] != null && line[colLemma].equals("essere") 
	            		&& (lines[i-1][colLemma].equals("ci")
	            		|| lines[i-1][colLemma].equals("c'"))){
	            	lines[i-1][colEvent] = "B-EVENT";
	            	lines[i][colEvent] = "I-EVENT";
	            	lines[i-1][colEventClass] = lines[i][colEventClass];
	            	lines[i-1][colEventId] = lines[i][colEventId];
	            }
	        }
	    }

	    return lines;
	}
	
}
