#!/bin/tcsh -f

#EventPro for Italian
#Intput: TextPro (token+tokenid+pos+full_morpho+comp_morpho+lemma+chunk+entity+parserid+head+deprel+tmx+tmxid+tmxvalue)
#Output: TextPro (event_id+event+eventclass)

set HOME="$TEXTPRO/modules/EventPro/"
set YAMCHA="$TEXTPRO/modules/tools/yamcha-0.33/usr/local/bin/yamcha"
set LANGUAGE=$1
set FILETXP=$2
set FILEOUT=$3

set EVENT1=$FILETXP.tmp1
set EVENT2=$FILETXP.tmp2

#cd $HOME

$JAVA_HOME/bin/java -cp "$HOME/lib/libstemmer_java/java/:$HOME/lib/preprocessLearning.jar" eu.fbk.newsreader.italian.eventDetection.PreProcessLearning_v3 $FILETXP $EVENT1 tense+stem+posSent+freq+derivatario+wnDomain+depHRel+root+sufpref+headWord+event_id+mainVb EVENT_eval

tail -n +4 $EVENT1 | $YAMCHA -m $HOME/models/event_bin_eval_final.model | $YAMCHA -m $HOME/models/event_class_2step_eval_final.model | cat > $EVENT2

$JAVA_HOME/bin/java -cp "$HOME/lib/postprocess.jar" eu.fbk.newsreader.italian.eventDetection.PostProcess $EVENT2 $EVENT1

#30 and 31
cut -f 29-31 $EVENT1 | tail -n +4 | cat > $EVENT2
tail -n +2 $FILETXP > $EVENT1
paste $EVENT1 $EVENT2 > $FILEOUT

rm $EVENT1
rm $EVENT2
