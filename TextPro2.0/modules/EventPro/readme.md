# Event detection and classification for Italian

***
Author: Anne-Lyse Minard (FBK)

Contact: minard@fbk.eu

Date: Sept 2014

version: v1.0
***

It is trained on the EVENTI@Evalita2014 dataset. For each token it adds the following information (as columns):

- event: if the token is inside the span of an event (B-EVENT, I-EVENT or O);
- eventid: the ID of the event;
- eventclass: the TimeML class of the event (OCCURRENCE, STATE, I_STATE, REPORTING, PERCEPTION, I_ACTION, ASPECTUAL).


Installation
------------
Copy the models used by this module:
```
cp textpro-ita/textpro-models/EventPro/* textpro-ita/TextPro2.0/modules/EventPro/models/
```

Usage EventPro
--------------

INPUT

In input the module takes a column format file containing the following fields:

   - token
   - tokenid
   - pos
   - full_morpho
   - comp_morpho
   - lemma
   - chunk
   - entity
   - parserid
   - head
   - deprel
   - tmx
   - tmxid
   - tmxvalue


OUTPUT

The output is a column format file containing three columns:

   - eventid
   - event
   - eventclass


EXAMPLES

- Running the module through textpro:
```
textpro.sh -l ita -c eventid+event+eventclass -o output_file input_file
```

- Running the module alone:
```
sh EventPro.sh input_file output_file
```


Third-party tools and resources (see NOTICE)
-------------------------------

* yamcha (http://chasen.org/~taku/software/yamcha/)
* TinySVM (http://chasen.org/~taku/software/TinySVM/)
* Snowball (http://snowball.tartarus.org/index.php)
* WN domain (http://wndomains.fbk.eu/)
* Derivatario (http://derivatario.sns.it/)


Reference
---------

Mirza, Paramita and Minard, Anne-Lyse. FBK-HLT-time: a complete Italian Temporal Processing system for EVENTI-Evalita 2014. Proceedings of the First Italian Conference on Computational Linguistic CLiC-it 2014 & the Fourth International Workshop EVALITA 2014 Vol. II, 2014, pp. 44-49.


