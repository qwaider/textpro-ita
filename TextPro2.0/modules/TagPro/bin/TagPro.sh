#! /bin/tcsh -f

### TagPro (APA-ALM)        ###
### V1, 2015   ###
### Authors: Alessio Palmero Aprosio and Anne-Lyse Minard ###

### How to use TagPro
### e.g.
### ./TagPro/bin/TagPro.sh -v
### ./TagPro/bin/TagPro.sh -h
### ./TagPro/bin/TagPro.sh ./TagPro/test/test.utf-8   //not using morphological information
### cat ./TagPro/test/test.utf-8 | ./TagPro/bin/TagPro.sh // reading from the standard input

### input file format
### file: the input file must be in one-word-per-line format, i.e. each line contains one token (word, punctuation character or
### parenthesis). A space line signals the end of a sentence.
### morphofile(optional): it contains the morphological information as produced by MorphoPro. 

### output format
### each line contains one token with its pos tag. A space line signals the end of a sentence.


### TagPro Version ###
set VERSION = "1.0.0, 2015"

### Initializing Parameters, Do not modify ###
#set TEXTPRO = /home/anne-lyse/Documents/NewsReader/TextPro/TextPro-master/modules/
#set JAVA_HOME = /usr/
set TAGPRO_DIR = $TEXTPRO/modules/TagPro/
set CRFSUITE_HOME = $TEXTPRO/modules/tools/crfsuite-0.12/bin/
set SCRIPT_DIR = $TEXTPRO/modules/bin
set MODEL_DIR = $TAGPRO_DIR/models/
set MODEL_NAME = "" #current model name
set ITA_MODEL_NAME = "PA.ita.ne.punc2.model" #ITA model
set GAZZETTINI_FOLDER = $TAGPRO_DIR/resources/
set outFileName = "" #output file
set inFileName = "" #input file
set inMorphoFileName = "" #morphological information file
set language = "ITA" #the language: [ENG|ITA]

set tmpFolderName = "/tmp"

set RANDOM=`bash -c 'echo $RANDOM'`
set inFileNamePreTreat = $tmpFolderName/$RANDOM-tagpro-pretreat.txt
set tmpFileName = $tmpFolderName/$RANDOM-tagpro-tmp.txt


### Reading Parameters ###
while ( {$1} != {} )
                
    if ( {$1} == "-h" || {$1} == "--help" ) then
        echo "TagPro"
        echo "Copyright (C) 2015 All rights reserved."
        echo ""
        echo "Usage: TagPro.sh [options] file"
        echo "  -h, --help                      show this help and exit"
        echo "  -v, --version                   show the version and exit"
        echo "  -s, --system                    check the system and exit"
        echo "  -m, --mfile=FILE                use FILE as a file containing morphological information"
        echo "  -l, --language=LANGUAGE         the language [ENG|ITA]  (default "$language")"
        echo "  -o, --output=FILE               use FILE as an output file"
        echo ""
        exit 0
    else if ( {$1} == "-v" || {$1} == "--version" ) then
        echo "TagPro Version: "$VERSION
        exit 0
    else if ( {$1} == "-s" || {$1} == "--system" ) then
        $TAGPRO_DIR/bin/TestTagPro.sh
        exit 0
    else if ( {$1} == "-l" || {$1} == "--language" ) then
        shift
        set language = $1
    else if ( {$1} == "-m" || {$1} == "--mfile" ) then
        shift
        set inMorphoFileName = $1
    else if ( {$1} == "-o" || {$1} == "--output" ) then
        shift
        set outFileName = $1
    else if ( $# == 1 ) then
        set inFileName = $1
    else
        $TAGPRO_DIR/bin/TagPro.sh -h
        exit 0
    endif

    shift

end


### Checking Parameters ###
if ( $inFileName == "" ) then
    echo "Error: Missing file name!"
    exit 1
endif
if ( ! -f $inFileName ) then
    echo "Error: No such file or directory!"
    exit 1
endif
if ( $inMorphoFileName != "" ) then
  if ( ! -f $inMorphoFileName ) then
    echo "Error: No such morpho file or directory!"
    exit 1
  endif
endif
if ( $language != "ENG" ) then 
  if ( $language != "ITA" ) then
    echo "Error: Unrecognized language option "$language"!"
    exit 1
  endif
endif


### Running System ###
if ( $inFileName == "") then
    cat $_ > $tmpFileName
else
    cat $inFileName > $tmpFileName
endif

#if ( $tmpFileName != "" ) then #reading from file
perl -pe 's/^(.+)$/$1 O/;s/^$/EOS/' $tmpFileName >$inFileNamePreTreat 
#perl -pe 's/^(.+)$/$1 O/;s/^$/EOS/' $tmpFileNameIn >$tmpFileNameEOS
#$TEXTPRO/../jdk8/bin/java -cp $SCRIPT_DIR/sectionextractor-1.0-SNAPSHOT-jar-with-dependencies.jar eu.fbk.dkm.sectionextractor.itapos.CreateTraining -i $tmpFileNameEOS -o $tmpFileName -s 2 -places $GAZZETTINI_FOLDER/list_loc_gpe_entitypro_utf8.txt -orgs $GAZZETTINI_FOLDER/list_org_entitypro_utf8.txt -nam $GAZZETTINI_FOLDER/list_nam_entitypro_p_github.txt -sur $GAZZETTINI_FOLDER/list_sur_entitypro_p_github.txt -f $TEXTPRO/modules/MorphoPro/
java -cp $SCRIPT_DIR/CreateTraining.jar eu.fbk.dkm.sectionextractor.itapos.CreateTraining -i $inFileNamePreTreat -o $tmpFileName -s 2 -places $GAZZETTINI_FOLDER/list_loc_gpe_entitypro_utf8.txt -orgs $GAZZETTINI_FOLDER/list_org_entitypro_utf8.txt -nam $GAZZETTINI_FOLDER/list_nam_entitypro_p_github.txt -sur $GAZZETTINI_FOLDER/list_sur_entitypro_p_github.txt -f $TEXTPRO/modules/MorphoPro/
$CRFSUITE_HOME/crfsuite tag -r -m $MODEL_DIR/$ITA_MODEL_NAME $tmpFileName | cut -f 2 | cat >$inFileNamePreTreat
    
if ( $outFileName == "" ) then
    #$tmpFileName
    if ( $inFileName == "") then
	paste $_ $inFileNamePreTreat | perl -pe 's/^EOS.*$//' | cat
    else
	paste $inFileName $inFileNamePreTreat | perl -pe 's/^EOS.*$//' | cat
    endif
else
    if ( $inFileName == "") then
	paste $_ $inFileNamePreTreat | perl -pe 's/^EOS.*$//' | cat >$outFileName
    else
	paste $inFileName $inFileNamePreTreat | perl -pe 's/^EOS.*$//' | cat >$outFileName
    endif
endif
#endif

\rm $tmpFileName
\rm $inFileNamePreTreat
