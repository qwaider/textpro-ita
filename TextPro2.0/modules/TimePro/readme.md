# Temporal expression recognition and normalization for Italian

***
Author: Paramita Mirza (FBK) and Anne-Lyse Minard (FBK)

Contact: paramita@fbk.eu, minard@fbk.eu

Date: Sept 2014

version: v1.0
***

It is trained on the EVENTI@Evalita2014 dataset for time expression recognition and classification. For time expression normalization we have adaptated timenorm library (https://github.com/bethard/timenorm) to Italian. For each token it adds the following information (as columns):

- tmx: the time expression type (DATE, TIME, SET or DURATION) in BIO format;
- tmxid: the ID of the time expression;
- tmxvalue: the normalized value of the time expression.


Installation
------------
Copy the models used by this module:
```
cp textpro-ita/textpro-models/TimePro/* textpro-ita/TextPro2.0/modules/TimePro/models/
```

Usage EventPro
--------------

INPUT

In input the module takes a column format file containing the following fields:

   - token
   - pos
   - lemma
   - chunk
   - entity


OUTPUT

The output is a column format file containing three columns:

   - tmx
   - tmxid
   - tmxvalue


EXAMPLES

- Running the module through textpro:
```
textpro.sh -l ita -date 2016-02-09 -c tmx+tmxid+tmxvalue -o output_file input_file
```

- Running the module alone:
```
sh TimePro.sh input_file output_file
```


Third-party tools and resources (see NOTICE)
-------------------------------

* yamcha (http://chasen.org/~taku/software/yamcha/)
* TinySVM (http://chasen.org/~taku/software/TinySVM/)
* timenorm (https://github.com/paramitamirza/timenorm)
* threetenbp-0.8.1 (http://www.threeten.org/)
* scala-library (http://www.scala-lang.org) 


Reference
---------

Mirza, Paramita and Minard, Anne-Lyse. FBK-HLT-time: a complete Italian Temporal Processing system for EVENTI-Evalita 2014. Proceedings of the First Italian Conference on Computational Linguistic CLiC-it 2014 & the Fourth International Workshop EVALITA 2014 Vol. II, 2014, pp. 44-49.


