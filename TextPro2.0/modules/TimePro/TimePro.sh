#!/bin/tcsh -f

set HOME="$TEXTPRO/modules/TimePro/"
set LANGUAGE=$1
set FILETXP=$2
set FILEOUT=$3
set CREATIONTIME=$4
set TIMEPRONORMIN="$FILEOUT-tmx.in"
set TIMEPRONORMOUT="$FILEOUT-tmx.out"
set MODEL=""

if ( $LANGUAGE == "eng" ) then
    set MODEL = "$HOME/models/tempeval3_silver-data.model"
else if ( $LANGUAGE == "ita" ) then
    set MODEL = "$HOME/models/timepro-it.model"
endif

if ( $MODEL != "" ) then
    #echo "cat $FILETXP | awk -f $HOME/resources/$LANGUAGE-rules | $TEXTPRO/modules/tools/yamcha-0.33/usr/local/bin/yamcha -m $MODEL >> $TIMEPRONORMIN"

    echo "# FILE:" > $TIMEPRONORMIN 
    if ( $CREATIONTIME == "" ) then
	echo "# CREATIONTIME:" >> $TIMEPRONORMIN
    else 
	echo "# CREATIONTIME: "$CREATIONTIME >> $TIMEPRONORMIN
    endif
    echo "# FIELDS: token pos     lemma   chunk   entity" >> $TIMEPRONORMIN

    cat $FILETXP |  awk -f $HOME/resources/$LANGUAGE"-rules" | $TEXTPRO/modules/tools/yamcha-0.33/usr/local/bin/yamcha -m $MODEL >> $TIMEPRONORMIN

    \rm -f /tmp/TIMEPRONORMIN.txt /tmp/TIMEPRONORMIN.out 
    #cp $TIMEPRONORMIN /tmp/TIMEPRONORMIN.txt

    java -cp "$HOME/lib/scala-library-2.11.5.jar:$HOME/lib/timenorm-0.9.2-IT.jar:$HOME/lib/threetenbp-0.8.1.jar:$HOME/lib/TimeProNorm.jar" eu.fbk.timePro.TimeProNormApply_TextPro $TIMEPRONORMIN $TIMEPRONORMOUT
#cp $TIMEPRONORMOUT /tmp/TIMEPRONORMIN.out

    tail -n +4 $TIMEPRONORMOUT > $FILEOUT
    #for english
    #echo "cat $TIMEPRONORMIN | $JAVA_HOME/bin/java -jar $HOME/lib/TimeProNorm_v2.1.jar $FILEOUT"
    #cat $TIMEPRONORMIN | $JAVA_HOME/bin/java -jar $HOME/lib/TimeProNorm_v2.1.jar $FILEOUT

endif

\rm $TIMEPRONORMIN
\rm $TIMEPRONORMOUT
