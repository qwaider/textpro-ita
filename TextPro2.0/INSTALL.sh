#! /bin/tcsh -f

set JAVA_REQUIRED_VERSION = 1.6

# Transform the required version string into a number that can be used in comparisons
set JREQVERSION = `echo $JAVA_REQUIRED_VERSION | sed -e 's;\.;0;g'`
# Check JAVA_HOME directory to see if Java version is adequate
echo "checking Java version ..."
if ( $?JAVA_HOME ) then
	set JAVA_EXE = $JAVA_HOME/bin/java
	set JCURRVERSION = `$JAVA_EXE -version |& grep "java version" | sed 's/java version *"*//' | sed 's/\.[^\.]*$//' | sed -e 's;\.;0;g'`
	if ( $JCURRVERSION ) then
		if ( $JCURRVERSION >= $JREQVERSION ) then
			set JAVA_HOME = `echo $JAVA_EXE | awk '{ print substr($1, 1, length($1)-9); }'`
			if ( $JAVA_HOME != "" ) then
				setenv JAVA_HOME $JAVA_HOME
        		endif 
		endif
	endif
endif

# If the correct Java version is detected, then export the JAVA_HOME environment variable
if ( $?JAVA_HOME ) then
	#echo $JAVA_HOME
else
    echo "WARNING! The required version of Java VM is "$JAVA_REQUIRED_VERSION" or later."
    echo "Please, set the JAVA_HOME environment variable before to install TextPro!"
    exit
endif

echo "installing TextPro, please wait ..."

if ( -d "./modules/src/" ) then
    cd ./modules/src/
    make clean
    make
    mkdir ../bin/
    cp ./tolowercase ../bin/
    cp ./invert ../bin/
    cd -
endif

# install TinySVM-0.09
echo "installing TinySVM-0.09, please wait ..."
cd ./modules/tools
gunzip < TinySVM-0.09.tar.gz | tar xf -
cd ./TinySVM-0.09
set TSVM_DIR = $PWD
mkdir ./usr
mkdir ./usr/local
./configure --prefix=$TSVM_DIR/usr/local/ --enable-shared=no
make
make check
make install
cd ../../../

# install yamcha-0.33
echo "installing yamcha-0.33, please wait ..."
cd ./modules/tools
gunzip < yamcha-0.33.tar.gz | tar xf -
cd ./yamcha-0.33
set YAMCHA_DIR = $PWD
mkdir ./usr
mkdir ./usr/local
./configure --with-svm-learn=$TSVM_DIR/usr/local/bin/svm_learn --prefix=$YAMCHA_DIR/usr/local/ --enable-shared=no
make
make check
make install
./yamcha-config --libexecdir
cp ./usr/local/libexec/yamcha/Makefile ./
cd ../../../

if ( ! -e "./modules/tools/yamcha-0.33/usr/local/bin/yamcha") then
    echo 
    echo "Yamcha installation failed!"
    echo " ...trying to install a modified version of yamcha-0.33, please wait ..."
    cd ./modules/tools
    gunzip < yamcha-0.33_fbk.tar.gz | tar xf -
    cd ./yamcha-0.33
    set YAMCHA_DIR = $PWD
    mkdir ./usr
    mkdir ./usr/local
    ./configure --with-svm-learn=$TSVM_DIR/usr/local/bin/svm_learn --prefix=$YAMCHA_DIR/usr/local/ --enable-shared=no
    make
    make check
    make install
    ./yamcha-config --libexecdir
    cp ./usr/local/libexec/yamcha/Makefile ./
    cd ../../../
endif

echo
echo "------------------"
echo " Tests running... "
echo "------------------"


set regpwd = `pwd | sed 's/\//\\\//g'`
sed "s/^TEXTPROHOME=.*/TEXTPROHOME=${regpwd}/" textpro.sh > textpro.sh.updated
set javahome = `echo $JAVA_HOME | sed 's/\//\\\//g'`
sed "s/^export JAVA_HOME=.*/export JAVA_HOME=${javahome}/" textpro.sh.updated > textpro.sh
\rm textpro.sh.updated

./RUNTEST.sh > ./RUNTEST.log
set wcrun = `grep '@Test ' ./RUNTEST.log | wc -l`
set wc = `grep 'TEST PASSED!' ./RUNTEST.log | wc -l`
echo
if ( $wc == $wcrun ) then
    echo "All tests passed. Installation complete!"
else
    echo "Sorry! Some tests failed."
endif


