#!/bin/sh


TEXTPROMEM="-ms128m -mx256m"
TEXTPROHOME=/home/newsreader/components-it/TextPro2.0
export TEXTPROHOME

export JAVA_HOME=/usr

$JAVA_HOME/bin/java -Dfile.encoding=UTF8 $TEXTPROMEM -cp "$TEXTPROHOME/lib/textpro2.0.jar:$TEXTPROHOME/lib/cleanpro-1.1.2.jar:$TEXTPROHOME/lib/commons-io-2.4.jar:$TEXTPROHOME/lib/json_simple-1.1.jar:$TEXTPROHOME/lib/jdom.jar:$TEXTPROHOME/lib/jericho-html-3.2.jar:$TEXTPROHOME/lib/junit-4.5.jar:$TEXTPROHOME/lib/lc4j-0.4.jar:$TEXTPROHOME/lib/colt.jar:$TEXTPROHOME/lib/fastutil-5.1.5.jar:$TEXTPROHOME/lib/jopt-simple-4.8.jar" eu.fbk.textpro.wrapper.wrapper $*

