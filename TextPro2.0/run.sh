#!/bin/sh

### This script is used when running the whole NewsReader Italian pipeline.


scratchDir=/tmp

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink 
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR

RANDOM=`bash -c 'echo $RANDOM'`

RANDOMFIX=$RANDOM
FILETXT=$scratchDir/$RANDOMFIX-textpro.txt
NAFFILE=$scratchDir/$RANDOMFIX.naf

cat $1 > $NAFFILE

BEGINTIME=`date '+%Y-%m-%dT%H:%M:%S%z'`

TEXTPRO=$DIR

java -cp $TEXTPRO/lib/NAF-TXP_conversion.jar:$TEXTPRO/lib/kaflib-naf-1.1.9.jar:$TEXTPRO/lib/jdom-2.0.5.jar eu.fbk.nwrtools.NAF2Txp $NAFFILE > $FILETXT

CREATIONTIME=`grep "# creationtime:" $FILETXT | sed "s/# creationtime:\s//"`
if [ $CREATIONTIME != "" ]; then
   CREATIONTIME=" -date "$CREATIONTIME
fi


$TEXTPRO/textpro.sh -l ita -v -y$CREATIONTIME -c token+tokenstart+pos+comp_morpho+parserid+head+deprel+chunk+entity+lemma+tmx+tmxid+tmxvalue+event+eventid+eventclass+polarity+certainty+evtime+tlinks $FILETXT

if [ -e "$FILETXT.txp" ]; then
   java -cp $TEXTPRO/lib/NAF-TXP_conversion.jar:$TEXTPRO/lib/kaflib-naf-1.1.9.jar:$TEXTPRO/lib/jdom-2.0.5.jar eu.fbk.nwrtools.Txp2NAF_v5 $FILETXT.txp $NAFFILE $FILETXT.txp.naf $BEGINTIME
fi

cat $FILETXT.txp.naf

#echo "Removing temporary files $FILETXT and $FILETXT.txp"
\rm $FILETXT
\rm $FILETXT.txp
\rm $FILETXT.txp.naf
\rm $NAFFILE
