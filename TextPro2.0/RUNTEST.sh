#!/bin/sh
echo "Testing..."

\rm test/output/*.txp

##echo "TextPro is running on test/input/trento_wiki_it.txt"
#./textpro.sh -v -o test/output/ -n trento_wiki_it.txt.txp -y -c token+lemma+tmx+tmxid+tmxvalue+polarity+certainty+evtime+event+eventid+eventclass+tlinks test/input/trento_wiki_it.txt
./textpro.sh -l ita -v -o test/output/ -n trento_wiki_it.txt.txp -y -c token+lemma+deprel+head+parserid test/input/trento_wiki_it.txt

echo
$JAVA_HOME/bin/java -Dfile.encoding=UTF8 -cp "./lib/textpro2.0.jar:./lib/junit-4.5.jar" eu.fbk.textpro.tester.TextProTester -e eu.fbk.textpro.tester.exact -runTextpro false -t test/gold/trento_wiki_it.txt.txp -f test/output/trento_wiki_it.txt.txp
