                     -------- TextPro v2.0-NWR -------

         ---- FBK (Fondazione Bruno Kessler, Povo, Italy) ----
     --- Emanuele Pianta, Christian Girardi, Mohammad Qwaider ---

TextPro supports the most common NLP tasks, such as tokenization, sentence segmentation, 
part-of-speech tagging, lemmatization, named entity extraction, chunking and keywords 
extraction for Italian and English.
This version of TextPro has been set up for the NewsReader project. It contains only the 
modules of TextPro2.0 working for Italian and new modules developed for the purpose of 
NewsReader (time expression, event detection, event factuality, temporal relation extraction).


    Copyright (C) 2015-2016 Fondazione Bruno Kessler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


Requirements
------------
    * Java 1.6 or higher
    * Perl 5.8.0 or higher
    * GNU make
    * rm, cat, sed, awk (they are fundamental UNIX tools)
    * tcsh shell
    * C++ compiler (gcc 2.95 or higher)
    * Python 2.6 or higher
    

Installing TextPro
------------------

1) Unzip the zip file (e.g. TextProFBK-2.0.zip) into a directory of your choice (e.g. /home/).
WARNING: be very careful to don't put the spaces in the path (tcsh shell doesn't like spaces!).

2) TextPro needs a Java™ Virtual Machine on your computer. If you don't have Java™ runtime 
environment (JRE) yet, download and install it from http://java.sun.com/javase/downloads/index.jsp.
Then set the JAVA_HOME environment variable to point to the directory where the Java™ runtime 
environment is installed on your computer.
On csh/tcsh shell type this command:
$> setenv JAVA_HOME <directory where the JDK is installed>

3) From the TextPro home directory execute:
   $> ./INSTALL.sh

If you see the message "All tests passed. Installation complete!" then TextPro run well.


Usage TextPro
-------------

INPUT

TextPro needs a plain text or string by STDIN. The encoding must be UTF8. 
The plain text can be already tokenized (one token per line) or not. 
In the first case you must disable the tokenization by "-d tokenization" option. The empty lines are left.


OUTPUT

TextPro produces an output file with one token per line. The informations for each token are represented 
in the line with the columns separated by tabular space. 
You choose the output information with -c option (see the document docs/TextPro_annotations.html for details).
It is possible to obtain XML-formatted output from TextPro using the option -xml.


EXAMPLES 
```
- Part of Speech tagging for Italian language:
$> textpro.sh -l ita -c token+pos -o /tmp/ example_ita.txt

- Named Entity Recognition for English language:
$> textpro.sh -l eng -c token+entity -o /tmp/ example_eng.txt

- Keywords extraction for Italian getting just the best 3 keywords:
$> textpro.sh -l ita -c keywords -kxparams "n=3" -o /tmp/ example_ita.txt

- XML output:
$> textpro.sh -l ita -c token+pos+lemma+entity -xml -o /tmp/ example_ita.txt
```


HELP
----
```
$> ./textpro.sh -h
Usage:
   textpro.sh [OPTIONS] <STDIN or INPUT FILE or DIR>

Options:
-version                            show the version details and exit;
-html                               clean html input file; the relevant text is kept as input text;
-h                                  show the help and exit;
-debug                              debug mode, do not delete tmp-files and to get more verbose output;
-report                             check the input text and print a report on the unknown things;
-v                                  verbose mode;
-l        <LANGUAGE>                the language: 'eng' or 'ita' are possible; 'eng' is the default;
-c        <COLUMN or HEADER fields> the sequence of the output values: token+tokenid+tokennorm+tokenstart+tokenend+tokentype+full_morpho+pos+comp_morpho+lemma+wnpos+chunk+entity+parserid+feats+head+deprel+keywords+tmx+tmxid+tmxvalue+event+eventid+eventclass+polarity+certainty+evtime+tlinks;
-o        <DIRNAME>                 the output directory path;
-n        <FILENAME>                the output filename. If this value is specified the output is redirected to the file named as FILENAME. By default the file named as INPUTFILE plus '.txp' suffix;
-xml                                provides XML output;
-y                                  force rewriting all existing output files;
-d        tokenization+sentence     disable the tokenization or/and sentence splitting;
-r                                  process the input directory recursively;
-tmp      <TMPDIR>                  set a temporary directory (by default TextPro uses /tmp/);
-kxparams <PARAMS>                  set the options of the keywords extraction module: PARAMS is a list of pair PARAMNAME[=VALUE] separated by comma. The list of all PARAMNAME and their VALUE are available in docs/KX_Reference.pdf.
-update                             update TextPro model using manual adding;
-date     <CREATIONTIME>            set the document creation time (yyyy-mm-dd)
```


UPGRADING FROM v1.5.2 TO v2.0
-----------------------------
The main changes are:

- UTF encoding is fully supported; 

- TextPro wrapper has been coded using Java instead of Perl. So the previous call textpro.pl should be changed into textpro.sh command;

- the column sentence was removed (the string "<eos>" to split sentences is no longer used). Output sentences are separated by an empty lines (also TextPro keeps all input empty lines as output). Moreover the output contains just the columns specified in the -c option only;

- if a module does not have an output to give, it is value should be __NULL__ (in the version < 1.5.2 of TextPro it was used an empty string but this format could produce some parsing errors mainly using Java's String.split method that, by defaults, removes empty fields between two delimiters).

- two default header lines are present in output: the language (automatically detected or passed using the option -l by the user) and the timestamp about when the processing was done. The following is an example of header lines:

\# FILE: prova.txt

\# LANGUAGE: ita

\# TIMESTAMP: 2014-09-16T10:00:20+0200

\# FIELDS: token	pos	lemma

- a dependecy parser (for Italian only) trained on TUT (Turin University Treebank) has been added. It uses MaltParser, a state-of-the-art data-driven dependency parser. 
For each token it adds the following information (as columns):
* parserid: the token ID within the sentence;
* feats: the morphological features of the token;
* head: the ID of the head of the token;
* deprel: the label of the dependency between the token and its head.


UPGRADING FROM v2.0 TO v2.O.NWR
------------------------------
The main changes are:

- TimePro for Italian has been implemented. It is trained on the EVENTI@Evalita2014 dataset for time expression recognition and classification. For time expression normalization we have adaptated timenorm library (https://github.com/bethard/timenorm) to Italian. 
For each token it adds the following information (as columns):
* tmx: the time expression type (DATE, TIME, SET or DURATION) in BIO format;
* tmxid: the ID of the time expression;
* tmxvalue: the normalized value of the time expression.

Authors: Paramita Mirza (paramita@fbk.eu) and Anne-Lyse Minard (minard@fbk.eu)

- a new module for event detection and classification for Italian has been added (EventPro). It is trained on the EVENTI@Evalita2014 dataset. 
For each token it adds the following information (as columns):
* event: if the token is inside the span of an event (B-EVENT, I-EVENT or O);
* eventid: the ID of the event;
* eventclass: the TimeML class of the event (OCCURRENCE, STATE, I_STATE, REPORTING, PERCEPTION, I_ACTION, ASPECTUAL).

Author: Anne-Lyse Minard

- a new module for event factuality for Italian has been implemented (FactPro). It is trained on Fact-Ita Bank (http://hlt-nlp.fbk.eu/technologies/fact-ita-bank). It describes the factuality of an event (detected by the EventPro module) through 3 attributes: polarity, certainty and time.
For each token it adds the following information (as columns):
* polarity: the polarity of the event (POL_POS, POL_NEG, O);
* certainty: how certain the source is about the event (CERTAIN, UNCERTAIN, O);
* evtime: when an event occured or will occur (NON_FUTURE, FUTURE, O).

Authors: Anne-Lyse Minard and Federico Nanni

- a new module for temporal relation extraction for Italian has been added (TempRelPro). It is trained on EVENTI@Evalita2014 dataset. It detects temporal relations between two events. between one event and one time expression and between two time expressions. 
It adds the list of relations in the header of the file as follows:

\# TLINKS: (e1;IS_INCLUDED;tmx1) (tmx0;AFTER;tmx1)

Authors: Paramita Mirza and Anne-Lyse Minard 

- TagPro module has been changed in order to use a training corpus compatible with the GNU GPLv3 license. The new version is based on CRFsuite and the model is trained on a corpus composed by Wikipedia, Wikinews and Wikisource articles annotated automatically using a model trained on the annotated corpus Elsnet (http://catalog.elra.info/product_info.php?products_id=86).

Authors: Anne-Lyse Minard and Alessio Palmero Aprosio
